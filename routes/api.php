<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['cors']], function(){
    Route::get('', 'HomeController@index');
    Route::get('verify/{verification_code}', 'AuthController@verifyUser');
    Route::get('update-inscriptions-cron', 'InscriptionController@updateStatusCron');
    Route::group(['middleware' => ['checksession']], function(){
        Route::post('register', 'AuthController@register');
        Route::post('register/check-email/{id?}', 'AuthController@checkEmail');
        Route::post('register/check-number-id/{id?}', 'AuthController@checkNumberId');
        Route::post('login', 'AuthController@login');
        Route::post('password/reset', 'Auth\PasswordController@reset')->name('password.reset');
        Route::post('recover', 'AuthController@recover');
        Route::post('reset', 'Auth\ResetPasswordController@reset')->name('password.reset');
        Route::get('student/types', 'UserController@studentTypes');
        Route::get('student/by-id/{id}', 'UserController@studentById');
        Route::get('campus','CampusController@index');
        Route::get('campus/{id}','CampusController@show');
        Route::get('career','CareerController@index');
        Route::get('career/{id}','CareerController@show');
        Route::get('classroom','ClassroomController@index');
        Route::get('classroom/{id}','ClassroomController@show');
        Route::get('occupation','OccupationController@index');
        Route::get('workshop','WorkshopController@index');
        Route::get('workshop/current-period','WorkshopController@currentPeriod');
        Route::get('workshop/by-slug/{slug}','WorkshopController@bySlug');
        Route::get('workshop/by-slug-current-period/{slug}','WorkshopController@bySlugCurrentPeriod');
        Route::get('workshop-period/by-period/{period_id}','WorkshopPeriodController@getByPeriod');
        Route::get('requirement','RequirementController@index');
        Route::get('requirement/by-period/{period_id}','RequirementController@byPeriod');
        Route::get('slider','SliderController@index');
        Route::get('event-category','EventCategoryController@index');
        Route::get('event-category/{id}','EventCategoryController@show');
        Route::get('event-category/{id}/events','EventCategoryController@getEventsByCategory');
        Route::get('event','EventController@index');
        Route::get('event/by-slug/{slug}','EventController@bySlug');
        Route::get('event/offering-by-slug/{slug}','EventController@offeringBySlug');
        Route::get('event/verify/{event_showing_id}/{code}','EventController@verify');
        Route::get('offering','OfferingController@index');
        Route::get('offering/by-event/{event_id}','OfferingController@byEvent');
        Route::get('period/current','PeriodController@current');
        Route::post('talent','TalentController@store');
        Route::get('talent/pdf/{token}','TalentController@getPdf');
        Route::get('users/active','UserController@active');
        Route::group(['middleware' => ['jwt.auth']], function() {
            Route::post('logout', 'AuthController@logout');
            Route::get('logout', 'AuthController@logout');
            Route::put('users/profile','UserController@updateProfile');
            Route::put('users/profile-pass','UserController@updateProfilePass');
            Route::put('users/profile-photo','UserController@updateProfilePhoto');
            Route::put('users/profile-qr','UserController@updateQrPhoto');
            Route::get('users/profile-id-card/{id?}','UserController@idCard');
            Route::get('users/profile-id-card-admin/{user_id}/{id?}','UserController@idCardAdmin');
            Route::post('users/valid-token','UserController@validToken');
            Route::post('users/export','UserController@exportExcel');
            Route::put('users/{id}/restore','UserController@restore')->middleware('group:depto');
            //users
            Route::get('perfil','UserController@index')->middleware('group:depto');

            Route::get('users','UserController@index')->middleware('group:depto');
            Route::post('users','UserController@store')->middleware('role:admin');
            Route::get('users/search','UserController@search')->middleware('group:depto');
            Route::get('users/{id}','UserController@show')->middleware('group:depto');
            Route::put('users/{id}','UserController@update')->middleware('role:admin');
            Route::delete('users/{id}','UserController@destroy')->middleware('role:admin');
            Route::put('users/change-password/{id}','UserController@changePassword')->middleware('role:admin');
            Route::put('users/verify/{id}','UserController@verify')->middleware('role:admin');
            // Route::post('student/{id}','UserController@updateStudent')->middleware('group:depto');
            
            Route::get('period','PeriodController@index')->middleware('role:admin');
            Route::post('period/check-name/{id?}','PeriodController@checkName')->middleware('role:admin');
            Route::post('period/requirement','PeriodController@requirement')->middleware('role:admin');
            Route::get('period/user-files-requirements','PeriodController@requirementsFilesPeriodUser');
            Route::get('period/user-files-requirements/{user_id}','PeriodController@requirementsFilesPeriodByUser')->middleware('group:depto');
            Route::get('period/user-files-requirements-inscription/{inscription_id}','PeriodController@requirementsFilesPeriodByInscription')->middleware('group:depto');
            Route::post('period','PeriodController@store')->middleware('role:admin');
            Route::get('period/{id}','PeriodController@show')->middleware('group:depto');
            Route::put('period/{id}','PeriodController@update')->middleware('role:admin');
            Route::delete('period/{id}','PeriodController@destroy')->middleware('role:admin');
            Route::put('period/{id}/restore','PeriodController@restore')->middleware('group:depto');

            Route::get('teacher','TeacherController@index')->middleware('group:depto');
            Route::post('teacher','TeacherController@store')->middleware('role:admin');
            Route::get('teacher/{id}','TeacherController@show')->middleware('group:depto');
            Route::put('teacher/{id}','TeacherController@update')->middleware('role:admin');
            Route::delete('teacher/{id}','TeacherController@destroy')->middleware('role:admin');
            Route::put('teacher/{id}/restore','TeacherController@restore')->middleware('group:depto');

            Route::post('classroom/check-name/{id?}','ClassroomController@checkName')->middleware('role:admin');
            Route::post('classroom','ClassroomController@store')->middleware('role:admin');
            Route::put('classroom/{id}','ClassroomController@update')->middleware('role:admin');
            Route::delete('classroom/{id}','ClassroomController@destroy')->middleware('role:admin');
            Route::put('classroom/{id}/restore','ClassroomController@restore')->middleware('group:depto');

            Route::post('workshop','WorkshopController@store')->middleware('role:admin');
            Route::post('workshop/check-name/{id?}','WorkshopController@checkName')->middleware('role:admin');
            Route::post('workshop/inscribe','WorkshopController@inscribe');
            Route::get('workshop/inscribe-pdf/{workshop_period_id}','WorkshopController@getInscribePdf');
            Route::get('workshop/{id}','WorkshopController@show')->middleware('group:depto');
            Route::put('workshop/{id}','WorkshopController@update')->middleware('role:admin');
            Route::delete('workshop/{id}','WorkshopController@destroy')->middleware('role:admin');
            Route::put('workshop/{id}/restore','WorkshopController@restore')->middleware('group:depto');
            
            Route::post('workshop-period','WorkshopPeriodController@store')->middleware('role:admin');
            Route::post('workshop-period/check-name/{id?}','WorkshopPeriodController@checkName')->middleware('role:admin');
            Route::get('workshop-period/all-by-user','WorkshopPeriodController@getAllByUser');
            Route::get('workshop-period/all-by-user/{user_id}','WorkshopPeriodController@getAllByUserAdmin')->middleware('group:depto');
            Route::get('workshop-period/{id}/by-user/{user_id}','WorkshopPeriodController@getByUserAdmin')->middleware('group:depto');
            Route::get('workshop-period/{id}/by-user','WorkshopPeriodController@getByUser');
            Route::get('workshop-period/{id}/users','WorkshopPeriodController@getUsers')->middleware('role:admin');
            Route::get('workshop-period/{id}/pdf-users','WorkshopPeriodController@getPdfUsers')->middleware('group:depto');
            
            Route::get('workshop-period/{id}','WorkshopPeriodController@show')->middleware('group:depto');
            Route::put('workshop-period/{id}','WorkshopPeriodController@update')->middleware('role:admin');
            Route::delete('workshop-period/{id}','WorkshopPeriodController@destroy')->middleware('role:admin');
            Route::put('workshop-period/{id}/restore','WorkshopPeriodController@restore')->middleware('group:depto');

            Route::post('campus','CampusController@store')->middleware('role:admin');
            Route::post('campus/check-name/{id?}','CampusController@checkName')->middleware('role:admin');
            Route::put('campus/{id}','CampusController@update')->middleware('role:admin');
            Route::delete('campus/{id}','CampusController@destroy')->middleware('role:admin');
            Route::put('campus/{id}/restore','CampusController@restore')->middleware('group:depto');

            Route::post('career','CareerController@store')->middleware('role:admin');
            Route::post('career/check-name/{id?}','CareerController@checkName')->middleware('role:admin');
            Route::put('career/{id}','CareerController@update')->middleware('role:admin');
            Route::delete('career/{id}','CareerController@destroy')->middleware('role:admin');
            Route::put('career/{id}/restore','CareerController@restore')->middleware('group:depto');

            Route::post('occupation','OccupationController@store')->middleware('role:admin');
            Route::post('occupation/check-name/{id?}','OccupationController@checkName')->middleware('role:admin');
            Route::get('occupation/{id}','OccupationController@show')->middleware('group:depto');
            Route::put('occupation/{id}','OccupationController@update')->middleware('role:admin');
            Route::delete('occupation/{id}','OccupationController@destroy')->middleware('role:admin');
            Route::put('occupation/{id}/restore','OccupationController@restore')->middleware('group:depto');
            
            Route::post('requirement','RequirementController@store')->middleware('role:admin');
            Route::get('requirement/{id}','RequirementController@show')->middleware('group:depto');
            Route::put('requirement/{id}','RequirementController@update')->middleware('role:admin');
            Route::delete('requirement/{id}','RequirementController@destroy')->middleware('role:admin');
            Route::put('requirement/{id}/restore','RequirementController@restore')->middleware('group:depto');
            Route::post('requirement/file','RequirementController@storeFile');

            Route::post('slider','SliderController@store')->middleware('role:admin');
            Route::get('slider/{id}','SliderController@show')->middleware('group:depto');
            Route::put('slider/{id}','SliderController@update')->middleware('role:admin');
            Route::delete('slider/{id}','SliderController@destroy')->middleware('role:admin');
            Route::put('slider/{id}/restore','SliderController@restore')->middleware('group:depto');
            
            Route::post('event-category','EventCategoryController@store')->middleware('role:admin');
            Route::put('event-category/{id}','EventCategoryController@update')->middleware('role:admin');
            Route::delete('event-category/{id}','EventCategoryController@destroy')->middleware('role:admin');
            
            Route::post('event','EventController@store')->middleware('role:admin');
            Route::post('event/check-name/{id?}','EventController@checkName')->middleware('role:admin');
            Route::post('event/inscribe','EventController@inscribe');
            Route::get('event/get-codes','EventController@getCodes')->middleware('group:depto');
            Route::get('event/{id}/inscribe-pdf','EventController@getInscribePdf');
            /*Route::post('event/{id}/sync-inscriptions','EventController@syncInscriptions');*/
            Route::get('event/{id}/inscribe-pdf-admin','EventController@getInscribePdfAdmin')->middleware('role:admin');
            Route::get('event/{id}/tickets','EventController@tickets')->middleware('group:depto');
            Route::get('event/{id}','EventController@show')->middleware('group:depto');
            Route::put('event/{id}','EventController@update')->middleware('role:admin');
            Route::post('event/{id}/add-showing','EventController@addShowing')->middleware('group:depto');
            Route::delete('event/{id}','EventController@destroy')->middleware('role:admin');
            Route::put('event/{id}/restore','EventController@restore')->middleware('group:depto');
            
            Route::get('event-ticket','EventTicketController@index')->middleware('group:admin');
            Route::post('event-ticket','EventTicketController@store')->middleware('group:admin');
            Route::get('event-ticket/{id}','EventTicketController@show');
            Route::delete('event-ticket/{id}','EventController@destroy')->middleware('role:admin');

            Route::post('offering','OfferingController@store')->middleware('role:admin');
            Route::get('offering/{id}','OfferingController@show')->middleware('group:depto');
            Route::put('offering/{id}','OfferingController@update')->middleware('role:admin');
            Route::delete('offering/{id}','OfferingController@destroy')->middleware('role:admin');
            Route::put('offering/{id}/restore','OfferingController@restore')->middleware('group:depto');

            Route::get('role/by-group/{slug}','RoleController@getByGroup')->middleware('role:admin');
            Route::get('role/','RoleController@index')->middleware('group:depto');

            Route::get('inscription','InscriptionController@index')->middleware('group:depto');
            Route::get('inscription/files-actives','InscriptionController@filesActives')->middleware('group:depto');
            Route::get('inscription/{id}','InscriptionController@show')->middleware('group:depto');
            Route::delete('inscription/{id}','InscriptionController@destroy')->middleware('group:depto');
            Route::post('inscription/check-ticket/{id}','InscriptionController@checkTicket')->middleware('group:depto');
            Route::put('inscription/{id}/validate','InscriptionController@validateStatus')->middleware('group:depto');
            Route::put('inscription/ask-again-files/{id}','InscriptionController@askAgainFiles')->middleware('group:depto');
            Route::post('inscription/inscribe/{id}','InscriptionController@inscribe')->middleware(['role:admin']);
            Route::post('inscription/change/{id}','InscriptionController@change')->middleware(['role:admin']);
            Route::get('inscription/by-user/{id}/{user_id}','InscriptionController@getByUserAdmin')->middleware('group:depto');
            Route::get('inscription/by-user/{id}','InscriptionController@getByUser');
            Route::get('inscription/inscribe-pdf-user/{user_id}/{id}','InscriptionController@getInscribePdfUser')->middleware('group:depto');
            Route::get('inscription/inscribe-pdf/{id}','InscriptionController@getInscribePdf');
            Route::put('inscription/files-uploaded/{id}','InscriptionController@filesUploaded');
            Route::put('inscription/send-catureline/{id}','InscriptionController@sendCaptureline');
            Route::put('inscription/resend-catureline/{id}','InscriptionController@resendCaptureline');
            Route::put('inscription/remove-catureline/{id}','InscriptionController@removeCaptureline');
            
            
            
            Route::get('talent','TalentController@index')->middleware('group:depto');
            Route::get('talent/export-csv','TalentController@exportCsv')->middleware(['role:admin|talent']);
            Route::get('talent/{id}','TalentController@show')->middleware('group:depto');
            Route::get('talent/pdf-admin/{id}','TalentController@getPdfAdmin')->middleware('group:depto');

            Route::post('report/one' , 'ReportController@one')->middleware('role:admin');
            Route::post('report/two' , 'ReportController@two')->middleware('role:admin');
            Route::post('report/three' , 'ReportController@three')->middleware('role:admin');
            Route::post('report/events' , 'ReportController@events')->middleware('role:admin');

            Route::get('captureline','CapturelineController@index')->middleware('group:depto');
            Route::post('captureline','CapturelineController@store')->middleware('group:depto');
            Route::get('captureline/{id}','CapturelineController@show')->middleware('group:depto');
            Route::put('captureline/{id}','CapturelineController@update')->middleware('group:depto');
            Route::delete('captureline/{id}','CapturelineController@destroy')->middleware('group:depto');

            Route::get('college-degree-hours','CollegeDegreeHoursController@index')->middleware('group:depto');
            Route::post('college-degree-hours','CollegeDegreeHoursController@store')->middleware('group:depto');
            Route::get('college-degree-hours/{id}','CollegeDegreeHoursController@show')->middleware('group:depto');
            Route::put('college-degree-hours/{id}','CollegeDegreeHoursController@update')->middleware('group:depto');
            Route::delete('college-degree-hours/{id}','CollegeDegreeHoursController@destroy')->middleware('group:depto');
            Route::post('college-degree-hours/start','CollegeDegreeHoursController@start')->middleware('group:depto');
            Route::put('college-degree-hours/finish/{id}','CollegeDegreeHoursController@finish')->middleware('group:depto');
            // Route::put('captureline/{id}/restore','CapturelineController@restore')->middleware('group:depto');

            Route::post('upload/excel', 'UploadController@excel')->middleware('role:admin');
            Route::post('upload/captureline', 'UploadController@captureline')->middleware('role:admin');
            Route::post('upload/images/', 'UploadController@images');
            Route::post('upload/image', 'UploadController@image');
            Route::post('upload/image-base64', 'UploadController@imageBase64');
            Route::post('upload/images/profile', 'UploadController@profile');
            Route::post('upload/requirement', 'UploadController@requirement');
            Route::post('upload/requirement/{user_id}', 'UploadController@requirementUser')->middleware('group:depto');
            Route::delete('upload/tmp/{file}', 'UploadController@deleteTmp');
        });
    });
});