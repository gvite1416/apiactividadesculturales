<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            text-rendering: optimizeLegibility;
            font-family: "Times New Roman", sans-serif;
        }

        .col-md-2 {
            width: 16.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-4 {
            width: 33%;
            position: relative;
            display: inline-block;
        }
        .col-md-6 {
            width: 49.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-8 {
            width: 66%;
            position: relative;
            display: inline-block;
        }
        .col-md-12 {
            width: 100%;
            position: relative;
            display: inline-block;
        }
        .page {
            position: relative;
            page-break-after: always;
            margin: 2cm 0 0 2cm;
            padding: 0;
            width: 20cm;
            height: 29.5cm;
            overflow: hidden;
            background-color: #ffffff;
        }
        h1{
            font-size: 20px;
        }
        h2{
            font-size: 18px;
        }
        h3{
            font-size: 16px;
        }
        .header-titles{
            width: 5cm;
            margin-left: 10px;
            border-left: 5px solid #0036A4;
            padding-left: 10px;
            position: relative;
            display: inline-block;
            color: #0036A4;
        }
        .header-titles h3,.header-titles h4,.header-titles h5{
            line-height: 0.8;
            white-space: pre-wrap;
            margin-bottom: 3px;
            text-transform: uppercase;
        }
        .header-titles h3 {
            font-size: 0.4cm;
            font-weight: bold;
        }
        .header-titles h4 {
            font-size: 0.3cm;
            font-weight: bold;
        }
        .header-titles h5 {
            font-size: 0.3cm;
        }
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        p{
            font-size: 22px;
            text-align: justify;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        table thead tr{
            background-color: #E0E0E0;
        }
        table, th, td{
            border: 1px solid #E0E0E0;
        }
    </style>
</head>

<body>
    @foreach($period['workshops'] as $workshop)
    <div class="page">
        <div>
            <div class="col-md-4">
                <img width="100px" src="{{storage_path('app/public/svg/logo-unam.png')}}">
            </div>
            <div class="col-md-8 text-right">
                <img width="100px" src="{{storage_path('app/public/svg/logo-fesa.png')}}">
                <img width="100px" src="{{storage_path('app/public/svg/logo-cultural.png')}}">
                <div class="header-titles">
                    <h3 class="text-left">FACULTAD DE ESTUDIOS SUPERIORES ARAGÓN</h3>
                    <h4 class="text-left">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h4>
                    <h5 class="text-left">ACTIVIDADES CULTURALES</h5>
                </div>
            </div>
        </div>
        <div>
            <div class="col-md-12">
                <h1 class="text-center">UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO</h1>
                <h2 class="text-center">FES - ARAGÓN</h2>
                <h3 class="text-center">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h3>
                <h3 class="text-center">DEPARTAMENTO DE ACTIVIDADES CULTURALES</h3>
            </div>
        </div>
        <div>
            <h4>Carrera: {{$career}}</h4>
            <table>
                <thead>
                    <tr>
                        <th colspan="3">Taller</th>
                        <th colspan="2">Numero de Alumnos</th>
                    </tr>
                    <tr>
                        <th colspan="3">{{$workshop['name']}}</th>
                        <th colspan="2">{{$workshop['count_inscribed']}}</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Alumno</th>
                        <th>No. CTA</th>
                        <th>Sistema</th>
                        <th>Carrera</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($workshop['students'] as $key => $si)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$si['name']}} {{$si['lastname']}} {{$si['surname']}}</td>
                            <td>{{$si['number_id']}}</td>
                            <td>{{$si['dataStudent'][0]['system']}}</td>
                            <td>{{$si['dataStudent'][0]['career']['name']}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endforeach
</body>

</html>