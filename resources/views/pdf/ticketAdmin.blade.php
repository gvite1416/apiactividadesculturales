<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            text-rendering: optimizeLegibility;
            font-family: "Times New Roman", sans-serif;
            color: #0036A4;
        }
        .col-md-1 {
            width: 14%;
            position: relative;
            display: inline-block;
        }
        .col-md-2 {
            width: 16.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-3 {
            width: 24.4%;
            position: relative;
            display: inline-block;
        }
        .col-md-4 {
            width: 33%;
            position: relative;
            display: inline-block;
        }
        .col-md-5 {
            width: 41%;
            position: relative;
            display: inline-block;
        }
        .col-md-6 {
            /*width: 370px;*/
            width: 47.7%;
            position: relative;
            display: inline-block;
        }
        .col-md-7 {
            width: 58.3%;
            position: relative;
            display: inline-block;
        }
        .col-md-8 {
            width: 66%;
            position: relative;
            display: inline-block;
        }
        .col-md-12 {
            width: 97%;
            position: relative;
            display: inline-block;
        }
        .page {
            position: relative;
            page-break-after: always;
            margin: 1cm 2cm 0 2cm;
            padding: 0px 0px 0px 0px;
            overflow: hidden;
            background-color: #ffffff;
        }
        h1{
            font-size: 0.6cm;
            font-weight: bold;
        }
        h2{
            font-size: 0.5cm;
            font-weight: bold;
        }
        h3{
            font-size: 0.3cm;
            font-weight: bold;
        }
        h4 {
            font-size: 0.3cm;
            font-weight: bold;
        }
        h5 {
            font-size: 0.2cm;
        }
        .header-titles{
            padding-left: 10px;
            position: relative;
            display: inline-block;
            margin-top: 10px;
            margin-left: 335px;
        }
        .user-titles{
            padding-left: 10px;
            position: relative;
            display: inline-block;
            margin-top: 10px;
            margin-left: 260px;
        }
        .header-titles h3,.header-titles h4,.header-titles h5{
            line-height: 0.9;
            white-space: pre-line;
            text-transform: uppercase;
        }
        
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        p{
            font-size: 22px;
            text-align: justify;
        }
        .border-card{
            border-top: 3px solid #0036A4;
            border-bottom: 3px solid #0036A4;
            padding-left: 10px;
            height: 480px;
        }
        .border-card1{
            border-left: 3px solid #0036A4;
            border-right: 0px solid #0036A4;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        .border-card2{
            border-right: 3px solid #0036A4;
            border-left: 0px solid #0036A4;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }
        .text-unam{
            padding-left: 60px;
            width: 350px;
            position: relative;
            display: inline-block;
        }
        .data-user{
            padding-left: 30px;
            padding-top: 90px;
            width: 400px;
            position: relative;
            display: inline-block;
        }
        .img-unam{
            position: absolute;
            top: 200px;
            left: 160px;
        }
        .img-ac{
            position: absolute;
            top: 25px;
            right: 115px;
        }
        .img-qr{
            position: absolute;
            top: 10px;
            right: 20px;
        }
        .signing{
            position: absolute;
            width: 100%;
            bottom: 30px;
        }
        .signing h3{
            width: 50%;
            border-top: 3px solid #0036A4;
            margin: 0 auto;
        }
        ul li{
            font-size: 0.4cm;
        }
        .float-right{
            float: right;
        }
        .profile-photo{
            width: 130px;
            position: absolute;
            top: 80px;
            right: 20px;
        }
        .row{
            position: relative;
            height: 600px;
        }
        .text-schedule{
            padding-top: 30px;
            padding-left: 30px;
        }
        .line{
            border-bottom: 2px dashed #000000;
        }
        .img-ticket{
            position: absolute;
            top: 10px;
            left: -10px;
        }
        .img-event{
            position: absolute;
            top: 25px;
            left: 0px;
        }
        .img-unam{
            position: absolute;
            top: 45px;
            left: 260px;
        }
        .img-fesa{
            position: absolute;
            top: 45px;
            left: 640px;
        }
        .img-qr{
            position: absolute;
            top: 280px;
            left: 560px;
        }
    </style>
</head>

<body>
    <div class="page">
        <section class="row">
            <img class="img-ticket" width="760px" height="450px" src="{{storage_path('/app/public/img/events/ticket.png')}}">
            <img class="img-event" width="250px" height="415px" src="{{$event_showing->event->ticket_url}}" alt="">
            <img class="img-unam" width="70px" src="{{storage_path('app/public/svg/logo-unam.png')}}">
            <img class="img-fesa" width="70px" src="{{storage_path('app/public/svg/logo-fesa.png')}}">
            <img class="img-qr" width="150px" src="{{env('SFTP_WEB') . "img/events/qr/-1.svg"}}">
            <h2>BOLETO ELECTRÓNICO (PERSONAL DEL EVENTO)</h2>
            <div class="header-titles text-center">
                <h3>UNIVERSIDAD NACIONAL AUT&Oacute;NOMA DE M&Eacute;XICO</h3>
                <h4>FES - ARAG&Oacute;N</h4>
                <h5>UNIDAD DE EXTENSI&Oacute;N UNIVERSITARIA</h5>
                <h5>DEPARTAMENTO DE ACTIVIDADES CULTURALES</h5>
            </div>
            <div class="user-titles text-left">
                <table>
                    <tbody>
                        <tr>
                            <td class="td-col-title">{{$role->name}}:</td>
                            <td class="td-col-6">{{$user->name}} {{$user->lastname}} {{$user->surname}}</td>
                        </tr>
                        <tr>
                            <td class="td-col-title">Evento:</td>
                            <td class="td-col-6">{{$event_showing->event->title}}</td>
                        </tr>
                        <tr>
                            <td class="td-col-title">Lugar:</td>
                            <td class="td-col-6">{{$event_showing->event->classroom->name}}</td>
                        </tr>
                        <tr>
                            <td class="td-col-title">Fecha y hora:</td>
                            <td class="td-col-6">{{App\Libraries\Dates::changeFormatUTC($event_showing->start, 'd-m-Y H:i')}}</td>
                        </tr>
                        <tr>
                            <td class="td-col-title">Folio:</td>
                            <td class="td-col-6">-1</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
        <section class="line"></section>
        <section class="row">
            <img class="img-ticket" width="760px" height="450px" src="{{storage_path('/app/public/img/events/ticket.png')}}">
            <img class="img-event" width="250px" height="415px" src="{{$event_showing->event->ticket_url}}" alt="">
            <img class="img-unam" width="70px" src="{{storage_path('app/public/svg/logo-unam.png')}}">
            <img class="img-fesa" width="70px" src="{{storage_path('app/public/svg/logo-fesa.png')}}">
            <img class="img-qr" width="150px" src="{{env('SFTP_WEB') . "img/events/qr/-1.svg"}}">
            <h2>BOLETO ELECTRÓNICO (PERSONAL DEL EVENTO)</h2>
            <div class="header-titles text-center">
                <h3>UNIVERSIDAD NACIONAL AUT&Oacute;NOMA DE M&Eacute;XICO</h3>
                <h4>FES - ARAG&Oacute;N</h4>
                <h5>UNIDAD DE EXTENSI&Oacute;N UNIVERSITARIA</h5>
                <h5>DEPARTAMENTO DE ACTIVIDADES CULTURALES</h5>
            </div>
            <div class="user-titles text-left">
                <table>
                    <tbody>
                        <tr>
                            <td class="td-col-title">{{$role->name}}:</td>
                            <td class="td-col-6">{{$user->name}} {{$user->lastname}} {{$user->surname}}</td>
                        </tr>
                        <tr>
                            <td class="td-col-title">Evento:</td>
                            <td class="td-col-6">{{$event_showing->event->title}}</td>
                        </tr>
                        <tr>
                            <td class="td-col-title">Lugar:</td>
                            <td class="td-col-6">{{$event_showing->event->classroom->name}}</td>
                        </tr>
                        <tr>
                            <td class="td-col-title">Fecha y hora:</td>
                            <td class="td-col-6">{{App\Libraries\Dates::changeFormatUTC($event_showing->start, 'd-m-Y H:i')}}</td>
                        </tr>
                        <tr>
                            <td class="td-col-title">Folio:</td>
                            <td class="td-col-6">-1</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</body>

</html>