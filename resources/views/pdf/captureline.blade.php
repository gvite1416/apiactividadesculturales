<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            text-rendering: optimizeLegibility;
            font-family: "Times New Roman", sans-serif;
        }

        .col-md-2 {
            width: 16.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-4 {
            width: 33%;
            position: relative;
            display: inline-block;
        }
        .col-md-6 {
            width: 49.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-8 {
            width: 66%;
            position: relative;
            display: inline-block;
        }
        .col-md-12 {
            width: 100%;
            position: relative;
            display: inline-block;
        }
        .page {
            position: relative;
            page-break-after: always;
            margin: 2cm 0 0 2cm;
            padding: 0;
            width: 20cm;
            height: 29.5cm;
            overflow: hidden;
            background-color: #ffffff;
        }
        h1{
            font-size: 20px;
        }
        h2{
            font-size: 18px;
        }
        h3{
            font-size: 16px;
        }
        .header-titles{
            width: 5cm;
            margin-left: 10px;
            border-left: 5px solid #0036A4;
            padding-left: 10px;
            position: relative;
            display: inline-block;
            color: #0036A4;
        }
        .header-titles h3,.header-titles h4,.header-titles h5{
            line-height: 0.8;
            white-space: pre-wrap;
            margin-bottom: 3px;
            text-transform: uppercase;
        }
        .header-titles h3 {
            font-size: 0.4cm;
            font-weight: bold;
        }
        .header-titles h4 {
            font-size: 0.3cm;
            font-weight: bold;
        }
        .header-titles h5 {
            font-size: 0.3cm;
        }
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        p{
            font-size: 22px;
            text-align: justify;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        table thead tr{
            background-color: #E0E0E0;
        }
        table, th, td{
            border: 1px solid #E0E0E0;
        }
    </style>
</head>

<body>
    <div class="page">
        <img width="100%" src="{{$url_captureline}}?token={{$token}}">
        <div class="col-md-12 text-center">
            <p>La fecha de vigencia indicada en la línea de captura no es tu fecha límite de pago. La fecha límite para efectuar tu pago, y subir a la plataforma tu comprobante de pago, es: <strong>{{App\Libraries\Dates::changeFormatUTC($inscription->expiration, 'd-m-Y')}}</strong></p>
        </div>
    </div>
</body>

</html>