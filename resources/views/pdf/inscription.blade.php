<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            text-rendering: optimizeLegibility;
            font-family: "Times New Roman", sans-serif;
        }

        .col-md-2 {
            width: 16.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-4 {
            width: 33%;
            position: relative;
            display: inline-block;
        }
        .col-md-6 {
            width: 49.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-8 {
            width: 66%;
            position: relative;
            display: inline-block;
        }
        .col-md-12 {
            width: 100%;
            position: relative;
            display: inline-block;
        }
        .page {
            position: relative;
            page-break-after: always;
            margin: 2cm 0 0 2cm;
            padding: 0;
            width: 20cm;
            height: 29.5cm;
            overflow: hidden;
            background-color: #ffffff;
        }
        h1{
            font-size: 20px;
        }
        h2{
            font-size: 18px;
        }
        h3{
            font-size: 16px;
        }
        .header-titles{
            width: 5cm;
            margin-left: 10px;
            border-left: 5px solid #0036A4;
            padding-left: 10px;
            position: relative;
            display: inline-block;
            color: #0036A4;
        }
        .header-titles h3,.header-titles h4,.header-titles h5{
            line-height: 0.8;
            white-space: pre-wrap;
            margin-bottom: 3px;
            text-transform: uppercase;
        }
        .header-titles h3 {
            font-size: 0.4cm;
            font-weight: bold;
        }
        .header-titles h4 {
            font-size: 0.3cm;
            font-weight: bold;
        }
        .header-titles h5 {
            font-size: 0.3cm;
        }
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        p{
            font-size: 22px;
            text-align: justify;
        }
        .p-legal{
            font-size: 15px;
        }
        .ul-legal{
            font-size: 13px;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        table thead tr{
            background-color: #E0E0E0;
        }
        table, th, td{
            border: 1px solid #E0E0E0;
        }
    </style>
</head>

<body>
    <div class="page">
        <div>
            <div class="col-md-4">
                <img width="100px" src="{{storage_path('app/public/svg/logo-unam.png')}}">
            </div>
            <div class="col-md-8 text-right">
                <img width="100px" src="{{storage_path('app/public/svg/logo-fesa.png')}}">
                <img width="100px" src="{{storage_path('app/public/svg/logo-cultural.png')}}">
                <div class="header-titles">
                    <h3 class="text-left">FACULTAD DE ESTUDIOS SUPERIORES ARAGÓN</h3>
                    <h4 class="text-left">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h4>
                    <h5 class="text-left">ACTIVIDADES CULTURALES</h5>
                </div>
            </div>
        </div>
        <div>
            <div class="col-md-12">
                <h1 class="text-center">UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO</h1>
                <h2 class="text-center">FES - ARAGÓN</h2>
                <h3 class="text-center">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h3>
                <h3 class="text-center">DEPARTAMENTO DE ACTIVIDADES CULTURALES</h3>
                <h3 class="text-center">HOJA DE PREINSCRIPCIÓN</h3>
            </div>
        </div>
        <div>
            <div class="col-md-4">
                <img width="150px" src="{{$inscription->user->qr}}?token={{$token}}">
            </div>
            <div class="col-md-8">
                <p class="text-right">Folio: {{$inscription->folio}}</p>
                <p class="text-right">Fecha Expedición: {{App\Libraries\Dates::changeFormatUTC($inscription->created_at , 'd-m-Y H:i')}}</p>
                <p class="text-right">Alumno: {{$inscription->user->name}} {{$inscription->user->lastname}} {{$inscription->user->surname}}</p>
            </div>
        </div>
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Grupo</th>
                        <th>Taller</th>
                        <th>Profesor</th>
                        <th>Salón</th>
                        <th>Horario</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$inscription->workshop_period->group}}</td>
                        <td>{{$inscription->workshop_period->workshop->name}}</td>
                        <td>{{$inscription->workshop_period->teacher->name}} {{$inscription->workshop_period->teacher->lastname}} {{$inscription->workshop_period->teacher->surname}}</td>
                        <td>{{$inscription->workshop_period->classroom->name}}</td>
                        <td>
                            <ul>
                                @foreach ($inscription->workshop_period->schedules as $schedule)
                                    <li>{{App\Libraries\FormatData::scheduleFormat($schedule, true)}}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>{{App\Libraries\FormatData::currencyFormat($inscription->quota + $inscription->extra - $inscription->scholarship)}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>{{App\Libraries\FormatData::currencyFormat($inscription->quota + $inscription->extra - $inscription->scholarship)}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div>
            <p class="p-legal">
                LA APERTURA DEL TALLER ESTÁ SUJETA A LA INSCRIPCIÓN MÍNIMA DE 7 PERSONAS. CUANDO ESTÉ CUBIERTO ESTE CUPO MÍNMO, RECIBIRÁS UNA LÍNEA DE CAPTURA  EN TU CORREO ELECTRÓNICO
            </p>
            <p class="p-legal">AL RECIBIR TU LÍNEA DE CAPTURA:</p>
            <ul class="ul-legal">
                <li>Utiliza tu línea de captura para efectuar tu pago bancario en ventanilla de BBVA o cajero automático BBVA. también tienes la opción de realizar transferencia bancaria bajo la única condición de que el banco de origen sea BBVA. </li>
                <li>DEBERÁS PAGAR EL IMPORTE EXACTO QUE INDICA LA LÍNEA DE CAPTURA, NI MÁS, NI MENOS.</li>
                <li>VERIFICA QUE EN EL VOUCHER O RECIBO DE PAGO QUE TE ENTREGA LA INSTITUCIÓN BANCARIA APAREZCAN REGISTRADOS EXACTAMENTE LA LÍNEA DE CAPTURA  Y EL IMPORTE QUE TE FUE PROPORCIONADO POR LA PLATAFORMA DE LOS TALLERES CULTURALES, EN CASO CONTRARIO NO PODRÁ ACREDITARSE TU PAGO.</li>
            </ul>
            <p class="p-legal">LAS LÍNEAS DE CAPTURA SON PERSONALES, NO TRANSFERIBLES Y VÁLIDAS ÚNICAMENTE PARA UN SOLO TRÁMITE.</p>
            <ul class="ul-legal">
                <li>Una vez realizado el pago, ingresa con tu usuario y contraseña a la plataforma de los Talleres Culturales, en horario de 10:00 a 20:00 horas, para subir los siguientes documentos (en formato de imagen o en pdf): </li>
                <li>
                    <ol class="ul-legal">
                        <li>Identificación que avale tu estatus como alumno(a), exalumno(a), trabajador(a), externo(a), etc. Los alumnos y alumnas que no dispongan de identificación podrán enviar un documento como tira de materias.</li>
                        <li>Recibo bancario de pago efectuado. En el caso de realizar transferencia bancaria, podrá subirse una captura de pantalla.</li>
                        <li>TODAS LAS IMÁGENES DEBERÁN SER NÍTIDAS.</li>
                    </ol>
                </li>
            </ul>
            <p class="p-legal">DISPONES DE 3 DÍAS PARA EFECTUAR EL PAGO Y SUBIR A LA PLATAFORMA LOS DOCUMENTOS REQUERIDOS (EL DÍA QUE RECIBES TU LÍNEA DE CAPTURA CUENTA COMO PRIMER DÍA).</p>
            <ul class="ul-legal">
                <li>Previa verificación de los documentos que hayas subido a la plataforma, se te notificará, vía electrónica, que tu inscripción ha sido efectuada.</li>
            </ul>
            <p class="p-legal">SE CANCELARÁ TU INSCRIPCIÓN EN CASO DE NO CONCLUIR EL PROCEDIMIENTO EN TIEMPO Y FORMA</p>
        </div>
    </div>
</body>

</html>