<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            text-rendering: optimizeLegibility;
            font-family: "Times New Roman", sans-serif;
        }

        .col-md-2 {
            width: 16.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-4 {
            width: 33%;
            position: relative;
            display: inline-block;
        }
        .col-md-6 {
            width: 49.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-8 {
            width: 66%;
            position: relative;
            display: inline-block;
        }
        .col-md-12 {
            width: 100%;
            position: relative;
            display: inline-block;
        }
        .page {
            position: relative;
            page-break-after: always;
            margin: 2cm 0 0 2cm;
            padding: 0;
            width: 20cm;
            height: 29.5cm;
            overflow: hidden;
            background-color: #ffffff;
        }
        h1{
            font-size: 20px;
        }
        h2{
            font-size: 18px;
        }
        h3{
            font-size: 16px;
        }
        .header-titles{
            width: 5cm;
            margin-left: 10px;
            border-left: 5px solid #0036A4;
            padding-left: 10px;
            position: relative;
            display: inline-block;
            color: #0036A4;
        }
        .header-titles h3,.header-titles h4,.header-titles h5{
            line-height: 0.8;
            white-space: pre-wrap;
            margin-bottom: 3px;
            text-transform: uppercase;
        }
        .header-titles h3 {
            font-size: 0.4cm;
            font-weight: bold;
        }
        .header-titles h4 {
            font-size: 0.3cm;
            font-weight: bold;
        }
        .header-titles h5 {
            font-size: 0.3cm;
        }
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        p{
            font-size: 22px;
            text-align: justify;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        table thead tr{
            background-color: #E0E0E0;
        }
        table, th, td{
            border: 1px solid #E0E0E0;
        }
        .page-break {
            page-break-after: always;
        }
        .col-margin{
            margin-top: 20px;
        }
        .span-6{
            width: 400px;
        }
    </style>
</head>

<body>
    <div class="page">
        <div>
            <div class="col-md-4">
                <img width="100px" src="{{storage_path('app/public/svg/logo-unam.png')}}">
            </div>
            <div class="col-md-8 text-right">
                <img width="100px" src="{{storage_path('app/public/svg/logo-fesa.png')}}">
                <img width="100px" src="{{storage_path('app/public/svg/logo-cultural.png')}}">
                <div class="header-titles">
                    <h3 class="text-left">FACULTAD DE ESTUDIOS SUPERIORES ARAGÓN</h3>
                    <h4 class="text-left">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h4>
                    <h5 class="text-left">ACTIVIDADES CULTURALES</h5>
                </div>
            </div>
        </div>
        <div>
            <div class="col-md-12">
                <h1 class="text-center">UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO</h1>
                <h2 class="text-center">FES - ARAGÓN</h2>
                <h3 class="text-center">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h3>
                <h3 class="text-center">DEPARTAMENTO DE ACTIVIDADES CULTURALES</h3>
                <h5 class="text-center">PROGRAMA DE TALENTO ARAGON&Eacute;S</h5>
                <h3 class="text-center">(FICHA DE INSCRIPCI&Oacute;N)</h3>
            </div>
        </div>
        <div class="col-margin">
            <strong for="">Nombre: </strong><span>{{$talent->name}}</span>
        </div>
        <div class="col-margin">
            <strong for="">Carrera: </strong><span>{{$talent->career->name}}</span>
        </div>
        <div class="col-margin">
            <span class="span-6">
                <strong for="">Semestre: </strong><span>{{$talent->semester}}</span>
            </span>
            <span class="span-6">
                <strong for="">No. Cuenta: </strong><span>{{$talent->number_id}}</span>
            </span>
        </div>
        <div class="col-margin">
            <strong for="">Correo electrónico: </strong><span>{{$talent->email}}</span>
        </div>
        <div class="col-margin">
            <span class="span-6">
                <strong for="">Teléfono Celular: </strong><span>{{$talent->celphone}}</span>
            </span>
            <span class="span-6">
                <strong for="">Teléfono Casa: </strong><span>{{$talent->phone}}</span>
            </span>
        </div>
        <div class="col-margin">
            <strong for="">Arte: </strong><span>{{$talent->arts}}</span>
        </div>
        <div class="col-margin">
            <strong for="">Actividad artística: </strong><span>{{$talent->art_activity}}</span>
        </div>
        <div class="col-margin">
            <strong for="">Género: </strong><span>{{$talent->gender}}</span>
        </div>   
        <div class="col-margin">
            <strong for="">Nombre del grupo/solista: </strong><span>{{$talent->band}}</span>
        </div>
        <div class="col-margin">
            <strong>Nombre de los integrantes: </strong>
        </div>
        <ol>
            @if (isset($members) && count($members) > 0)
                @foreach ($members as $member)
                    <li>{{$member}}</li>
                @endforeach
            @endif
        </ol>
    </div>
    <div class="page-break"></div>
    <div class="page">
        <div>
            <div class="col-md-4">
                <img width="100px" src="{{storage_path('app/public/svg/logo-unam.png')}}">
            </div>
            <div class="col-md-8 text-right">
                <img width="100px" src="{{storage_path('app/public/svg/logo-fesa.png')}}">
                <img width="100px" src="{{storage_path('app/public/svg/logo-cultural.png')}}">
                <div class="header-titles">
                    <h3 class="text-left">FACULTAD DE ESTUDIOS SUPERIORES ARAGÓN</h3>
                    <h4 class="text-left">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h4>
                    <h5 class="text-left">ACTIVIDADES CULTURALES</h5>
                </div>
            </div>
        </div>
        <div>
            <div class="col-md-12">
                <h1 class="text-center">UNIVERSIDAD NACIONAL AUTÓNOMA DE MÉXICO</h1>
                <h2 class="text-center">FES - ARAGÓN</h2>
                <h3 class="text-center">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h3>
                <h3 class="text-center">DEPARTAMENTO DE ACTIVIDADES CULTURALES</h3>
                <h5 class="text-center">PROGRAMA DE TALENTO ARAGON&Eacute;S</h5>
                <h3 class="text-center">(FICHA T&Eacute;CNICA)</h3>
            </div>
        </div>
        <div class="col-margin">
            <strong for="">Nombre del grupo/solista: </strong><span>{{$talent->band}}</span>
        </div>
        <div class="col-margin">
            <strong for="">Actividad artística: </strong><span>{{$talent->arts}}</span>
        </div>
        <div class="col-margin">
            <strong for="">Género: </strong><span>{{$talent->gender}}</span>
        </div>
        <h4>Área Técnica</h4>
        <div class="col-margin">
            <strong for="">Duración de la presentación: </strong><span>{{$talent->duration}} mins.</span>
        </div>
        <div class="col-margin"><strong>Listado de equipo (propiedad del participante): </strong></div>
        <ol>
            @if (isset($equipments) && count($equipments) > 0)
                @foreach ($equipments as $equipment)
                    <li>{{$equipment}}</li>
                @endforeach
            @endif
        </ol>
        <div class="col-margin">
            <strong for="">Escenografía: </strong><span>{{$talent->scenography}}</span>
        </div>
        <div class="col-margin">
            <span>
                <strong for="">Iluminación: </strong><span>{{$talent->illumination}}</span>
            </span>
            <span>
                <strong for="">Audio y video: </strong><span>{{$talent->audio_video}}</span>
            </span>
        </div>
        <div class="col-margin">
            <span>
                <strong for="">Montaje: </strong><span>{{$talent->assembly_duration}} mins.</span>
            </span>
            <span>
                <strong for="">Desmontaje: </strong><span>{{$talent->deassembly_duration}} mins.</span>
            </span>
        </div>
        <p><strong>* El Departamento de Actividades Culturales no proporciona ningún tipo de escenografía</strong></p>
        <p><strong>* El Departamento de Actividades Culturales unicamente prestará microfonos para la presentación de las actividades artisticas a realizar en el teatro</strong></p>
        <p><strong>* Presentarse 2 horas antes de la hora indicada de su presentación</strong></p>
        <p><strong>* No habrá ensayos previos a la presentación</strong></p>
    </div>
</body>

</html>