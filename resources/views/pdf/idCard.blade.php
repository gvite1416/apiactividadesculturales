<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
            text-rendering: optimizeLegibility;
            font-family: "Times New Roman", sans-serif;
            color: #0036A4;
        }
        .col-md-1 {
            width: 14%;
            position: relative;
            display: inline-block;
        }
        .col-md-2 {
            width: 16.5%;
            position: relative;
            display: inline-block;
        }
        .col-md-3 {
            width: 24.4%;
            position: relative;
            display: inline-block;
        }
        .col-md-4 {
            width: 33%;
            position: relative;
            display: inline-block;
        }
        .col-md-6 {
            /*width: 370px;*/
            width: 47.7%;
            position: relative;
            display: inline-block;
        }
        .col-md-8 {
            width: 66%;
            position: relative;
            display: inline-block;
        }
        .col-md-12 {
            width: 97%;
            position: relative;
            display: inline-block;
        }
        .page {
            position: relative;
            page-break-after: always;
            margin: 1cm 2cm 0 2cm;
            padding: 0px 0px 0px 0px;
            overflow: hidden;
            background-color: #ffffff;
        }
        h1{
            font-size: 0.5cm;
            font-weight: bold;
        }
        h2{
            font-size: 0.4cm;
            font-weight: bold;
        }
        h3{
            font-size: 0.3cm;
            font-weight: bold;
        }
        h4 {
            font-size: 0.2cm;
            font-weight: bold;
        }
        h5 {
            font-size: 0.15cm;
        }
        .header-titles{
            width: 2.2cm;
            border-left: 5px solid #0036A4;
            padding-left: 10px;
            position: relative;
            display: inline-block;
            margin-top: 10px;
            margin-left: 35px;
        }
        .header-titles h3,.header-titles h4,.header-titles h5{
            line-height: 0.9;
            white-space: pre-line;
            text-transform: uppercase;
        }
        
        .text-center {
            text-align: center;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        p{
            font-size: 22px;
            text-align: justify;
        }
        table{
            width: 100%;
            border-collapse: collapse;
        }
        table thead tr{
            background-color: #E0E0E0;
        }
        table, th, td{
            border: 1px solid #E0E0E0;
        }
        .border-card{
            border-top: 3px solid #0036A4;
            border-bottom: 3px solid #0036A4;
            padding-left: 10px;
            height: 480px;
        }
        .border-card1{
            border-left: 3px solid #0036A4;
            border-right: 0px solid #0036A4;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        .border-card2{
            border-right: 3px solid #0036A4;
            border-left: 0px solid #0036A4;
            border-top-right-radius: 5px;
            border-bottom-right-radius: 5px;
        }
        .text-unam{
            padding-left: 60px;
            width: 350px;
            position: relative;
            display: inline-block;
        }
        .data-user{
            padding-left: 30px;
            padding-top: 90px;
            width: 400px;
            position: relative;
            display: inline-block;
        }
        .img-unam{
            position: absolute;
            top: 200px;
            left: 160px;
        }
        .img-fesa{
            position: absolute;
            top: 25px;
            left: 25px;
        }
        .img-ac{
            position: absolute;
            top: 25px;
            right: 115px;
        }
        .img-qr{
            position: absolute;
            top: 10px;
            right: 20px;
        }
        .signing{
            position: absolute;
            width: 100%;
            bottom: 30px;
        }
        .signing h3{
            width: 50%;
            border-top: 3px solid #0036A4;
            margin: 0 auto;
        }
        ul li{
            font-size: 0.4cm;
        }
        .float-right{
            float: right;
        }
        .profile-photo{
            width: 130px;
            position: absolute;
            top: 80px;
            right: 20px;
        }
        .row{
            position: relative;
        }
        .text-schedule{
            padding-top: 30px;
            padding-left: 30px;
        }
    </style>
</head>

<body>
    <div class="page row">
        <div class="col-md-6 border-card border-card1">
            <img class="img-unam" width="200px" src="{{storage_path('/app/public/svg/logo-unam-opacity.png')}}">
            <img class="img-fesa" width="100px" src="{{storage_path('/app/public/svg/logo-fesa.png')}}">
            <img class="img-ac" width="80px" src="{{storage_path('/app/public/svg/logo-cultural.png')}}">
            <div class="row">
                <h1 class="text-center text-unam">UNIVERSIDAD NACIONAL AUTONOMA DE MÉXICO</h1>
                <div class="header-titles">
                    <h4 class="text-left">FACULTAD DE ESTUDIOS SUPERIORES ARAGÓN</h4>
                    <h5 class="text-left">UNIDAD DE EXTENSIÓN UNIVERSITARIA</h5>
                    <h5 class="text-left">ACTIVIDADES CULTURALES</h5>
                </div>
            </div>
            <div class="row">
                @if($user->photo)
                    <img class="profile-photo" src="{{$user->photo_url}}?token={{$token}}">
                @else
                    <img class="profile-photo" src="{{storage_path('/app/public/img/profiles/default.jpg')}}">
                @endif
                <div class="data-user">
                    <div>
                        <h1 class="text-left">Alumno: {{$user->name}} {{$user->lastname}} {{$user->surname}}</h1>
                        <h2 class="text-left">Tipo: {{$user->roles[0]->name}}</h2>
                        @role('student|exstudent')
                            <h2 class="text-left">Facultad: {{$user->dataStudent[0]->campus->name}}</h2>
                            <h2 class="text-left">Carrera: {{$user->dataStudent[0]->career->name}}</h2>
                            <h2 class="text-left">
                                No. Cuenta: {{$user->number_id}}
                            </h2>
                        @endrole
                        @role('employee')
                        <h2 class="text-left">Área: {{$user->dataEmployee[0]->area}}</h2>
                        <h2 class="text-left">No. Trabajador: {{$user->number_id}}</h2>
                        @endrole
                        <h2 class="text-left">Periodo: {{$period->name}}</h2>
                        <h3 class="text-left">
                            @role('student')
                            Semestre: {{$user->dataStudent[0]->semester}}
                            @endrole
                        </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 border-card border-card2 float-right">
            <img class="img-qr" width="220px" src="{{$user->qr}}?token={{$token}}">
            <div class="row">
                <div class="col-md-12 text-schedule">
                    <h1 class="text-left">Horario:</h1>
                    <h2>{{$inscription->workshop_period->workshop->name}}</h2>
                    <ul>
                        @foreach ($inscription->workshop_period->schedules as $schedule)
                            <li>{{App\Libraries\FormatData::scheduleFormat($schedule , true)}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="signing">
                <h3 class="text-center">Firma Alumno</h3>
            </div>
        </div>
    </div>
</body>

</html>