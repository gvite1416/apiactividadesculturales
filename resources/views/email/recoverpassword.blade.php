@extends('layouts.email')
@section('content')
<table class="main center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            Hola {{ $name }}
        </td>
    </tr>
    <tr>
        <td>
            Estas recibiendo éste correo porque se ha generado una solicitud para cambiar la contraseña de tu cuenta de Actividades Culturales
        </td>
    </tr>
    <tr>
        <td>
            <a href="{{$url_reset_password}}">Cambiar mi contraseña</a>
        </td>
    </tr>
    <tr>
        <td>
            Si no has solicitado el cambio de contraseña favor de hacer caso omiso de este correo.
        </td>
    </tr>
    <tr>
        <td>
            Si no puedes dar click al botón copia y pega el siguiente link.
        </td>
    </tr>
    <tr>
        <td>
            {{$url_reset_password}}
        </td>
    </tr>

</table>
@endsection