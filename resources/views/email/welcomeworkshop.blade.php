@extends('layouts.email')
@section('content')
<table class="main center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            Hola {{ $inscription->user->name }}
        </td>
    </tr>
    <tr>
        <td>
            Bienvenido al taller de {{ $inscription->workshop_period->workshop->name}} del periodo {{$inscription->workshop_period->period->name}}
        </td>
    </tr>
    <tr>
        <td>
            Tu inscripción ha sido completada.
        </td>
    </tr>
    <tr>
        <td>
            Saludos.
        </td>
    </tr>
</table>
@endsection