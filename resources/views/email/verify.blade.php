@extends('layouts.email')
@section('content')
<table class="main center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            Hola {{ $name }}
        </td>
    </tr>
    <tr>
        <td>
            Gracias por registrarte.
        </td>
    </tr>
    <tr>
        <td>
            Por favor validar tu email en el siguiente link
        </td>
    </tr>
    <tr>
        <td>
            <a href="{{$url_verification_code}}">Dar CLICK aquí</a>
        </td>
    </tr>
</table>
@endsection