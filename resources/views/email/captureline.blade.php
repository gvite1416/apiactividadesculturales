@extends('layouts.email')
@section('content')
<table class="main center" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            Hola {{ $inscription->user->name }}
        </td>
    </tr>
    <tr>
        <td>
            Se te informa que tu línea de captura correspondiente al taller de {{$inscription->workshop_period->workshop->name}} ya esta disponible en la plataforma de Actividades Culturales
        </td>
    </tr>
    <tr>
        <td>
            <a href="{{$url_captureline}}">Revisar Línea de captura</a>
        </td>
    </tr>
    <tr>
        <td>
            Recuerda tienes hasta el día {{App\Libraries\Dates::changeFormatUTC($inscription->expiration, 'd-m-Y \a \l\a\s H:i:s')}} para realizar el pago y subir los archivos
        </td>
    </tr>
    <tr>
        <td>
            <ul>
                @foreach ($inscription->workshop_period->period->requirements as $requirement)
                    @if($inscription->user->role->slug == $requirement->role->slug)
                        <li>{{$requirement->name}}</li>
                    @endif
                @endforeach
            </ul>
        </td>
    </tr>
</table>
@endsection