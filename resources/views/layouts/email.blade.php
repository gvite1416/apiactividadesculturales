<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Api</title>
    <style>
        .body-wrap {
        color: #65696d;
        font-family: Arial,"Times New Roman", Times, serif;
        font-size: 18px;
        }
        
        .body-wrap .content {
        margin-top: 70px;
        }
        
        .body-wrap .content .main tr {
        height: 40px;
        }
        
        .body-wrap .footer {
        font-size: 15px;
        color: #87888a;
        }
        
        .body-wrap .text-royal {
        color: #e2001a;
        }
        
        .body-wrap .center {
        text-align: center;
        }

        .body-wrap .justify {
        text-align:  justify;
        }
        
        .body-wrap .td-labrador {
        padding-left: 175px;
        }

        .body-wrap a {
            color: #e2001a;
            text-decoration:none;
        }
        .body-wrap a:active {
            color: #87888a;
        }
              
              
    </style>
</head>
<body>
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container" width="100%">
                <div class="header">
                    <table class="main" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="center">
                                <img src="{{ env('SFTP_WEB') . 'img/logo.png'}}" width="200"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="content">
                    @yield('content')
                </div>
                <div class="footer">
                    <table width="100%">
                        <tr class="justify">
                            <td colspan="2">Este correo fue enviado desde una dirección solamente de notificacionesque no puede atender correo electrónico entrante. Por favor no respondas a este mensaje</td>
                        </tr>
                        <tr class="justify">
                            <td colspan="2">Si tienes alguna duda puedes comunicarte al correo: <a href="mailto:talleresculturales@aragon.unam.mx">talleresculturales@aragon.unam.mx</a></td>
                        </tr>
                        <tr>
                            <td>
                                <img src="{{ env('SFTP_WEB') . 'img/logo.png'}}" width="130"/>
                            </td>
                            <td>
                                Departamento de Actividades Culturales
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td></td>
        </tr>
    </table>
</body>
</html>
