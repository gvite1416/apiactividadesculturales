<?php

namespace App\Libraries;

class FormatData{

    public static function scheduleFormat($schedule , $utc = false){
        $days = ['Domingo', 'Lunes', 'Martes' , 'Miercoles' , 'Jueves' , 'Viernes', 'Sábado'];
        if($utc) {
            return $days[$schedule->day] . ' ' . date ("H:i", strtotime($schedule->start) - 60 * 60 * 5) . ' Hrs. - ' . date ("H:i", strtotime($schedule->finish) - 60 * 60 * 5) . ' Hrs.';
        } else {
            return $days[$schedule->day] . ' ' . date ("H:i", strtotime($schedule->start)) . ' Hrs. - ' . date ("H:i", strtotime($schedule->finish)) . ' Hrs.';
        }
    }
    public static function currencyFormat($money){        
        return '$ ' . number_format($money, 2) . ' MXN';
    }
}