<?php

namespace App\Libraries;
use DateTime;
use DateTimeZone;
class Dates{

    public static function changeFormat($date, $format = "Y-m-d H:i:s" , $utc=true){
        $dt = new DateTime($date, new DateTimeZone("America/Mexico_City"));
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt->setTimezone(new DateTimeZone($zone));
        return $dt->format($format);
    }
    public static function changeFormatUTC($date, $format = "Y-m-d H:i:s" , $utc=false){
        $dt = new DateTime($date, new DateTimeZone("UTC"));
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt->setTimezone(new DateTimeZone($zone));
        return $dt->format($format);
    }
    public static function getTime($date = false, $utc = false){
        $dt = $date === false ? new DateTime() : new DateTime($date, new DateTimeZone("America/Mexico_City"));
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt->setTimezone(new DateTimeZone($zone));
        return $dt->getTimestamp();
    }
    public static function getTimeUTC($date = false, $utc = false){
        $dt = $date === false ? new DateTime() : new DateTime($date, new DateTimeZone("UTC"));
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt->setTimezone(new DateTimeZone($zone));
        return $dt->getTimestamp();
    }
    public static function getTimeUTCAddOneDay($date = false, $utc = false){
        $dt = $date === false ? new DateTime() : new DateTime($date, new DateTimeZone("UTC"));
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt->setTimezone(new DateTimeZone($zone));
        $dt->modify('+1 day');
        return $dt->getTimestamp();
    }
    public static function addNowTwoDays($utc=false) {
        
        $dt = new DateTime();
        $dt->setTimezone(new DateTimeZone("America/Mexico_City"));
        $hourArray = explode(":" , env("HOUR_UPDATE"));
        $dt->setTime($hourArray[0], $hourArray[1], $hourArray[2]);
        if ($dt->format("N") == 5) {
            $dt->modify('+3 day');
        } else {
            $dt->modify('+2 day');
        }
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt->setTimezone(new DateTimeZone($zone));
        return $dt->format('Y-m-d H:i:s');
    }
    public static function addTwoDays($date, $utc=false) {
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt = new DateTime($date, new DateTimeZone($zone));
        $hourArray = explode(":" , env("HOUR_UPDATE"));
        $dt->setTime($hourArray[0], $hourArray[1], $hourArray[2]);
        if ($dt->format("N") == 5) {
            $dt->modify('+3 day');
        } else {
            $dt->modify('+2 day');
        }
        return $dt->format('Y-m-d H:i:s');
    }
    public static function addTwoDaysToTime($date, $utc=false){
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt = new DateTime($date, new DateTimeZone($zone));
        $hourArray = explode(":" , env("HOUR_UPDATE"));
        $dt->setTime($hourArray[0], $hourArray[1], $hourArray[2]);
        if ($dt->format("N") == 5) {
            $dt->modify('+3 day');
        } else {
            $dt->modify('+2 day');
        }
        return $dt->getTimestamp();
    }
    public static function addThreeDaysToTime($date, $utc=false){
        $zone = $utc ? "UTC" : "America/Mexico_City";
        $dt = new DateTime($date, new DateTimeZone($zone));
        $hourArray = explode(":" , env("HOUR_UPDATE"));
        $dt->setTime($hourArray[0], $hourArray[1], $hourArray[2]);
        if ($dt->format("N") == 5) {
            $dt->modify('+4 day');
        } else {
            $dt->modify('+3 day');
        }
        return $dt->getTimestamp();
    }

    public static function getMonthName($monthId){
        $months = [
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        ];
        return $months[$monthId];
    }
}