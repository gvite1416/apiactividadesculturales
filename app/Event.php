<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * @OA\Schema(
 *      title="Event",
 *      description="Event model",
 *      @OA\Xml(
 *          name="Event"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="title", type="string", example=""),
 *      @OA\Property(property="slug", readOnly="true", type="string", example=""),
 *      @OA\Property(property="image", type="string", example=""),
 *      @OA\Property(property="all_campus", type="boolean", example="true"),
 *      @OA\Property(property="thumbnail", type="string", example=""),
 *      @OA\Property(property="ticketimage", type="string", example=""),
 *      @OA\Property(property="description", type="string", example=""),
 *      @OA\Property(property="inscription_start", type="string", format="date-time"),
 *      @OA\Property(property="inscription_finish", type="string", format="date-time"),
 *      @OA\Property(property="quota", type="integer", example=""),
 *      @OA\Property(property="init_folio", type="integer", example=""),
 *      @OA\Property(property="event_category_id", type="integer", example="1"),
 *      @OA\Property(property="autopdf", type="boolean", example="1 or true"),
 *      @OA\Property(property="classroom_id", type="integer", example="1"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */
class Event extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','slug','image','all_campus','one_more_time', 'init_folio','thumbnail','ticketimage','description','inscription_start','inscription_finish','quota','event_category_id','classroom_id','autopdf'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image_url','thumbnail_url','ticket_url'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    function event_category (){
        return $this->belongsTo('App\EventCategory');
    }
    function classroom (){
        return $this->belongsTo('App\Classroom');
    }
    function event_students (){
        return $this->hasMany('App\EventStudent');
    }
    function offerings (){
        return $this->hasMany('App\Offering');
    }
    function showings (){
        return $this->hasMany('App\EventShowing');
    }
    function students () {
        return $this->belongsToMany('App\Role', 'event_students', 'event_id', 'role_id')->withPivot('id', 'price','limit');
    }
    function campus() {
        return $this->belongsToMany('App\Campus', 'event_campus', 'event_id', 'campus_id')->withTimestamps();
    }
    function event_tickets (){
        return $this->hasMany('App\EventTicket');
    }
    /**
     * Get the Photo.
     *
     * @param  string  $value
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return ($this->attributes['image']) ? Storage::disk("public")->url( "img/events/" . $this->attributes['image']) : $this->attributes['image'];
    }
    /**
     * Get the Photo.
     *
     * @param  string  $value
     * @return string
     */
    public function getThumbnailUrlAttribute()
    {
        return ($this->attributes['thumbnail']) ? Storage::disk("public")->url("img/events/" . $this->attributes['thumbnail']) : $this->attributes['thumbnail'];
    }
    /**
     * Get the Photo ticket.
     *
     * @param  string  $value
     * @return string
     */
    public function getTicketUrlAttribute()
    {
        return ($this->attributes['ticketimage']) ? Storage::disk("public")->url("img/events/" . $this->attributes['ticketimage']) : $this->attributes['ticketimage'];
    }
}
