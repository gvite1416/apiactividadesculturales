<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *      title="Period",
 *      description="Period model",
 *      @OA\Xml(
 *          name="Period"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="name", type="string", example="Name"),
 *      @OA\Property(property="start", type="string", format="date-time"),
 *      @OA\Property(property="finish", type="string", format="date-time"),
 *      @OA\Property(property="inscription_start", type="string", format="date-time"),
 *      @OA\Property(property="inscription_finish", type="string", format="date-time"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */

class Period extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','start','finish','inscription_start','inscription_finish'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'periods';

    function workshops_period (){
        return $this->hasMany('App\WorkshopPeriod');
    }
    function workshops (){
        return $this->belongsToMany('App\Workshop', 'workshop_periods', 'period_id', 'workshop_id')->withTimestamps()->withPivot('id','teacher_id','classroom_id','quota','group', 'deleted_at');
    }
    function requirements (){
        return $this->belongsToMany('App\Requirement', 'period_requirements', 'period_id', 'requirement_id')->withTimestamps()->withPivot('id','period_id','requirement_id');
    }
    public function scopeCurrent($query){
        $time = strtotime(date("Y-m-d H:i:s"));
        return $query->where('start','<=',date("Y-m-d H:i:s", $time))->where('finish' , '>=',date("Y-m-d H:i:s",$time))->limit(1);
    }
}
