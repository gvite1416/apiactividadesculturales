<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
class UserPeriodRequirement extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','period_requirement_id','inscription_id','file', 'mime'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_period_requirements';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    /**
     * Get the Img.
     *
     * @param  string  $value
     * @return string
     */
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    
    protected $appends = ['file_url'];
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function period_requirement(){
        return $this->belongsTo('App\PeriodRequirement');
    }
    public function getFileUrlAttribute()
    {
        return ($this->attributes['file']) ? Storage::disk("users")->url($this->user->id . "/" . $this->attributes['file']) : $this->attributes['file'];
    }
}