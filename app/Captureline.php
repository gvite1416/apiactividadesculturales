<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * @OA\Schema(
 *      title="Captureline",
 *      description="Captureline model",
 *      @OA\Xml(
 *          name="Captureline"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="pathname", readOnly="true", type="string"),
 *      @OA\Property(property="agreement", type="string", description="optional"),
 *      @OA\Property(property="reference", type="string", description="optional"),
 *      @OA\Property(property="date_valid", type="string", format="date-time"),
 *      @OA\Property(property="value", type="integer", example="360"),
 *      @OA\Property(property="inscription_id", readOnly="true", type="integer", example="1"),
 *      @OA\Property(property="period_id", readOnly="true", type="integer", example="1"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */
class Captureline extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pathname', 'agreement', 'reference', 'date_valid','value','inscription_id','period_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    
    protected $appends = ['img_url'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'capturelines';

    public function inscription(){
        return $this->hasOne('App\Inscription');
    }

    public function period(){
        return $this->hasOne('App\Period');
    }
    /**
     * Get the Img.
     *
     * @param  string  $value
     * @return string
     */
    public function getImgUrlAttribute()
    {
        return (isset($this->attributes['pathname'])) ? Storage::disk("capturelines")->url($this->attributes['pathname']) : null;
    }
}
