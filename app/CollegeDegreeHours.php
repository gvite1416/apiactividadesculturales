<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CollegeDegreeHours extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start','finish','inscription_id','total_hours'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'college_degree_hours';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
    /**
     * Get the Img.
     *
     * @param  string  $value
     * @return string
     */
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    
    public function inscription(){
        return $this->belongsTo('App\Inscription');
    }
}