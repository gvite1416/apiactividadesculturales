<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;

/**
 * @OA\Schema(
 *      title="Workshop",
 *      description="Workshop model",
 *      @OA\Xml(
 *          name="Workshop"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="name", type="string", example="Name"),
 *      @OA\Property(property="slug", type="string", readOnly="true", example=""),
 *      @OA\Property(property="objective", type="string"),
 *      @OA\Property(property="requirement", type="string"),
 *      @OA\Property(property="information", type="string"),
 *      @OA\Property(property="image", type="string"),
 *      @OA\Property(property="one_more_time", type="boolean"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */

class Workshop extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','slug','objective','requirement','information','image','one_more_time'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workshops';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image_url'];

    public function periods(){
        return $this->belongsToMany('App\Period', 'workshop_periods', 'workshop_id', 'period_id');
    }
    function workshops_periods (){
        return $this->hasMany('App\WorkshopPeriod');
    }


    /**
     * Get the Photo.
     *
     * @param  string  $value
     * @return string
     */
    public function getImageUrlAttribute()
    {
        return ($this->attributes['image']) ? Storage::disk("public")->url("img/workshops/" . $this->attributes['image']) : $this->attributes['image'];
    }
}
