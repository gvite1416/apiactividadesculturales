<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserRequirement extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','requirement_id','file'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_requirements';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function user (){
        return $this->belongsTo('App\User');
    }

    function requirement (){
        return $this->belongsTo('App\Requirement');
    }
}
