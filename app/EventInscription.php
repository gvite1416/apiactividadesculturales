<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventInscription extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_showing_id','user_id','folio','ticket_folio','ticket_date','quota','get_into_date','status','event_ticket_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_inscriptions';

    function event_showing (){
        return $this->belongsTo('App\EventShowing');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function event_ticket(){
        return $this->belongsTo('App\EventTicket');
    }
}
