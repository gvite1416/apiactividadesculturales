<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * @OA\Schema(
 *      title="Slider",
 *      description="Slider model",
 *      @OA\Xml(
 *          name="Slider"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="title", type="string", example="Name"),
 *      @OA\Property(property="img_desktop", type="string", example="img_desktop.jpg"),
 *      @OA\Property(property="img_mobile", type="string", example="img_mobile.jpg"),
 *      @OA\Property(property="link", type="string", example="link"),
 *      @OA\Property(property="order", type="integer", example="1"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */

class Slider extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','img_desktop','img_mobile','link','order'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sliders';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['img_desktop_url', 'img_mobile_url'];

    /**
     * Get the Photo.
     *
     * @param  string  $value
     * @return string
     */
    public function getImgDesktopUrlAttribute()
    {
        return ($this->attributes['img_desktop']) ? Storage::disk("public")->url("img/sliders/" . $this->attributes['img_desktop']) : $this->attributes['img_desktop'];
    }
    /**
     * Get the Photo.
     *
     * @param  string  $value
     * @return string
     */
    public function getImgMobileUrlAttribute()
    {
        return ($this->attributes['img_mobile']) ? Storage::disk("public")->url("img/sliders/" . $this->attributes['img_mobile']) : $this->attributes['img_mobile'];
    }
}
