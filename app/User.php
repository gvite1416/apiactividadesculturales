<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Tymon\JWTAuth\Contracts\JWTSubject;

use HttpOz\Roles\Traits\HasRole;
use HttpOz\Roles\Contracts\HasRole as HasRoleContract;
use Illuminate\Support\Facades\Storage;

/**
 * @OA\Schema(
 *      title="User",
 *      description="User model",
 *      @OA\Xml(
 *          name="User"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="name", type="string", example="Name"),
 *      @OA\Property(property="lastname", type="string", example="lastname"),
 *      @OA\Property(property="surname", type="string", example="surname"),
 *      @OA\Property(property="number_id", type="string", example="XXXXXX"),
 *      @OA\Property(property="email", type="string", example="example@example.com"),
 *      @OA\Property(property="password", type="string", example="Name"),
 *      @OA\Property(property="phone", type="string", example="phone"),
 *      @OA\Property(property="gender", type="string", example="Any"),
 *      @OA\Property(property="celphone", type="string", example="celphone"),
 *      @OA\Property(property="photo", type="string", example="img_desktop.jpg"),
 *      @OA\Property(property="order", type="integer", example="1"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */

class User extends Authenticatable implements JWTSubject,HasRoleContract
{
    use Notifiable;
    use SoftDeletes;
    use HasRole;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_verified', 'is_verified_dept', 'lastname', 'surname', 'number_id', 'phone', 'celphone', 'gender', 'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    
    protected $appends = ['qr','photo_url', 'role'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function dataStudent(){
        return $this->hasMany('App\DataStudent');
    }
    public function dataEmployee(){
        return $this->hasMany('App\DataEmployee');
    }
    public function dataExternal(){
        return $this->hasMany('App\DataExternal');
    }
    public function roles(){
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }
    public function inscriptions() {
        return $this->hasMany('App\Inscription');
    }
    public function event_inscriptions() {
        return $this->hasMany('App\EventInscription');
    }
    public function user_period_requirements() {
        return $this->hasMany('App\UserPeriodRequirement');
    }

    /**
     * Get the user's name.
     *
     * @param  string  $value
     * @return string
     */
    public function getNameAttribute($value)
    {
        return mb_convert_case(mb_strtolower($value), MB_CASE_TITLE , 'UTF-8');
    }

    /**
     * Get the user's last name.
     *
     * @param  string  $value
     * @return string
     */
    public function getLastnameAttribute($value)
    {
        return mb_convert_case(mb_strtolower($value), MB_CASE_TITLE , 'UTF-8');
    }

    /**
     * Get the user's sur name.
     *
     * @param  string  $value
     * @return string
     */
    public function getSurnameAttribute($value)
    {
        return mb_convert_case(mb_strtolower($value), MB_CASE_TITLE , 'UTF-8');
    }
    
    /**
     * Get the Qr.
     *
     * @return string
     */
    public function getQrAttribute()
    {
        return Storage::disk("users")->url($this->attributes['id'] . "/qr.svg");
    }

    /**
     * Get the Qr.
     *
     * @return string
     */
    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    /**
     * Get the Photo.
     *
     * @param  string  $value
     * @return string
     */
    public function getPhotoUrlAttribute()
    {
        return ($this->attributes['photo']) ? Storage::disk("users")->url($this->attributes['id'] . "/" . $this->attributes['photo']) : $this->attributes['photo'];
    }
}
