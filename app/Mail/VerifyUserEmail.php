<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyUserEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $url_verification_code;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $url_verification_code)
    {
        $this->name = $name;
        $this->url_verification_code = $url_verification_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Verifica tu email")
            ->view('email.verify');
    }
}
