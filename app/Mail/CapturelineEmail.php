<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CapturelineEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $inscription;
    public $url_captureline;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inscription, $url_captureline)
    {
        $this->inscription = $inscription;
        $this->url_captureline = $url_captureline;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Línea de Captura")
            ->view('email.captureline');
    }
}
