<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecoverPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $url_reset_password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $url_reset_password)
    {
        $this->name = $name;
        $this->url_reset_password = $url_reset_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Recupera tu contraseña")
            ->view('email.recoverpassword');
    }
}
