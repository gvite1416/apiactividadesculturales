<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DataStudent extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'year','semester','career_id','campus_id','user_id','system'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'data_students';

    public function campus(){
        return $this->belongsTo('App\Campus');
    }

    public function career(){
        return $this->belongsTo('App\Career');
    }
    
    public function student(){
        return $this->belongsTo('App\User');
    }
}
