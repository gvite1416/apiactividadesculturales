<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventStudent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id','role_id','limit', 'price'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_students';

    function role (){
        return $this->belongsTo('App\Role');
    }
}
