<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="Role",
 *      description="Role model",
 *      @OA\Xml(
 *          name="Role"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="name", type="string", example="Name"),
 *      @OA\Property(property="slug", type="string", readOnly="true", example=""),
 *      @OA\Property(property="description", type="string"),
 *      @OA\Property(property="group", type="string"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','slug','description','group'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'group'
    ];
}
