<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkshopPeriodStudent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workshop_period_id','role_id','limit', 'price'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workshop_period_students';

    function role (){
        return $this->belongsTo('App\Role');
    }
}
