<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *      title="EventCategory",
 *      description="EventCategory model",
 *      @OA\Xml(
 *          name="EventCategory"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="name", type="string", example="Concierto"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */
class EventCategory extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_categories';

    public function events(){
        return $this->hasMany('App\Event');
    }
}
