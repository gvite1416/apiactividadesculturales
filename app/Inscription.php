<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *      title="Inscription",
 *      description="Inscription model",
 *      @OA\Xml(
 *          name="Inscription"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="workshop_period_id", type="integer", example="1"),
 *      @OA\Property(property="user_id", type="integer", example="1"),
 *      @OA\Property(property="captureline_id", type="integer", example="1"),
 *      @OA\Property(property="user_validate_id", type="integer", example="1"),
 *      @OA\Property(property="folio", type="string", readOnly="true", example="000"),
 *      @OA\Property(property="ticket_folio", type="string", example="0"),
 *      @OA\Property(property="quota", type="integer", example="360"),
 *      @OA\Property(property="extra", type="integer", example="0"),
 *      @OA\Property(property="scholarship", type="integer", example="0"),
 *      @OA\Property(property="college_degree_option", type="integer", example="0"),
 *      @OA\Property(property="status", type="integer", example="0"),
 *      @OA\Property(property="expiration", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */
class Inscription extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workshop_period_id','user_id', 'captureline_id','folio','ticket_folio','ticket_date','quota','extra','scholarship','status', 'created_at', 'expiration','user_validate_id','college_degree_option'
    ];
    /**
     * Status Values
     * 0 Pre-Inscription
     * 1 Valid Inscription
     * 2 Penalized Inscription
     * 3 Failed or delete Inscription
     * 4 captureline send
     * 5 files validation upload
     * 6 revalidate files
     */

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'inscriptions';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    function workshop_period (){
        return $this->belongsTo('App\WorkshopPeriod');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function user_validate(){
        return $this->belongsTo('App\User');
    }
    public function captureline(){
        return $this->belongsTo('App\Captureline');
    }
}
