<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\CollegeDegreeHours;
use App\Inscription;
use App\Libraries\Dates;

class CollegeDegreeHoursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->query('limit', 10);
        $page = $request->query('page', 1);
        $totalRegister = CollegeDegreeHours::count();
        $college = CollegeDegreeHours::with("inscription")->skip($limit * ($page - 1))->take($limit)->get();
        return response()->json($college)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $inscription_id
     * @return \Illuminate\Http\Response
     */
    public function GetByInscriptionAdmin(Request $request, $inscription_id)
    {
        $limit = $request->query('limit', 10);
        $page = $request->query('page', 1);
        $totalRegister = CollegeDegreeHours::where("inscription_id", "=", $inscription_id)->count();
        $college = CollegeDegreeHours::where("inscription_id", "=", $inscription_id)->with("inscription")->skip($limit * ($page - 1))->take($limit)->get();
        return response()->json($college)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer  $inscription_id
     * @return \Illuminate\Http\Response
     */
    public function GetByInscription(Request $request, $inscription_id)
    {
        $user = auth()->user();
        $inscription = Inscription::find($inscription_id);
        if($user->id == $inscription->user->id){
            $limit = $request->query('limit', 10);
            $page = $request->query('page', 1);
            $totalRegister = CollegeDegreeHours::where("inscription_id", "=", $inscription_id)->count();
            $college = CollegeDegreeHours::where("inscription_id", "=", $inscription_id)->with("inscription")->skip($limit * ($page - 1))->take($limit)->get();
            return response()->json($college)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
        } else {
            return response()->json(['success' => false, 'message' => 'inscripción no encontrada'], 404);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'inscription_id' => 'required|exists:inscriptions,id',
            'start' => 'required|date',
            'finish' => 'required|date',
            'total_hours' => 'required|integer'
        ];
        $input = $request->only(
            'inscription_id',
            'start',
            'finish',
            'total_hours'
        );
        $messages = [
            'inscription_id.required' => 'Se requiere inscripción',
            'inscription_id.exists' => 'La inscripción no existe',
            'start.required' => 'Fecha de entrada requerida',
            'start.date' => 'Fecha de entrada no válida',
            'finish.required' => 'Fecha de salida requerida',
            'finish.date' => 'Fecha de salida no válida',
            'total_hours.required' => 'Total de horas requeridas',
            'total_hours.integer' => 'El total de horas debe ser un número entero'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        CollegeDegreeHours::create([
            'inscription_id' => $request->inscription_id,
            'start' => $request->start,
            'finish' => $request->finish,
            'total_hours' => $request->total_hours
        ]);
        return response()->json(['success'=> true, 'message'=> 'Entrada registrada']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $collegeDegree = CollegeDegreeHours::find($id);
        if ($collegeDegree){
            $rules = [
                'inscription_id' => 'required|exists:inscriptions,id',
                'start' => 'required|date',
                'finish' => 'required|date',
                'total_hours' => 'required|integer'
            ];
            $input = $request->only(
                'inscription_id',
                'start',
                'finish',
                'total_hours'
            );
            $messages = [
                'inscription_id.required' => 'Se requiere inscripción',
                'inscription_id.exists' => 'La inscripción no existe',
                'start.required' => 'Fecha de entrada requerida',
                'start.date' => 'Fecha de entrada no válida',
                'finish.required' => 'Fecha de salida requerida',
                'finish.date' => 'Fecha de salida no válida',
                'total_hours.required' => 'Total de horas requeridas',
                'total_hours.integer' => 'El total de horas debe ser un número entero'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            $collegeDegree->inscription_id = $request->inscription_id;
            $collegeDegree->start = $request->start;
            $collegeDegree->finish = $request->finish;
            $collegeDegree->total_hours = $request->total_hours;
            $collegeDegree->save();

            /*$inscription = Inscription::find($request->inscription_id);
            foreach($inscription->workshop_period->schedules as $schedule){
                if(date("w") == $schedule->day){
                    $diffReal = strtotime(date("Y-m-d") . " " . $schedule->start) - strtotime(date("Y-m-d") . " " . $schedule->finish);
                    $diffStudent = strtotime(date("Y-m-d") . " " . $request->start) - strtotime(date("Y-m-d") . " " . $request->finish);
                    if($diffReal*.8 >= $diffStudent){
                        
                    }
                }
            }
            CollegeDegreeHours::create([
                'inscription_id' => $request->inscription_id,
                'start' => $request->start,
                'finish' => $request->finish
            ]);*/
            return response()->json(['success'=> true, 'message'=> 'Horas registradas']);
        } else {
            return response()->json(['success' => false, 'message' => 'No se encuentran las horas'], 404);
        }
    }

    /**
     * start a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function start(Request $request)
    {
        $rules = [
            'inscription_id' => 'required|exists:inscriptions,id'
        ];
        $input = $request->only(
            'inscription_id'
        );
        $messages = [
            'inscription_id.required' => 'Se requiere inscripción',
            'inscription_id.exists' => 'La inscripción no existe',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $inscription = Inscription::find($request->inscription_id);
        if ($inscription->status == 1 && $inscription->college_degree_option == 1){
            $start = Dates::changeFormatUTC(date("Y-m-d H:i:s", Dates::getTime()));
            $dayStart = (int)date("w", Dates::getTimeUTC($start));
            $diff = 99999999;
            $startSchedule = '';
            foreach($inscription->workshop_period->schedules as $schedule){
                if($dayStart == (int)$schedule->day){
                    $startSchedule = Dates::changeFormatUTC(date("y-m-d") . " " . $schedule->start);
                    $diff = abs(strtotime($startSchedule) - strtotime($start)) / 60;
                }
            }
            if ($diff <= 15){
                CollegeDegreeHours::create([
                    'inscription_id' => $request->inscription_id,
                    'start' => Dates::changeFormat($start)
                ]);
                return response()->json(['success'=> true, 'message'=> 'Entrada registrada', 'diff' => $diff, 'start' => $start, 'startUTC' => Dates::changeFormat($start)]);
            } else {
                return response()->json(['success'=> false, 'message' => '']);
            }
        } else {
            return response()->json(['success'=> false, 'message'=> 'Iscripción no válida']);
        }
        // return response()->json(['success'=> false, 'inscription'=> $inscription]);
    }
    /**
     * finish the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function finish(Request $request, $id)
    {
        $collegeDegree = CollegeDegreeHours::find($id);
        if ($collegeDegree){
            foreach($collegeDegree->inscription->workshop_period->schedules as $schedule){
                if(date("w") == $schedule->day){
                    $diffReal = strtotime(date("Y-m-d") . " " . $schedule->start) - strtotime(date("Y-m-d") . " " . $schedule->finish);
                    $diffStudent = strtotime(date("Y-m-d") . " " . $collegeDegree->start) - strtotime(date("Y-m-d H:i:s"));
                    if($diffReal*.8 >= $diffStudent){
                        
                    }
                }
            }
            $collegeDegree->finish = $request->finish;
            $collegeDegree->total_hours = $request->total_hours;
            $collegeDegree->save();
            return response()->json(['success'=> true, 'message'=> 'Horas registradas']);
        } else {
            return response()->json(['success' => false, 'message' => 'No se encuentran las horas'], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
