<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
/**
* @OA\Info(title="API REST Actividades Culturales", version="1.0")
*
* @OA\Server(url="http://api-ac.local/apiactividadesculturales")
* @OA\Tag(
*     name="Auth",
*     description="API Endpoints of Authentication"
* )
* @OA\Tag(
*     name="Campus",
*     description="API Endpoints of Campus"
* )
* @OA\Tag(
*     name="Capturelines",
*     description="API Endpoints of Capturelines"
* )
* @OA\Tag(
*     name="Careers",
*     description="API Endpoints of Careers"
* )
* @OA\Tag(
*     name="Classrooms",
*     description="API Endpoints of Classrooms"
* )
* @OA\Tag(
*     name="EventCategories",
*     description="API Endpoints of Event Categories"
* )
* @OA\Tag(
*     name="Events",
*     description="API Endpoints of Events"
* )
* @OA\Tag(
*     name="Inscriptions",
*     description="API Endpoints of Inscriptions"
* )
*/
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
