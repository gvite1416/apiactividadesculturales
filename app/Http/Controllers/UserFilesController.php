<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

class UserFilesController extends Controller
{
    public function index(Request $request, $user_id, $file){
        if (strpos($file, '.') !== false) {
            $user = auth()->user();
            if ($user && ($user->isRole('admin') || $user->id == $user_id)) {
                /** Serve the file for the Admin and self user*/
                return $this->returnFile($user_id, $file);
            } else {
                abort(404);
            }
        } else {
            return abort(404);
        }
    }
    private function returnFile($user_id, $file)
    {
        //This method will look for the file and get it from drive
        $path = storage_path("app/users/$user_id/$file");
        try {
            /*$file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);*/
            return response()->file($path);
        } catch (FileNotFoundException $exception) {
            abort(404);
        }
    }
    public function list(){
        $files = Storage::disk('public')->allFiles('');
        dd($files);
    }
    public function movesvg(Request $request){
        $files = Storage::disk('public')->allFiles('img/profiles');
        // dd($files);
        foreach($files as $file) {
            $dirfile = explode("/", $file);
            Storage::disk('users')->put($dirfile[2] . "/" . $dirfile[4], Storage::disk('public')->get($file));
        }
        // Storage::disk('public')->delete($files);
        return response()->json(["success" => true]);
    }
    public function moverequirements(Request $request){
        $files = Storage::disk('public')->allFiles('requirements');
        // dd($files);
        foreach($files as $file) {
            $dirfile = explode("/", $file);
            Storage::disk('users')->put($dirfile[1] . "/" . $dirfile[2], Storage::disk('public')->get($file));
        }
        // Storage::disk('public')->delete($files);
        return response()->json(["success" => true]);
    }
}
