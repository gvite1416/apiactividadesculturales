<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventTicketTicket;
use App\Libraries\Dates;
use Validator;
use Illuminate\Support\Str as Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Crypt;
use PDF;
use App\Libraries\FormatData;

class EventTicketController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/event-ticket",
     *      operationId="getEventTicketList",
     *      tags={"EventTicketTickets"},
     *      summary="Display a listing of the event",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show EventTicket.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/EventTicket")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:title,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be title,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'file');
        $user = auth()->user();
        $totalRegister = EventTicket::count();
        if ($limit == -1){
            $event_tickets = EventTicket::orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->get();
        } else {
            $event_tickets = EventTicket::orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }
        return response()->json($event_tickets)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/event-ticket",
     *      summary="Store an event ticket",
     *      description="Store an event ticket object",
     *      operationId="storeEventTicket",
     *      tags={"EventTicketTickets"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/EventTicket")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'ticket' => 'file|mimes:pdf',
            'event_id' => 'required|exists:event,id'
        ];
        $input = $request->only(
            'ticket',
            'event_id',
            'event_inscription_id'
        );
        $messages = [
            'ticket.mimes' => "Formato no válido",
            'ticket.file' => 'Se requiere archivo',
            'event_id.required' => 'Se requiere evento',
            'event_id.exists' => 'No existe ese evento'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $event_ticket = Event::find($request->event_id);
        if(!Storage::disk("event_tickets")->exists("$event_ticket->id")){
            Storage::disk("event_tickets")->makeDirectory("$event_ticket->id", 0775, true); //creates directory
        }
        $path = Storage::disk("event_tickets")->putFile($event_ticket->id, $request->file('ticket'));
        $event_ticket = EventTicket::create([
            'ticket' => $path,
            'event_id' => $request->event_id
        ]);
        return response()->json(['message'=> 'Ticket registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/event-ticket/{id}",
     *      operationId="getEventTicket",
     *      tags={"EventTicketTickets"},
     *      summary="Display an event ticket",
     *      @OA\Parameter(
     *          description="ID of event",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show an event.",
     *          @OA\JsonContent(ref="#/components/schemas/EventTicket")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event_ticket = EventTicket::find($id);
        if($event_ticket){
            return response()->json($event_ticket);
        }else{
            return response()->json(['success' => false, 'message' => 'Ticket no encontrado'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/event-ticket/{id}",
     *      summary="Delete an event ticket",
     *      description="Delete an event ticket object",
     *      operationId="deleteEventTicket",
     *      tags={"EventTicketTickets"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of EventTicket",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $event_ticket = EventTicket::find($id);
        if ($event_ticket) {
            $event_ticket->delete();
            return response()->json(['message'=> 'EventTicketo eliminada']);
        } else {
            return response()->json(['message'=> 'EventTicketo no encontrada'], 404);
        }
    }
}