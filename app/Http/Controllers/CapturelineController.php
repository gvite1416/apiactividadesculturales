<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Captureline;
use Illuminate\Validation\Rule;
use App\Libraries\Dates;
use Illuminate\Support\Facades\Storage;

class CapturelineController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/captureline",
     *      operationId="getCapturelineList",
     *      tags={"Capturelines"},
     *      summary="Display a listing of the capturelines",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Filter by Value captureline",
     *          in="query",
     *          name="value",
     *          required=false,
     *          example="360",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Filter if captureline is in use - Enum: all,used,notused",
     *          in="query",
     *          name="use",
     *          required=false,
     *          example="all",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Filter by Period id",
     *          in="query",
     *          name="period_id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show capturelines.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Captureline")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:value,created_at,updated_at',
            'value' => 'nullable|integer',
            'use' => 'nullable|in:all,notused,used',
            'period_id' => 'nullable|exists:periods,id',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy',
            'value',
            'use',
            'period_id'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'value.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be value,created_at or updated_at',
            'orderBy.use' => 'orderBy must be all,notused,used',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $user = auth()->user();
        $limit = $request->query('limit', 1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'value');
        $value = $request->query('value', -1);
        $filterUse = $request->query('use', 'all');
        $period_id = $request->query('period_id', -1);
        $totalRegister = 0;
        if($user && $user->group() == 'depto'){
            $capturelineModel = Captureline::with("inscription")->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc');
            if($value != -1){
                $capturelineModel->where("value", "=", $value);
            }
            if($period_id != -1){
                $capturelineModel->where("period_id", "=", $period_id);
            }
            if($filterUse == 'used'){
                $capturelineModel->has('inscription');
            } else if($filterUse == 'notused'){
                $capturelineModel->doesntHave('inscription');
            }
            $totalRegister = $capturelineModel->count();
            $capturelines = $capturelineModel->skip($limit * ($page - 1))->take($limit)->get();
            $valuesDistinct = Captureline::select("value")->distinct()->get();
            $values = [];
            foreach($valuesDistinct as $e){
                $values[] = $e->value;
            }
        } else {
            $totalRegister = Captureline::count();
            $capturelines = Captureline::with("inscription")->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }

        return response()->json($capturelines)->header('X-Capturelines-Values', json_encode($values))->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/captureline",
     *      summary="Store a captureline",
     *      description="Store a captureline object",
     *      operationId="storeCaptureline",
     *      tags={"Capturelines"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Captureline")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'captureline' => 'file|mimes:jpg,png,jpeg',
            'value' => 'required|integer|min:1',
            'date_valid' => 'required|date',
            'period_id' => 'required|exists:periods,id',
            'agreement' => 'nullable',
            'reference' => 'nullable',
        ];
        $input = $request->only(
            "captureline",
            "value",
            "date_valid",
            "period_id",
            "agreement",
            "reference"
        );
        $messages = [
            'captureline.mimes' => "Formato no válido",
            'captureline.file' => "Archivo requerido",
            'value.required' => "Se requiere valor",
            'value.integer' => "El valor debe ser entero",
            'value.min' => "El valor debe ser mínimo 1",
            'date_valid.required' => "La fecha de validación es requerida",
            'date_valid.date' => "Se requiere una fecha válida",
            'period_id.required' => "Se requiere periodo",
            'period_id.exist' => "El periodo no existe",
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $path = Storage::disk("capturelines")->putFile('', $request->file('captureline'));
        Captureline::create([
            'pathname' => $path,
            'value' => $request->value,
            'date_valid' => Dates::changeFormat($request->date_valid),
            'period_id' => $request->period_id,
            'agreement' => $request->agreement,
            'reference' => $request->reference
        ]);
        return response()->json(['message'=> 'Captura de línea registrada']);
    }

    /**
     * @OA\Delete(
     *      path="/api/captureline/{id}",
     *      summary="Delete a captureline",
     *      description="Delete a captureline object",
     *      operationId="deleteCaptureline",
     *      tags={"Capturelines"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Captureline",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $capture = Captureline::withTrashed()->find($id);
        if($capture){
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'force must be true or false, 1 or 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $capture->delete();
            } else {
                Storage::disk('capturelines')->delete($capture->pathname);
                $capture->forceDelete();
            }
            return response()->json(['message'=> 'Captureline eliminada']);
        }else{
            return response()->json(['message' => 'Captureline no encontrada' . $id], 404);
        }
    }
}
