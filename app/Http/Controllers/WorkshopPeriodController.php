<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Period;
use App\WorkshopPeriod;
use App\WorkshopPeriodSchedule;
use App\WorkshopPeriodStudent;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str as Str;
use App\Libraries\Dates;
use PDF;
use App\Captureline;
use App\Inscription;

class WorkshopPeriodController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/workshop-period",
     *      operationId="getWorkshopPeriodList",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a listing of the workshop-period",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show WorkshopPeriod.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/WorkshopPeriod")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $workshopPeriods = WorkshopPeriod::withTrashed()->get();
        } else {
            $workshopPeriods = WorkshopPeriod::get();
        }
        return response()->json($workshopPeriods);
    }

    /**
     * @OA\Post(
     *      path="/api/workshop-period",
     *      summary="Store a workshop-period",
     *      description="Store a workshop-period object",
     *      operationId="storeWorkshopPeriod",
     *      tags={"WorkshopPeriods"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/WorkshopPeriod")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'workshop_id' => 'required|exists:workshops,id',
            'classroom_id' => 'required|exists:classrooms,id',
            'group' => 'required',
            'period_id' => 'required|exists:periods,id',
            'quota' => 'required|numeric',
            'teacher_id' => 'required|exists:users,id',
            'schedules.*.day' => 'required|numeric',
            'schedules.*.start' => 'required|date',
            'schedules.*.finish' => 'required|date',
            'students.*.id' => 'required|exists:roles',
            'students.*.pivot.limit' => 'numeric',
            'students.*.pivot.price' => 'numeric',
            'all_campus' => 'required|boolean',
            'notes' => 'nullable|alpha_text',
            'campus.*.id' => 'required_if:all_campus,true|numeric|exists:campus'
        ];
        $input = $request->only(
            'workshop_id',
            'classroom_id',
            'group',
            'period_id',
            'quota',
            'teacher_id',
            'schedules',
            'students',
            'all_campus',
            'campus',
            'notes'
        );
        $messages = [
            'workshop_id.required' => 'Se requiere taller',
            'workshop_id.exists' => 'No existe el taller',
            'classroom_id.required' => 'Se requiere salón',
            'classroom_id.exists' => 'No existe el salón',
            'period_id.required' => 'Se requiere periodo',
            'period_id.exists' => 'No existe el periodo',
            'teacher_id.required' => 'Se requiere profesor',
            'teacher_id.exists' => 'No existe el profesor',
            'quota.required' => 'Se requiere cupo',
            'quota.numeric' => 'Cupo debe ser numérico',
            'group.required' => 'Se requiere grupo',
            'schedules.*.day.required' => 'Se requiere día',
            'schedules.*.day.numeric' => 'Día no válido',
            'schedules.*.start.required' => 'Hora de init no válida',
            'schedules.*.start.date' => 'Hora de init no válida',
            'schedules.*.finish.required' => 'Hora de término no válida',
            'schedules.*.finish.date' => 'Hora de término no válida',
            'students.*.role_id.required' => 'Se requiere un tipo de estudiante',
            'students.*.role_id.exists' => 'El tipo de estudiante no existe',
            'students.*.pivot.limit.numeric' => 'El límite debe ser numérico',
            'students.*.pivot.price.numeric' => 'El precio debe ser numérico',
            'all_campus.required' => 'Se requiere las facultades participantes',
            'all_campus.boolean' => 'Facultades participantes debe ser booleano',
            'campus.*.id.exists' => 'La facultad no existe',
            'campus.*.id.required_if' => 'Se necesita ingresar una facultad',
            'notes.alpha_text' => "El texto no debe tener caracteres extraños"
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $workshopPeriod = WorkshopPeriod::create([
            'period_id' => $request->period_id,
            'workshop_id' => $request->workshop_id,
            'classroom_id' => $request->classroom_id,
            'teacher_id' => $request->teacher_id,
            'quota' => $request->quota,
            'group' => $request->group,
            'notes' => $request->notes,
            'all_campus' => $request->all_campus
        ]);
        
        foreach($request->schedules as $schedule) {
            WorkshopPeriodSchedule::create([
                'workshop_period_id' => $workshopPeriod->id,
                'day' => $schedule['day'],
                'start' => Dates::changeFormat($schedule['start'], 'H:i:s'),
                'finish' => Dates::changeFormat($schedule['finish'], 'H:i:s')
            ]);
        }
        $students = [];
        foreach($request->students as $student) {
            $students[$student['id']] = [
                'limit' => $student['pivot']['limit'],
                'price' => $student['pivot']['price']
            ];
        }
        $workshopPeriod->students()->attach($students);
        if (!$workshopPeriod->all_campus) {
            $campus = [];
            foreach($request->campus as $camp) {
                $campus[] = $camp['id'];
            }
            $workshopPeriod->campus()->attach($campus);
        }
        return response()->json(['success'=> true, 'message'=> 'Taller periodo registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/workshop-period/{id}",
     *      operationId="getWorkshopPeriod",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a workshop-period",
     *      @OA\Parameter(
     *          description="ID of workshop-period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a workshop-period.",
     *          @OA\JsonContent(ref="#/components/schemas/WorkshopPeriod")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workshopPeriod = WorkshopPeriod::find($id);
        if($workshopPeriod){
            $workshopPeriod->period;
            $workshopPeriod->workshop;
            $workshopPeriod->teacher;
            $workshopPeriod->classroom;
            $workshopPeriod->schedules;
            $workshopPeriod->students;
            $workshopPeriod->campus;
            return response()->json($workshopPeriod);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller periodo no encontrado'], 404);
        }
    }

    /**
     * 
     * @OA\Put(
     *      path="/api/workshop-period/{id}",
     *      summary="Update a workshop-period",
     *      description="Update a workshop-period object",
     *      operationId="updateWorkshopPeriod",
     *      tags={"WorkshopPeriods"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of WorkshopPeriod",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/WorkshopPeriod")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $workshopPeriod = WorkshopPeriod::withTrashed()->find($id);
        if($workshopPeriod){
            $rules = [
                'workshop_id' => 'required|exists:workshops,id',
                'classroom_id' => 'required|exists:classrooms,id',
                'group' => 'required',
                'period_id' => 'required|exists:periods,id',
                'quota' => 'required|numeric',
                'teacher_id' => 'required|exists:users,id',
                'schedules.*.day' => 'required|numeric',
                'schedules.*.start' => 'required|date',
                'schedules.*.finish' => 'required|date',
                'students.*.id' => 'required|exists:roles',
                'students.*.pivot.limit' => 'numeric',
                'students.*.pivot.price' => 'numeric',
                'all_campus' => 'required|boolean',
                'notes' => 'nullable|alpha_text',
                'campus.*.id' => 'required_if:all_campus,false|numeric|exists:campus'
            ];
            $input = $request->only(
                'workshop_id',
                'classroom_id',
                'group',
                'period_id',
                'quota',
                'teacher_id',
                'schedules',
                'students',
                'group',
                'all_campus',
                'notes',
                'campus'
            );
            $messages = [
                'workshop_id.required' => 'Se requiere taller',
                'workshop_id.exists' => 'No existe el taller',
                'classroom_id.required' => 'Se requiere salón',
                'classroom_id.exists' => 'No existe el salón',
                'period_id.required' => 'Se requiere periodo',
                'period_id.exists' => 'No existe el periodo',
                'teacher_id.required' => 'Se requiere profesor',
                'teacher_id.exists' => 'No existe el profesor',
                'quota.required' => 'Se requiere cupo',
                'group.required' => 'Se requiere grupo',
                'quota.numeric' => 'Cupo debe ser numérico',
                'schedules.*.day.required' => 'Se requiere día',
                'schedules.*.day.numeric' => 'Día no válido',
                'schedules.*.start.required' => 'Hora de init no válida',
                'schedules.*.start.date' => 'Hora de init no válida',
                'schedules.*.finish.required' => 'Hora de término no válida',
                'schedules.*.finish.date' => 'Hora de término no válida',
                'students.*.id.required' => 'Se requiere un tipo de estudiante',
                'students.*.id.exists' => 'El tipo de estudiante no existe',
                'students.*.pivot.limit.numeric' => 'El límite debe ser numérico',
                'students.*.pivot.price.numeric' => 'El precio debe ser numérico',
                'all_campus.required' => 'Se requiere las facultades participantes',
                'all_campus.boolean' => 'Facultades participantes debe ser booleano',
                'campus.*.id.exists' => 'La facultad no existe',
                'campus.*.id.required_if' => 'Se necesita ingresar una facultad',
                'notes.alpha_text' => "El texto no debe tener caracteres extraños"
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            $workshopPeriod->period_id = $request->period_id;
            $workshopPeriod->workshop_id = $request->workshop_id;
            $workshopPeriod->classroom_id = $request->classroom_id;
            $workshopPeriod->teacher_id = $request->teacher_id;
            $workshopPeriod->quota = $request->quota;
            $workshopPeriod->group = $request->group;
            $workshopPeriod->notes = $request->notes;
            $workshopPeriod->all_campus = $request->all_campus;
            $workshopPeriod->save();

            // Sync Schedules
            $idsSchedules = [];
            foreach ($request->schedules as $schedule) {
                if ($schedule['id']) {
                    $scheduleDB = WorkshopPeriodSchedule::find($schedule['id']);
                    $scheduleDB->day = $schedule['day'];
                    $scheduleDB->start = Dates::changeFormat($schedule['start']);
                    $scheduleDB->finish = Dates::changeFormat($schedule['finish']);
                    $scheduleDB->save();
                } else {
                    $scheduleDB = WorkshopPeriodSchedule::create([
                        'workshop_period_id' => $workshopPeriod->id,
                        'day' => $schedule['day'],
                        'start' => Dates::changeFormat($schedule['start']),
                        'finish' => Dates::changeFormat($schedule['finish'])
                    ]);
                }
                $idsSchedules[] = $scheduleDB->id;
            }
            WorkshopPeriodSchedule::where('workshop_period_id', $workshopPeriod->id)->whereNotIn('id', $idsSchedules)->delete();

            //sync roles
            $studentSync = [];
            foreach($request->students as $student) {
                $studentSync[$student['id']] = [
                    'limit' => $student['pivot']['limit'],
                    'price' => $student['pivot']['price']
                ];
            }
            $workshopPeriod->students()->sync($studentSync);

            //sync campus
            if (!$workshopPeriod->all_campus) {
                $campus = [];
                foreach($request->campus as $camp) {
                    $campus[] = $camp['id'];
                }
                $workshopPeriod->campus()->sync($campus);
            } else if(count($workshopPeriod->campus) > 0){
                $workshopPeriod->campus()->detach();
            }
            return response()->json(['success'=> true, 'message'=> 'Taller periodo registrado']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller periodo no válido'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/workshop-period/{id}",
     *      summary="Delete a workshop-period",
     *      description="Delete a workshop-period object",
     *      operationId="deleteWorkshopPeriod",
     *      tags={"WorkshopPeriods"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of WorkshopPeriod",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        WorkshopPeriod::destroy($id);
        return response()->json(['success'=> true, 'message'=> 'Se eliminó correctamente']);
    }

    /**
     * @OA\Post(
     *      path="/api/workshop-period/{id}/restore",
     *      summary="Restore a workshop-period",
     *      description="Restore a workshop-period object",
     *      operationId="restoreWorkshopPeriod",
     *      tags={"WorkshopPeriods"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of workshop-period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $workshopPeriod = WorkshopPeriod::withTrashed()->find($id);
        if($workshopPeriod){
            $workshopPeriod->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/workshop-period/by-period/{period_id}",
     *      operationId="getByPeriod",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a listing of the workshop-period",
     *      @OA\Parameter(
     *          description="Period Id",
     *          in="path",
     *          name="period_id",
     *          required=true,
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show WorkshopPeriod.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/WorkshopPeriod")
     *          )
     *      )
     * )
     * Get By Period
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getByPeriod(Request $request, $period_id) {
        $period = Period::find($period_id);
        $user = auth()->user();
        if($user->isRole('admin')){
            $workshops_period = $period->workshops_period()
            ->with('workshop')
            ->with('period')
            ->with('teacher')
            ->with('classroom')
            ->with('schedules')
            ->with('students')
            ->withTrashed()
            ->orderBy('workshop_id')->get();
        } else {
            $workshops_period = $period->workshops_period()
            ->with('workshop')
            ->with('period')
            ->with('teacher')
            ->with('classroom')
            ->with('schedules')
            ->with('students')
            ->orderBy('workshop_id')->get();
        }
        return response()->json($workshops_period);
    }
    /**
     * @OA\Get(
     *      path="/api/workshop-period/{id}/users",
     *      operationId="getUsers",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a workshop-period",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of workshop-period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a workshop-period.",
     *          @OA\JsonContent(ref="#/components/schemas/WorkshopPeriod")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get By with Users
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getUsers(Request $request, $id) {
        $workshopPeriod = WorkshopPeriod::with("workshop")
            ->with("schedules")
            ->with("teacher")
            ->with("students_inscribed.roles")
            ->find($id);
        if($workshopPeriod) {
            $workshopPeriodArray = $workshopPeriod->toArray();
            foreach($workshopPeriodArray["students_inscribed"] as $key => $student){
                if($student["pivot"]["captureline_id"]){
                    $workshopPeriodArray["students_inscribed"][$key]["pivot"]["captureline"] = Captureline::find($student["pivot"]["captureline_id"]);
                }
            }
            return response()->json($workshopPeriodArray);
        } else {
            return response()->json(['success' => false, 'message' => 'Taller periodo no válido'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/workshop-period/{id}/pdf-users",
     *      operationId="getPdfUsers",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a list",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of workshop-period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a list pdf."        
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get PDF with Users
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getPdfUsers(Request $request, $id) {
        $workshopPeriod = WorkshopPeriod::find($id);
        if($workshopPeriod) {
            ini_set("memory_limit","1024M");
            $workshopPeriod->workshop;
            $workshopPeriod->schedules;
            $workshopPeriod->teacher;
            $workshopPeriod->schedules;
            // dd($workshopPeriod->period);
            $days = $this->get_days_workshop($workshopPeriod);
            if (count($days) > 0) {
                $data = [    
                    "months" => ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
                    "days_name" => ["D","L","Ma","Mi","J","V","S"],
                    "days_array" => $days,
                    "wp" => $workshopPeriod
                ];
                $pdf = PDF::loadView('pdf.listWorkshop', $data);
                return $pdf->download('inscription.pdf');
            } else {
                response()->json(['success' => false, "message" => 'No hay días']);
            }
            return response()->json($workshopPeriod);
        } else {
            return response()->json(['success' => false, 'message' => 'Taller periodo no válido'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/workshop-period/{id}/by-user",
     *      operationId="getByUser",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a workshop-period",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of workshop-period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a workshop-period.",
     *          @OA\JsonContent(ref="#/components/schemas/Inscription")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get By Period
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getByUser(Request $request, $id) {
        $user = auth()->user();
        $inscription = Inscription::where('workshop_period_id', '=', $id)->where('user_id', '=', $user->id)->first();
        if ($inscription) {
            $inscription->user->roles;
            $inscription->workshop_period->workshop;
            $inscription->workshop_period->schedules;
            $inscription->workshop_period->period->requirements;
            return response()->json($inscription);
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }
    /**
     * @OA\Get(
     *      path="/api/workshop-period/{id}/by-user/{user_id}",
     *      operationId="getByUserAdmin",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a workshop-period",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of workshop-period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="ID of user",
     *          in="path",
     *          name="user_id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a inscription.",
     *          @OA\JsonContent(ref="#/components/schemas/Inscription")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get By Period
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getByUserAdmin(Request $request, $id, $user_id) {
        $inscription = Inscription::where('workshop_period_id', '=', $id)->where('user_id', '=', $user_id)->first();
        if ($inscription) {
            $inscription->user->roles;
            $inscription->workshop_period->workshop;
            $inscription->workshop_period->schedules;
            $inscription->workshop_period->period->requirements;
            return response()->json($inscription);
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/workshop-period/all-by-user",
     *      operationId="getAllByUser",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a list of Inscription by current user",
     *      security={ {"bearer": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="Show a workshop-period.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Inscription")
     *          ),
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get By Period
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getAllByUser(Request $request) {
        $user = auth()->user();
        $inscriptions = Inscription::has("workshop_period")
            ->with("workshop_period.workshop")
            ->with("workshop_period.schedules")
            ->with("workshop_period.period.requirements")
            ->where('user_id', '=', $user->id)->orderBy("inscriptions.created_at", "desc")->get();
        return response()->json($inscriptions);
    }
    /**
     * @OA\Get(
     *      path="/api/workshop-period/all-by-user/{user_id}",
     *      operationId="getAllByUser",
     *      tags={"WorkshopPeriods"},
     *      summary="Display a list of Inscription by user",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of user",
     *          in="path",
     *          name="user_id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a inscription.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Inscription")
     *          ),              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get By Period
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getAllByUserAdmin(Request $request, $user_id) {
        $inscriptions = Inscription::has("workshop_period")
            ->with("workshop_period.workshop")
            ->with("workshop_period.schedules")
            ->with("workshop_period.period.requirements")
            ->with("captureline")
            ->where('user_id', '=', $user_id)->orderBy("inscriptions.created_at", "desc")->get();
        return response()->json($inscriptions);
    }

    private function get_days_workshop($wp) {
        $init_array = explode('-', $wp->period->start);
        $finish_array = explode('-', $wp->period->finish);
        $days = [];
        $init_int = mktime(0, 0, 0, $init_array[1], $init_array[2], $init_array[0]);
        $finish_int = mktime(0, 0, 0, $finish_array[1], $finish_array[2], $finish_array[0]);
        if ($init_int < $finish_int) {
            while ($init_int < $finish_int) {
                $hasDay = false;
                foreach($wp->schedules as $schedule) {
                    if(date('w', $init_int) == $schedule->day) {
                        $hasDay = true;
                    }
                }
                if ($hasDay) {
                    $days[date('n', $init_int)][] = array('n' => date('d', $init_int) , 'd' => date('w', $init_int));
                }
                $init_int += 86400;
            }
        }
        return $days;
    }
    
}
