<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\Inscription;
use PDF;
class FilesController extends Controller
{
    /**
     * @OA\Get(
     *      path="/user-files/{user_id}/{filename}",
     *      operationId="getEvent",
     *      tags={"Files"},
     *      summary="Get a file",
     *      @OA\Parameter(
     *          description="ID of user",
     *          in="path",
     *          name="user_id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Name of file",
     *          in="path",
     *          name="filename",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Get a file.",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="resume",
     *                  type="file",
     *                  format="file"
     *              )
     *          )     
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * 
     */
    public function index(Request $request, $storage, $file){
        if (strpos($file, '.') !== false) {
            $user = auth()->user();
            if ($user && $user->isRole('admin')) {
                /** Serve the file for the Admin and self user*/
                return $this->returnFile($storage, $file);
            } else {
                abort(404);
            }
        } else {
            return abort(404);
        }
    }

    /**
     * @OA\Get(
     *      path="/files/captureline-img/{inscription_id}/{filename}",
     *      operationId="getEvent",
     *      tags={"Files"},
     *      summary="Get a file",
     *      @OA\Parameter(
     *          description="ID inscription",
     *          in="path",
     *          name="inscription_id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Name of file",
     *          in="path",
     *          name="filename",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Get a file.",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="resume",
     *                  type="file",
     *                  format="file"
     *              )
     *          )     
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * 
     */
    public function capturelineHideInscription(Request $request, $inscription_id, $file){
        if (strpos($file, '.') !== false) {
            $user = auth()->user();
            $inscription = Inscription::find($inscription_id);
            if ($user && $inscription && $inscription->user_id == $user->id) {
                /** Serve the file for the Admin and self user*/
                return $this->returnFile('capturelines', $file);
            } else {
                abort(404);
            }
        } else {
            return abort(404);
        }
    }

    /**
     * @OA\Get(
     *      path="/files/captureline-inscription/{inscription_id}",
     *      operationId="getEvent",
     *      tags={"Files"},
     *      summary="Get a PDF file",
     *      @OA\Parameter(
     *          description="ID inscription",
     *          in="path",
     *          name="inscription_id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Get a file.",
     *          @OA\Schema(
     *              @OA\Property(
     *                  property="resume",
     *                  type="file",
     *                  format="file"
     *              )
     *          )     
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * 
     */
    public function capturelineInscription(Request $request, $inscription_id){
        $user = auth()->user();
        $inscription = Inscription::find($inscription_id);
        if ($user && $inscription && $inscription->user_id == $user->id) {
            /** Serve the file for the Admin and self user*/
            $data["url_captureline"] = env("FILES_STORAGE") . "captureline-img/$inscription_id/".$inscription->captureline->pathname;
            $data["token"] = $request->query("token");
            $data["inscription"] = $inscription;
            $pdf = PDF::loadView('pdf.captureline', $data);
            return $pdf->download('inscription.pdf');
        } else {
            abort(404);
        }
    }
    private function returnFile($storage, $file)
    {
        //This method will look for the file and get it from drive
        $path = storage_path("app/$storage/$file");
        try {
            return response()->file($path);
        } catch (FileNotFoundException $exception) {
            abort(404);
        }
    }
}