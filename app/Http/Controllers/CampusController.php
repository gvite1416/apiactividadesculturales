<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Campus;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str as Str;

class CampusController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/campus",
     *      operationId="getCampusList",
     *      tags={"Campus"},
     *      summary="Display a listing of the campus",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Campus.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Campus")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,code,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,code,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'name');
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Campus::withTrashed()->count();
            if($limit == -1){
                $campus = Campus::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            } else {
                $campus = Campus::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        } else {
            $totalRegister = Campus::count();
            if ($limit == -1){
                $campus = Campus::orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->get();
            } else {
                $campus = Campus::orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        }
        return response()->json($campus)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/campus",
     *      summary="Store a campus",
     *      description="Store a campus object",
     *      operationId="storeCampus",
     *      tags={"Campus"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Campus")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|alpha_numeric_space|unique:campus',
            'code' => 'nullable|max:3|alpha_num'
        ];
        $input = $request->only(
            'name',
            'code'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.alpha_numeric_space' => 'Nombre debe contener solo carácteres alfanuméricos',
            'name.unique' => 'Ya exite una facultad con ese nombre',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'code.alpha_num' => 'El código debe ser alfanumérico',
            'code.max' => 'Nombre debe contener máximo 3 carácteres'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error], 400);
        }
        Campus::create([
            'name' => $request->name,
            'code' => $request->code,
            'college_degree_option' => $request->college_degree_option == 1 || $request->college_degree_option == true ? $request->college_degree_option : 0
        ]);
        return response()->json(['message'=> 'Facultad registrada'], 201);
    }

    /**
     * @OA\Get(
     *      path="/api/campus/{id}",
     *      operationId="getCampus",
     *      tags={"Campus"},
     *      summary="Display a campus",
     *      @OA\Parameter(
     *          description="ID of campus",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a campus.",
     *          @OA\JsonContent(ref="#/components/schemas/Campus")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $campus = Campus::withTrashed()->find($id);
        } else {
            $campus = Campus::find($id);
        }
        if($campus){
            return response()->json($campus);
        }else{
            return response()->json(['success' => false, 'message' => 'Facultad no encontrada'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/campus/{id}",
     *      summary="Update a campus",
     *      description="Update a campus object",
     *      operationId="updateCampus",
     *      tags={"Campus"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Campus",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Campus")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $campus = Campus::withTrashed()->find($id);
        if($campus){
            $rules = [
                'name' => ['required','max:255','alpha_numeric_space',Rule::unique('campus')->ignore($campus->id)],
                'code' => 'nullable|max:3|alpha_num'
            ];
            $input = $request->only(
                'name',
                'code'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.alpha_numeric_space' => 'Nombre debe contener solo carácteres alfanuméricos',
                'name.max' => 'Nombre debe contener máximo 255 carácteres',
                'code.alpha_num' => 'El código debe ser alfanumérico',
                'code.max' => 'Nombre debe contener máximo 3 carácteres'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $campus->name = $request->name;
            $campus->code = $request->code;
            $campus->college_degree_option = $request->college_degree_option == 1 || $request->college_degree_option == true ? $request->college_degree_option : 0;
            $campus->save();
            return response()->json(['message'=> 'Facultad actualizada']);
        }else{
            return response()->json(['message' => 'Facultad no encontrada'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/campus/{id}",
     *      summary="Delete a campus",
     *      description="Delete a campus object",
     *      operationId="deleteCampus",
     *      tags={"Campus"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Campus",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $campus = Campus::withTrashed()->find($id);
        if ($campus) {
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'Debe ser true o false, 1 ó 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $campus->delete();
            } else {
                $campus->forceDelete();
            }
            return response()->json(['message'=> 'Facultad eliminada']);
        } else {
            return response()->json(['message'=> 'Campus Not Found'], 404);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/campus/{id}/restore",
     *      summary="Restore a campus",
     *      description="Restore a campus object",
     *      operationId="restoreCampus",
     *      tags={"Campus"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of campus",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $campus = Campus::withTrashed()->find($id);
        if($campus){
            $campus->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Facultad no encontrada'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/campus/check-name/{id?}",
     *      summary="checkNameCampus",
     *      description="Check if the name exist",
     *      operationId="checkNameCampus",
     *      tags={"Campus"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Campus",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * API Check Name
     * 
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkName(Request $request, $id = false){
        if ($id === false) {
            $rules = [
                'name' => 'required|alpha_numeric_space|max:150|unique:campus',
            ];
        } else {
            $rules = [
                'name' => ['required', 'alpha_numeric_space', 'max:150', Rule::unique('campus')->ignore($id)],
            ];
        }
        $input = $request->only(
            'name'
        );
        $messages = [
            'name.required' => 'required',
            'name.unique' => 'unique'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['message'=> $validator->messages()], 400);
        }
        return response()->json(['message'=> "Don't exist"]);
    }
}
