<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Talent;
use Validator;
use PDF;
use App\Libraries\Dates;

class TalentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $talents = Talent::get();
        foreach($talents as $key => $talent) {
            $talents[$key]->career;
        }
        return response()->json($talents);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|alpha_space',
            'career_id' => 'required|numeric|exists:careers,id',
            'semester' => 'required|numeric',
            'number_id' => 'required|alpha_num',
            'email' => 'required|email|max:255',
            'phone' => 'nullable',
            'celphone' => 'required',
            'arts' => 'required|max:255|enum:Artes escénicas,Artes musicales',
            'art_activity' => 'nullable|max:255|alpha_text',
            'band' => 'nullable|max:255|alpha_text',
            'gender' => 'nullable|max:255|alpha_text',
            'members' => 'nullable|alpha_text',
            'duration' => 'nullable|numeric',
            'equipment' => 'nullable|alpha_text',
            'scenography' => 'nullable|alpha_text',
            'illumination' => 'nullable|alpha_text',
            'audio_video' => 'nullable|alpha_text',
            'assembly_duration' => 'nullable|numeric',
            'deassembly_duration' => 'nullable|numeric'
        ];
        $input = $request->only(
            'name',
            'career_id',
            'semester',
            'number_id',
            'email',
            'phone',
            'arts',
            'art_activity',
            'band',
            'gender',
            'members',
            'duration',
            'celphone',
            'equipment',
            'scenography',
            'audio_video',
            'illumination',
            'assembly_duration',
            'deassembly_duration'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.max' => 'Nombre debe tener menos de 255 caracteres',
            'name.alpha_space' => 'Nombre no debe contener caracteres especiales',
            'career_id.required' => 'Se requiere carrera',
            'career_id.numeric' => 'Carrera debe de ser un id válido',
            'career_id.exists' => 'No existe la carrera ingresada',
            'semester.required' => 'Se require semestre',
            'semester.numeric' => 'Semestre debe ser numérico',
            'number_id.required' => 'Se require número de cuenta',
            'number_id.alpha_num' => 'Número de cuenta debe ser alfanumérico',
            'email.required' => 'Se requiere email',
            'email.email' => 'Debe de ser email válido',
            'email.max' => 'Email debe tener menos de 255 caracteres',
            'celphone.required' => 'Se require teléfono celular',
            'arts.required' => 'Se requiere tipo de Arte',
            'arts.max' => 'Artes debe tener menos de 255 caracteres',
            'arts.enum' => 'Arte debe ser Artes escénicas o Artes musicales',
            'art_activity.max' => 'Actividad artistica debe tener menos de 255 caracteres',
            'art_activity.alpha_text' => 'Actividad artistica no debe tener caracteres especiales',
            'band.max' => 'Banda/Grupo debe tener menos de 255 caracteres',
            'band.alpha_text' => 'Banda/Grupo no debe tener caracteres especiales',
            'gender.max' => 'Género debe tener menos de 255 caracteres',
            'gender.alpha_text' => 'Género no debe tener caracteres especiales',
            'members.alpha_text' => 'Miembros de la banda no debe tener caracteres especiales',
            'duration.numeric' => 'La duración debe ser numérica',
            'equipment.alpha_text' => 'Equipamiento no debe tener caracteres especiales',
            'scenography.alpha_text' => 'Escenografía no debe tener caracteres especiales',
            'illumination.alpha_text' => 'Iluminación no debe tener caracteres especiales',
            'illumination.audio_video' => 'Audio y video no debe tener caracteres especiales',
            'assembly_duration.numeric' => 'La duración de montaje debe ser numérico',
            'deassembly_duration.numeric' => 'La duración de desmontaje debe ser numérico',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $verification_code = str_random(30); //Generate verification code
        $talent = Talent::create([
            'name' => $request->name,
            'career_id' => $request->career_id,
            'semester' => $request->semester,
            'number_id' => $request->number_id,
            'email' => $request->email,
            'phone' => $request->phone,
            'arts' => $request->arts,
            'art_activity' => $request->art_activity,
            'band' => $request->band,
            'gender' => $request->gender,
            'members' => $request->members,
            'duration' => $request->duration,
            'celphone' => $request->celphone,
            'equipment' => $request->equipment,
            'scenography' => $request->scenography,
            'audio_video' => $request->audio_video,
            'illumination' => $request->illumination,
            'assembly_duration' => $request->assembly_duration,
            'deassembly_duration' => $request->deassembly_duration,
            'token' => $verification_code
        ]);
        return response()->json(['success' => true, 'message' => 'Participación registrada', 'token' => $verification_code]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $talent = Talent::find($id);
        if($talent){
            $talent->career;
            return response()->json($talent);
        }else{
            return response()->json(['success' => false, 'message' => 'Talento no encontrado'], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $talent = Talent::find($id);
        if($talent){
            $rules = [
                'name' => 'required|max:255|alpha_space',
                'career_id' => 'required|numeric|exists:careers,id',
                'semester' => 'required|numeric',
                'number_id' => 'required|alpha_num',
                'email' => 'required|email|max:255',
                'phone' => 'nullable',
                'celphone' => 'required',
                'arts' => 'required|max:255|enum:Artes escénicas,Artes musicales',
                'art_activity' => 'nullable|max:255|alpha_text',
                'band' => 'nullable|max:255|alpha_text',
                'gender' => 'nullable|max:255|alpha_text',
                'members' => 'nullable|alpha_text',
                'duration' => 'nullable|numeric',
                'equipment' => 'nullable|alpha_text',
                'scenography' => 'nullable|alpha_text',
                'illumination' => 'nullable|alpha_text',
                'audio_video' => 'nullable|alpha_text',
                'assembly_duration' => 'nullable|numeric',
                'deassembly_duration' => 'nullable|numeric'
            ];
            $input = $request->only(
                'name',
                'career_id',
                'semester',
                'number_id',
                'email',
                'phone',
                'arts',
                'art_activity',
                'band',
                'gender',
                'members',
                'duration',
                'celphone',
                'equipment',
                'scenography',
                'audio_video',
                'illumination',
                'assembly_duration',
                'deassembly_duration'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.max' => 'Nombre debe tener menos de 255 caracteres',
                'name.alpha_space' => 'Nombre no debe contener caracteres especiales',
                'career_id.required' => 'Se requiere carrera',
                'career_id.numeric' => 'Carrera debe de ser un id válido',
                'career_id.exists' => 'No existe la carrera ingresada',
                'semester.required' => 'Se require semestre',
                'semester.numeric' => 'Semestre debe ser numérico',
                'number_id.required' => 'Se require número de cuenta',
                'number_id.alpha_num' => 'Número de cuenta debe ser alfanumérico',
                'email.required' => 'Se requiere email',
                'email.email' => 'Debe de ser email válido',
                'email.max' => 'Email debe tener menos de 255 caracteres',
                'celphone.required' => 'Se require teléfono celular',
                'arts.required' => 'Se requiere tipo de Arte',
                'arts.max' => 'Artes debe tener menos de 255 caracteres',
                'arts.enum' => 'Arte debe ser Artes escénicas o Artes musicales',
                'art_activity.max' => 'Actividad artistica debe tener menos de 255 caracteres',
                'art_activity.alpha_text' => 'Actividad artistica no debe tener caracteres especiales',
                'band.max' => 'Banda/Grupo debe tener menos de 255 caracteres',
                'band.alpha_text' => 'Banda/Grupo no debe tener caracteres especiales',
                'gender.max' => 'Género debe tener menos de 255 caracteres',
                'gender.alpha_text' => 'Género no debe tener caracteres especiales',
                'members.alpha_text' => 'Miembros de la banda no debe tener caracteres especiales',
                'duration.numeric' => 'La duración debe ser numérica',
                'equipment.alpha_text' => 'Equipamiento no debe tener caracteres especiales',
                'scenography.alpha_text' => 'Escenografía no debe tener caracteres especiales',
                'illumination.alpha_text' => 'Iluminación no debe tener caracteres especiales',
                'illumination.audio_video' => 'Audio y video no debe tener caracteres especiales',
                'assembly_duration.numeric' => 'La duración de montaje debe ser numérico',
                'deassembly_duration.numeric' => 'La duración de desmontaje debe ser numérico',
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            $talent->name = $request->name;
            $talent->career_id = $request->career_id;
            $talent->semester = $request->semester;
            $talent->number_id = $request->number_id;
            $talent->email = $request->email;
            $talent->phone = $request->phone;
            $talent->arts = $request->arts;
            $talent->art_activity = $request->art_activity;
            $talent->band = $request->band;
            $talent->gender = $request->gender;
            $talent->members = $request->members;
            $talent->duration = $request->duration;
            $talent->celphone = $request->celphone;
            $talent->equipment = $request->equipment;
            $talent->scenography = $request->scenography;
            $talent->audio_video = $request->audio_video;
            $talent->illumination = $request->illumination;
            $talent->assembly_duration = $request->assembly_duration;
            $talent->deassembly_duration = $request->deassembly_duration;
            $talent->save();
            return response()->json($offering);
        }else{
            return response()->json(['success' => false, 'message' => 'Talento no encontrada'], 404);
        }
    }


    function getPdf(Request $request, $token) {
        $talent = Talent::where('token', '=', $token)->first();
        if($talent) {
            //$talent->token = '';
            //$talent->save();
            $members = explode(',' , $talent->members);
            $equipments = explode(',' , $talent->equipment);
            $data = ['talent' => $talent , 'members' => $members, 'equipments' => $equipments];
            $pdf = PDF::loadView('pdf.talent', $data);
            return $pdf->download('registro_talento.pdf');
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }

    function getPdfAdmin(Request $request, $id) {
        $talent = Talent::find($id);
        if($talent) {
            //$talent->token = '';
            //$talent->save();
            $members = explode(',' , $talent->members);
            $equipments = explode(',' , $talent->equipment);
            $data = ['talent' => $talent , 'members' => $members, 'equipments' => $equipments];
            $pdf = PDF::loadView('pdf.talent', $data);
            return $pdf->download('registro_talento.pdf');
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }

    function exportCsv(Request $request) {
        $headers = array(
            "Content-Encoding" => "UTF-8",
            "Content-type" => "text/csv; charset=utf-8",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $columns = [
            'Nombre',
            'Carrera',
            'Semestre',
            utf8_decode('Número de Cuenta'),
            'Correo',
            utf8_decode('Teléfono Fijo'),
            utf8_decode('Teléfono Celular'),
            'Arte',
            utf8_decode('Actividad Artística'),
            'Nombre de Grupo/Solista',
            utf8_decode('Género'),
            'Integrantes',
            utf8_decode('Duración'),
            'Equipamiento',
            utf8_decode('Escenografía'),
            utf8_decode('Iluminación'),
            'Audio y Video',
            utf8_decode('Duración de montaje'),
            utf8_decode('Duración de desmontaje'),
            'Fecha de Registro'
        ];
        $talents = Talent::get();
        $callback = function() use ($talents, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach($talents as $talent) {
                fputcsv($file, [
                    utf8_decode($talent->name),
                    utf8_decode($talent->career->name),
                    $talent->semester,
                    $talent->number_id,
                    $talent->email,
                    $talent->phone,
                    $talent->celphone,
                    utf8_decode($talent->arts),
                    utf8_decode($talent->art_activity),
                    utf8_decode($talent->band),
                    utf8_decode($talent->gender),
                    utf8_decode($talent->members),
                    $talent->duration,
                    utf8_decode($talent->equipment),
                    utf8_decode($talent->scenography),
                    utf8_decode($talent->illumination),
                    utf8_decode($talent->audio_video),
                    $talent->assembly_duration,
                    $talent->deassembly_duration,
                    Dates::changeFormatUTC($talent->created_at , 'd:m:Y H:i:s'),
                ]);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
