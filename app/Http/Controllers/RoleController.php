<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{

    /**
     * @OA\Get(
     *      path="/api/role",
     *      operationId="getRoleList",
     *      tags={"Role"},
     *      summary="Display a listing of roles system",
     *      security={ {"bearer": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="Show Roles.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Role")
     *          )
     *      )
     * )
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roles = Role::get();
        return response()->json($roles);
    }
    /**
     * @OA\Get(
     *      path="/api/role/by-group/{slug}",
     *      operationId="getRoleListGroup",
     *      tags={"Role"},
     *      summary="Display a listing of roles system by group",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="slug group role",
     *          in="path",
     *          name="slug",
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Roles.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Role")
     *          )
     *      )
     * )
     * Display a listing of the resource.
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function getByGroup($slug) {
        $roles = Role::where('group' , $slug)->get();
        return response()->json($roles);
    }
}
