<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getResetToken(Request $request)
    {
        $user = User::where('email', $request->user)->first();
        if (!$user) {
            $user = User::where('number_id', $request->user)->first();
            if (!$user) {
                $error_message = "El usuario no fue encontrado.";
                return response()->json(['success' => false, 'message' => $error_message]);
            }
        }
        $token = $this->broker()->createToken($user);
        return response()->json(Json::response(['token' => $token]));
    }

    public function sendResetLink(Request $request)
    {
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $user = User::where('email', $request->user)->first();
        if (!$user) {
            $user = User::where('number_id', $request->user)->first();
            if (!$user) {
                $error_message = "El usuario no fue encontrado.";
                return response()->json(['success' => false, 'message' => $error_message]);
            }
        }
        /*$response = $this->broker()->sendResetLink(['email' => $user->email]);
        
        $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($response)
                    : $this->sendResetLinkFailedResponse($request, $response);

        if($response == "passwords.sent"){
            return response()->json([
                'success' => true, 'data'=> ['message'=> 'Revisa tu email se ha mandado un link para reestablecer tu contraseña.']
            ]);
        }*/
        $error_message = "El usuario no fue encontrado.";
        return response()->json(['success' => false, 'message' => $error_message]);

    }

}
