<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @OA\Post(
     *      path="/api/reset",
     *      summary="reset",
     *      description="Reset the password",
     *      operationId="resetAuth",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="token", type="string"),
     *              @OA\Property(property="user", type="string"),
     *              @OA\Property(property="password", type="string"),
     *              @OA\Property(property="password_confirmation", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Returns when token not exist",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="El token no es válido o ya expiró."),
     *          )
     *      )
     * )
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());
        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        try{
            $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            });
        }catch(Exception $e){
            return response()->json(['message'=> 'e: ' . $e], 500);
        }
        
        if ($response == Password::PASSWORD_RESET) {
            return response()->json(['message'=> 'Contraseña actualizada']);
        } else if($response == Password::INVALID_TOKEN){
            return response()->json(['message'=> 'El token no es válido o ya expiró'], 400);
        } else {
            return response()->json(['message'=> 'No se pudo cambiar la contraseña: ' . $response], 400);
        }
    }
}
