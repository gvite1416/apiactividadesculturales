<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Imports\ImportUsers;
use Excel;
use App\UserPeriodRequirement;
use App\User;

class UploadController extends Controller
{
    /**
     * @OA\Post(
     *      path="/api/upload/images",
     *      summary="Upload a image",
     *      description="Upload a image",
     *      operationId="images",
     *      tags={"Upload"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="image",
     *                      type="string",
     *                      format="binary"
     *                  )
     *              )    
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="fullpath", type="string"),
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function images(Request $request) {
        $path = Storage::disk("public")->putFile('tmp', $request->file('image'));
        return response()->json(['fullpath'=> Storage::disk("public")->url( $path), 'name' => str_replace("tmp/", "", $path)]);
    }

    /**
     * @OA\Post(
     *      path="/api/upload/image",
     *      summary="Upload a image",
     *      description="Upload a image",
     *      operationId="image",
     *      tags={"Upload"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="image",
     *                      type="string",
     *                      format="binary"
     *                  )
     *              )    
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="fullpath", type="string"),
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function image(Request $request) {
        // $path = Storage::disk("public")->putFile('tmp', $request->file('image'));
        $path = Storage::disk("public")->putFile('tmp', $request->file('image'));
        return response()->json(['fullpath'=> Storage::disk("public")->url( $path), 'name' => str_replace("tmp/", "", $path)]);
    }

    /**
     * @OA\Post(
     *      path="/api/upload/image-base64",
     *      summary="Upload a image encode base64",
     *      description="Upload a image encode base64",
     *      operationId="imageBase64",
     *      tags={"Upload"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          @OA\JsonContent(
     *              @OA\Property(property="image", type="string"),
     *              @OA\Property(property="old", type="boolean")
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="fullpath", type="string"),
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function imageBase64(Request $request) {
        $rules = [
            'image' => 'required',
        ];
        $input = $request->only(
            'image',
            'old'
        );
        $messages = [
            'image.required' => 'Se require imagen'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $base64_str = substr($request->image, strpos($request->image, ",")+1);
        $image = base64_decode($base64_str);
        $imgname = uniqid();
        Storage::disk("public")->put("tmp/$imgname.jpg", $image);
        if($request->old) {
            Storage::disk("public")->delete("tmp/" . $request->old);
        }
        return response()->json(['fullpath'=> Storage::disk("public")->url("tmp/$imgname.jpg"), 'name'=> $imgname . '.jpg']);
    }

    /**
     * @OA\Post(
     *      path="/api/upload/images/profile",
     *      summary="Upload a profile image",
     *      description="Upload a profile image",
     *      operationId="profile",
     *      tags={"Upload"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="photo",
     *                      type="string",
     *                      format="binary"
     *                  )
     *              )    
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="fullpath", type="string"),
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function profile(Request $request) {
        /*$path = $request->file('photo')->store('tmp');
        $new_path = 'public/' . $path;
        $old_path = $path;
        Storage::move($old_path, $new_path);*/
        $path = Storage::disk("public")->putFile('tmp', $request->file('photo'));
        return response()->json(['fullpath'=> Storage::disk("public")->url( $path), 'name'=> str_replace("tmp/", "", $path)]);
    }

    /**
     * @OA\Post(
     *      path="/api/upload/excel",
     *      summary="Upload a excel file",
     *      description="Upload a excel file",
     *      operationId="excel",
     *      tags={"Upload"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="excel",
     *                      type="file",
     *                      format="file"
     *                  )
     *              )    
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="fullpath", type="string"),
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function excel(Request $request) {
        $rules = [
            'excel' => 'mimes:xlsx,xls'
        ];
        $input = $request->only("excel");
        $messages = [
            'excel.mimes' => "Formato no válido"
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $path = $request->file('excel')->store('tmp');
        $filename = str_replace("tmp/", "", $path);
        $array = Excel::toArray(new ImportUsers, storage_path('app/tmp/' . $filename));
        $rows = 0;
        if(count($array) > 0){
            foreach($array[0] as $row){
                if($row[0] && $row[0] !== 'sis'){
                    $rows++;
                }
            }
            Storage::disk('local')->put('tmp/' . $filename . '.json', json_encode($array[0]));
            Storage::disk('local')->delete('tmp/' . $filename);
        }
        return response()->json(['success'=> true, 'file'=> $filename . '.json', 'count_rows' => $rows]);
    }

    /**
     * @OA\Post(
     *      path="/api/upload/requirement",
     *      summary="Upload a file",
     *      description="Upload file",
     *      operationId="requirement",
     *      tags={"Upload"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="filereq",
     *                      type="file",
     *                      format="file"
     *                  ),
     *                  @OA\Property(property="period_requirement_id", type="integer"),
     *                  @OA\Property(property="inscription_id", type="integer"),
     *                  @OA\Property(property="user_period_requirement_id", type="integer")
     *              )    
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean"),
     *              @OA\Property(property="new", type="boolean")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function requirement(Request $request) {
        $rules = [
            'filereq' => 'file|mimes:jpg,png,jpeg,pdf',
            'period_requirement_id' => 'required|integer',
            'inscription_id' => 'integer',
            'user_period_requirement_id' => 'integer'
        ];
        $input = $request->only("filereq","period_requirement_id", "inscription_id", "user_period_requirement_id");
        $messages = [
            'filereq.mimes' => "Formato no válido",
            'filereq.file' => "Archivo requerido",
            'period_requirement_id.required' => "Se requiere valor",
            'period_requirement_id.integer' => "El valor debe ser entero",
            'inscription_id.integer' => "El inscription id debe ser válido",
            'user_period_requirement_id.integer' => "El user_period_requirement id debe ser válido"
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $user = auth()->user();
        $path = Storage::disk("users")->putFile("$user->id", $request->file('filereq'));
        $name = str_replace("$user->id/", "", $path);
        if($request->user_period_requirement_id){
            $upr = UserPeriodRequirement::find($request->user_period_requirement_id);
            Storage::disk('users')->delete("$user->id/$upr->file");
            $upr->file = $name;
            $upr->mime = $request->filereq->extension();
            $upr->save();
            return response()->json(['success'=> true, "new" => false]);
        } else {
            UserPeriodRequirement::Create([
                'user_id' => $user->id,
                'period_requirement_id' => $request->period_requirement_id,
                'inscription_id' => $request->inscription_id,
                'file' => $name,
                'mime' => $extension = $request->filereq->extension()
            ]);
            return response()->json(['success'=> true, "new" => true]);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/upload/requirement/{user_id}",
     *      summary="Upload a file user",
     *      description="Upload file user",
     *      operationId="requirementUser",
     *      tags={"Upload"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="User Id",
     *          in="path",
     *          name="user_id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          @OA\MediaType(
     *              mediaType="multipart/form-data",
     *              @OA\Schema(
     *                  @OA\Property(
     *                      property="filereq",
     *                      type="file",
     *                      format="file"
     *                  ),
     *                  @OA\Property(property="period_requirement_id", type="integer"),
     *                  @OA\Property(property="inscription_id", type="integer"),
     *                  @OA\Property(property="user_period_requirement_id", type="integer")
     *              )    
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean"),
     *              @OA\Property(property="new", type="boolean")
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * @return \Illuminate\Http\Response
     * 
     */
    public function requirementUser(Request $request, $user_id) {
        $rules = [
            'filereq' => 'file|mimes:jpg,png,jpeg,pdf',
            'period_requirement_id' => 'required|integer',
            'inscription_id' => 'integer',
            'user_period_requirement_id' => 'integer'
        ];
        $input = $request->only("filereq","period_requirement_id", "inscription_id", "user_period_requirement_id");
        $messages = [
            'filereq.mimes' => "Formato no válido",
            'filereq.file' => "Archivo requerido",
            'period_requirement_id.required' => "Se requiere valor",
            'period_requirement_id.integer' => "El valor debe ser entero",
            'inscription_id.integer' => "El inscription id debe ser válido",
            'user_period_requirement_id.integer' => "El user_period_requirement id debe ser válido"
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $user = User::find($user_id);
        if ($user){
            $path = Storage::disk("users")->putFile("$user->id", $request->file('filereq'));
            $name = str_replace("$user->id/", "", $path);
            if($request->user_period_requirement_id){
                $upr = UserPeriodRequirement::find($request->user_period_requirement_id);
                Storage::disk('users')->delete("$user->id/$upr->file");
                $upr->file = $name;
                $upr->mime = $request->filereq->extension();
                $upr->save();
                return response()->json(['success'=> true, "new" => false]);
            } else {
                UserPeriodRequirement::Create([
                    'user_id' => $user->id,
                    'period_requirement_id' => $request->period_requirement_id,
                    'inscription_id' => $request->inscription_id,
                    'file' => $name,
                    'mime' => $extension = $request->filereq->extension()
                ]);
                return response()->json(['success'=> true, "new" => true]);
            }
        } else {
            return response()->json(['success'=> false, 'message'=> 'Usuario no encontrado'],404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/upload/tmp/{file}",
     *      summary="Delete a file tmp",
     *      description="Delete a file tmp",
     *      operationId="deleteUploadTmp",
     *      tags={"Upload"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="file",
     *          in="path",
     *          name="file",
     *          required=true,
     *          example="1.jpg",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteTmp($file){
        Storage::disk("public")->delete("tmp/" . $file);
        return response()->json(['success'=> true, 'message'=> 'Se eliminó correctamente']);
    }
}
