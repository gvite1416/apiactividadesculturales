<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requirement;
use App\Period;
use Validator;
class RequirementController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/requirement",
     *      operationId="getRequirementList",
     *      tags={"Requirement"},
     *      summary="Display a listing of the requirements",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Requirement.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Requirement")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'created_at');

        $totalRegister = Requirement::count();
        if($limit == -1)
            $requirements = Requirement::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
        else
            $requirements = Requirement::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        return response()->json($requirements)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/requirement",
     *      summary="Store a requirement",
     *      description="Store a requirement object",
     *      operationId="storeRequirement",
     *      tags={"Requirement"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Requirement")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'role_id' => 'required|numeric|exists:roles,id',
            'has_file' => 'required',
            'upload_type' => 'enum:PERIOD,WORKSHOP'
        ];
        $input = $request->only(
            'name',
            'role_id',
            'has_file',
            'upload_type'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'role_id.required' => 'Se necesita tipo de usuario',
            'role_id.numeric' => 'Sólo se permite números',
            'role_id.exists' => 'No existe ese tipo de usuario',
            'has_file.required' => 'Se requiere indicar si se debe subir un archivo',
            'upload_type.enum' => 'Sólo se debe ingresar PERIOD o WORKSHOP'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        Requirement::create([
            'name' => $request->name,
            'role_id' => $request->role_id,
            'has_file' => $request->has_file,
            'upload_type' => $request->upload_type
        ]);
        return response()->json(['success'=> true, 'message'=> 'Requerimiento registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/requirement/{id}",
     *      operationId="getRequirement",
     *      tags={"Requirement"},
     *      summary="Display a requirement",
     *      @OA\Parameter(
     *          description="ID of requirement",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a requirement.",
     *          @OA\JsonContent(ref="#/components/schemas/Requirement")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $requirement = Requirement::find($id);
        if($requirement){
            return response()->json($requirement);
        }else{
            return response()->json(['success' => false, 'message' => 'Requerimiento no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/requirement/{id}",
     *      summary="Update a requirement",
     *      description="Update a requirement object",
     *      operationId="updateRequirement",
     *      tags={"Requirement"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Requirement",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Requirement")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requirement = Requirement::withTrashed()->find($id);
        if($requirement){
            $rules = [
                'name' => 'required|max:255',
                'role_id' => 'required|numeric|exists:roles,id',
                'has_file' => 'boolean',
                'upload_type' => 'enum:PERIOD,WORKSHOP'
            ];
            $input = $request->only(
                'name',
                'role_id',
                'has_file',
                'upload_type'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.max' => 'Nombre debe contener máximo 255 carácteres',
                'role_id.required' => 'Se necesita tipo de usuario',
                'role_id.numeric' => 'Sólo se permite números',
                'role_id.exists' => 'No existe ese tipo de usuario',
                'has_file.boolean' => 'Se requiere indicar si se debe subir un archivo',
                'upload_type.enum' => 'Sólo se debe ingresar PERIOD o WORKSHOP'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            
            $requirement->name = $request->name;
            $requirement->role_id = $request->role_id;
            $requirement->has_file = $request->has_file;
            $requirement->upload_type = $request->upload_type;
            $requirement->save();
            return response()->json(['success'=> true, 'message'=> 'Requerimiento actualizado']);
        }else{
            return response()->json(['success' => false, 'message' => 'Requerimiento no encontrado'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/requirement/{id}",
     *      summary="Delete a requirement",
     *      description="Delete a requirement object",
     *      operationId="deleteRequirement",
     *      tags={"Requirement"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Requirement",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requirement = Requirement::withTrashed()->find($id);
        if ($requirement) {
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'Debe ser true o false, 1 ó 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $requirement->delete();
            } else {
                $requirement->forceDelete();
            }
            return response()->json(['message'=> 'Ocupación eliminada']);
        } else {
            return response()->json(['message'=> 'Requirement Not Found'], 404);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/requirement/{id}/restore",
     *      summary="Restore a requirement",
     *      description="Restore a requirement object",
     *      operationId="restoreRequirement",
     *      tags={"Requirement"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of requirement",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $requirement = Requirement::withTrashed()->find($id);
        if($requirement){
            $requirement->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Requerimiento no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/requirement/check-name/{id?}",
     *      summary="checkNameRequirement",
     *      description="Check if the name exist",
     *      operationId="checkNameRequirement",
     *      tags={"Requirement"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Requirement",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function byPeriod($period_id)
    {
        $period = Period::find($period_id);
        return response()->json($period->requirements);
    }
}
