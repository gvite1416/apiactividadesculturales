<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Period;
use App\Career;
use App\Event;
use App\Libraries\Dates;
use App\Libraries\FormatData;

class ReportController extends Controller
{
    /**
     * @OA\Post(
     *      path="/api/report/one",
     *      summary="Download report 1",
     *      description="Download report 1",
     *      operationId="one",
     *      tags={"Report"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="period", type="integer"),
     *              @OA\Property(property="career", type="integer"),
     *              @OA\Property(property="student", type="integer"),
     *              @OA\Property(property="typeFile", type="string", example="csv|pdf"),
     *              @OA\Property(property="complete", type="string", example="no"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Return report number one.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function one(Request $request) {
        $period = Period::find($request->period);
        if($period){
            $s = [];
            $career = 'Todas las carreras';
            $careerAux = false;
            if($request->career && $request->career !== 0) {
                $careerAux = Career::find($request->career);
                $career = $careerAux->name;
            }
            $period->workshops_period;
            $periodArray = [
                'name' => $period->name,
                'workshops' => [],
                'countTotal' => 0
            ];
            foreach($period->workshops_period as $key => $wp) {
                if(!isset($periodArray['workshops'][$wp->workshop->id]['count_inscribed'])){
                    $periodArray['workshops'][$wp->workshop->id]['count_inscribed'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['students'] = [];
                    $periodArray['workshops'][$wp->workshop->id]['name'] = $wp->workshop->name;
                }
                foreach($period->workshops_period[$key]->students_inscribed as $si){
                    if(
                        (((($si->isRole('student') || $si->isRole('exstudent')) && $request->student == 0) ||
                        ($si->isRole('student') && $request->student == 2) || 
                        ($si->isRole('exstudent') && $request->student == 3)) && $si->pivot->status == 1) &&
                        ($careerAux === false || $careerAux->id === $si->dataStudent[0]->career->id)
                    ){
                        $periodArray['workshops'][$wp->workshop->id]['count_inscribed']++;
                        $periodArray['countTotal']++;
                        $periodArray['workshops'][$wp->workshop->id]['students'][] = $si;
                    }
                }
            }
            if($request->typeFile === 'csv') {
                $headers = array(
                    "Content-Encoding" => "UTF-8",
                    "Content-type" => "text/csv; charset=utf-8",
                    "Content-Disposition" => "attachment; filename=file.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                $callback = function() use ($periodArray, $career, $request)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, []);
                    fputcsv($file, ["Periodo", $periodArray['name']]);
                    fputcsv($file, ["Carrera" , $career]);
                    if($request->complete === 'no') {
                        fputcsv($file, ["Taller","No. Alumnos"]);
                        foreach($periodArray['workshops'] as $workshop) {
                            fputcsv($file, [
                                utf8_decode($workshop['name']),
                                $workshop['count_inscribed']
                            ]);
                        }
                        fputcsv($file, ["Total",$periodArray['countTotal']]);
                    } else {
                        foreach($periodArray['workshops'] as $workshop) {
                            fputcsv($file, []);
                            fputcsv($file, ["","Taller: " . utf8_decode($workshop['name'])]);
                            fputcsv($file, ["",utf8_decode("Número de alumnos: ") . $workshop['count_inscribed']]);
                            fputcsv($file, ["No", "Alumno", "No. Cta", "Sistema", "Carrera"]);
                            foreach($workshop['students'] as $key => $si){
                                fputcsv($file, [
                                    $key + 1,
                                    $si['name'] . " " .$si['lastname'] . "  " . $si['surname'],
                                    $si['number_id'],
                                    $si['dataStudent'][0]['system'],
                                    $si['dataStudent'][0]['career']['name']
                                ]);
                            }
                        }
                    }
                    fclose($file);
                };
                return response()->stream($callback, 200, $headers);
            } else {
                if(!$career) {
                    $career="";
                }
                if($request->complete === 'no') {
                    if (!$career) {
                        $career = "";
                    }
                    $pdf = PDF::loadView('pdf.reportOneA' , ['period' => $periodArray, 'career' => $career]); 
                } else {
                    if (!$career) {
                        $career = "";
                    }
                    $pdf = PDF::loadView('pdf.reportOneB' , ['period' => $periodArray, 'career' => $career]); 
                }
                return $pdf->download('reporte_1.pdf');
            }
        } else {
            abort(404);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/report/two",
     *      summary="Download report 2",
     *      description="Download report 2",
     *      operationId="two",
     *      tags={"Report"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="period", type="integer"),
     *              @OA\Property(property="start", type="date"),
     *              @OA\Property(property="finish", type="date"),
     *              @OA\Property(property="typeFile", type="string", example="csv|pdf")
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Return report number two.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function two(Request $request) {
        ini_set('max_execution_time', 180);
        $period = Period::find($request->period);
        if($period){
            $s = [];
            $period->workshops_period;
            $periodArray = [
                'name' => $period->name,
                'workshops' => [],
                'total' => 0
            ];
            $timeStart = strtotime(Dates::changeFormat($request->start, 'Y-m-d'));
            $timeFinish = strtotime(Dates::changeFormat($request->finish, 'Y-m-d'));
            foreach($period->workshops_period as $key => $wp) {
                if(!isset($periodArray['workshops'][$wp->workshop->id]['count_inscribed'])){
                    $periodArray['workshops'][$wp->workshop->id]['count_inscribed'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['students'] = [];
                    $periodArray['workshops'][$wp->workshop->id]['total'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['name'] = $wp->workshop->name;
                }
                foreach($period->workshops_period[$key]->students_inscribed as $si){
                    if ($si->pivot->ticket_date) {
                        $siTime = strtotime($si->pivot->ticket_date);    
                    } else {
                        $siTime = strtotime(Dates::changeFormatUTC($si->pivot->created_at->format('Y-m-d H:i:s'), 'Y-m-d', true));
                    }
                    if($si->pivot->status == 1 && $siTime >= $timeStart && $siTime <= $timeFinish){
                        $periodArray['workshops'][$wp->workshop->id]['count_inscribed']++;
                        $periodArray['workshops'][$wp->workshop->id]['total'] += $si->pivot->quota + $si->pivot->extra - $si->pivot->scholarship;
                        $periodArray['total'] += $si->pivot->quota + $si->pivot->extra - $si->pivot->scholarship;
                        $periodArray['workshops'][$wp->workshop->id]['students'][] = $si;
                    }
                }
            }
            if($request->typeFile === 'csv') {
                $headers = array(
                    "Content-Encoding" => "UTF-8",
                    "Content-type" => "text/csv; charset=utf-8",
                    "Content-Disposition" => "attachment; filename=file.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                $callback = function() use ($periodArray, $request)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, []);
                    fputcsv($file, ["Periodo", $periodArray['name']]);
                    fputcsv($file, ["NO. RECIBO", "FECHA", "IMPORTE" , "NOMBRE", "TALLER", "TIPO","SISTEMA", "CARRERA", "NO. CTA"]);
                    foreach($periodArray['workshops'] as $workshop) {
                        foreach($workshop['students'] as $key => $si){
                            if($si->pivot->scholarship && $si->pivot->scholarship > 0){
                                $folio = "BECA";
                            } else {
                                $folio = $si->pivot->ticket_folio;
                            }
                            $careerStudent = "";
                            $system = "";
                            if($si->isRole('student')){
                                $careerStudent = $si->dataStudent[0]->career->name;
                                $system = $si->dataStudent[0]->system;
                                $student = "Alumno";
                            } else if($si->isRole('exstudent')){
                                $careerStudent = $si->dataStudent[0]->career->name;
                                $system = $si->dataStudent[0]->system;
                                $student = "Exalumno";
                            } else if($si->isRole('employee')){
                                $student = "Trabajador";
                            } else if($si->isRole('external')){
                                $student = "Externo";  
                            }
                            if ($si->pivot->ticket_date) {
                                $datesi = Dates::changeFormatUTC($si->pivot->ticket_date, 'd-m-Y');
                            } else {
                                $datesi = Dates::changeFormatUTC($si->pivot->created_at, 'd-m-Y');
                            }
                            fputcsv($file, [
                                $folio,
                                $datesi,
                                FormatData::currencyFormat($si->pivot->quota + $si->pivot->extra - $si->pivot->scholarship),
                                $si->name . " " .$si->lastname . "  " . $si->surname,
                                $workshop['name'],
                                $student,
                                $system,
                                $careerStudent,
                                $si->number_id
                            ]);
                        }
                        fputcsv($file, [
                            "",
                            "Subtotal",
                            FormatData::currencyFormat($workshop['total']),
                            "",
                            "",
                            ""
                        ]);
                    }
                    fputcsv($file, [
                        
                    ]);
                    fputcsv($file, [
                        "",
                        "Total",
                        FormatData::currencyFormat($periodArray['total']),
                        "",
                        "",
                        ""
                    ]);
                    fclose($file);
                };
                return response()->stream($callback, 200, $headers);
            } else {
                $pdf = PDF::loadView('pdf.reportTwo' , ['period' => $periodArray]);
                return $pdf->download('reporte_2.pdf');
            }
        } else {
            abort(404);
        }
    }
    /**
     * 
     * @OA\Post(
     *      path="/api/report/three",
     *      summary="Download report 3",
     *      description="Download report 3",
     *      operationId="three",
     *      tags={"Report"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="period", type="integer"),
     *              @OA\Property(property="start", type="date"),
     *              @OA\Property(property="finish", type="date"),
     *              @OA\Property(property="typeFile", type="string", example="csv|pdf")
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Return report number three.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
     public function three(Request $request) {
        ini_set('max_execution_time', 180);
        $period = Period::find($request->period);
        if($period){
            $s = [];
            $period->workshops_period;
            $periodArray = [
                'name' => $period->name,
                'workshops' => [],
                'total' => 0,
                'count_inscribed' => 0,
                'count_inscribed_student' => 0,
                'count_inscribed_exstudent' => 0,
                'count_inscribed_employee' => 0,
                'count_inscribed_external' => 0,
                'months' => []
            ];
            $timeStart = strtotime(Dates::changeFormat($request->start, 'Y-m-d'));
            $timeFinish = strtotime(Dates::changeFormat($request->finish, 'Y-m-d'));
            foreach($period->workshops_period as $key => $wp) {
                if(!isset($periodArray['workshops'][$wp->workshop->id]['count_inscribed'])){
                    $periodArray['workshops'][$wp->workshop->id]['count_inscribed'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['count_inscribed_student'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['total_inscribed_student'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['count_inscribed_exstudent'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['total_inscribed_exstudent'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['count_inscribed_employee'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['total_inscribed_employee'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['count_inscribed_external'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['total_inscribed_external'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['total'] = 0;
                    $periodArray['workshops'][$wp->workshop->id]['name'] = $wp->workshop->name;
                }
                
                foreach($period->workshops_period[$key]->students_inscribed as $si){
                    if ($si->pivot->ticket_date) {
                        $siTime = strtotime($si->pivot->ticket_date);    
                    } else {
                        $siTime = strtotime(Dates::changeFormatUTC($si->pivot->created_at->format('Y-m-d H:i:s'), 'Y-m-d', true));
                    }
                    if($si->pivot->status == 1 && $siTime >= $timeStart && $siTime <= $timeFinish){
                        $periodArray['count_inscribed']++;
                        $periodArray['workshops'][$wp->workshop->id]['count_inscribed']++;
                        $totals = $si->pivot->quota + $si->pivot->extra - $si->pivot->scholarship;
                        $periodArray['workshops'][$wp->workshop->id]['total'] += $totals;
                        $periodArray['total'] += $totals;
                        if ($si->pivot->ticket_date) {
                            $month = Dates::changeFormatUTC($si->pivot->ticket_date, 'm', true);
                        } else {
                            $month = Dates::changeFormatUTC($si->pivot->created_at->format('Y-m-d H:i:s'), 'm', true);
                        }
                        if(!isset($periodArray['months'][$month])){
                            $periodArray['months'][$month] = 0;
                        }
                        $periodArray['months'][$month] += $totals;
                        if ($si->isRole('student')) {
                            $periodArray['count_inscribed_student']++;
                            $periodArray['workshops'][$wp->workshop->id]['count_inscribed_student']++;
                            $periodArray['workshops'][$wp->workshop->id]['total_inscribed_student'] += $totals;
                        } else if($si->isRole('exstudent')) {
                            $periodArray['count_inscribed_exstudent']++;
                            $periodArray['workshops'][$wp->workshop->id]['count_inscribed_exstudent']++;
                            $periodArray['workshops'][$wp->workshop->id]['total_inscribed_exstudent'] += $totals;
                        } else if($si->isRole('employee')) {
                            $periodArray['count_inscribed_employee']++;
                            $periodArray['workshops'][$wp->workshop->id]['count_inscribed_employee']++;
                            $periodArray['workshops'][$wp->workshop->id]['total_inscribed_employee'] += $totals;
                        } else if($si->isRole('external')) {
                            $periodArray['count_inscribed_external']++;
                            $periodArray['workshops'][$wp->workshop->id]['count_inscribed_external']++;
                            $periodArray['workshops'][$wp->workshop->id]['total_inscribed_external'] += $totals;
                        }

                    }
                }
            }
            if($request->typeFile === 'csv') {
                $headers = array(
                    "Content-Encoding" => "UTF-8",
                    "Content-type" => "text/csv; charset=utf-8",
                    "Content-Disposition" => "attachment; filename=file.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                $callback = function() use ($periodArray, $request)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, []);
                    fputcsv($file, ["Periodo", $periodArray['name']]);
                    fputcsv($file, ["TALLER", "UNIVERSITARIOS", "EGRESADOS", "TRABAJADORES", "EXTERNOS", "TOTAL", "INGRESO"]);
                    foreach($periodArray['workshops'] as $workshop) {
                        fputcsv($file, [
                            $workshop['name'],
                            $workshop['count_inscribed_student'],
                            $workshop['count_inscribed_exstudent'],
                            $workshop['count_inscribed_employee'],
                            $workshop['count_inscribed_external'],
                            $workshop['count_inscribed'],
                            FormatData::currencyFormat($workshop['total'])
                        ]);
                    }
                    fputcsv($file, [
                        "",
                        $periodArray['count_inscribed_student'],
                        $periodArray['count_inscribed_exstudent'],
                        $periodArray['count_inscribed_employee'],
                        $periodArray['count_inscribed_external'],
                        $periodArray['count_inscribed'],
                        FormatData::currencyFormat($periodArray['total'])
                    ]);
                    fputcsv($file, []);
                    foreach($periodArray['months'] as $key => $month){
                        fputcsv($file, [
                            Dates::getMonthName($key),
                            FormatData::currencyFormat($month)
                        ]);
                    }
                    fputcsv($file, [
                        "Total",
                        FormatData::currencyFormat($periodArray['total'])
                    ]);
                    fclose($file);
                };
                return response()->stream($callback, 200, $headers);
            } else {
                $pdf = PDF::loadView('pdf.reportThree' , ['period' => $periodArray]);
                return $pdf->download('reporte_3.pdf');
                // return response()->json(['period' => $periodArray]);
            }
        } else {
            abort(404);
        }
     }
    /**
     * @OA\Post(
     *      path="/api/report/events",
     *      summary="Download report events",
     *      description="Download report events",
     *      operationId="events",
     *      tags={"Report"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="id", type="integer", description="Event Id"),
     *              @OA\Property(property="typeFile", type="string", example="csv|pdf")
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * ) 
     * Return report events.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function events(Request $request) {
        $event = Event::find($request->id);
        if($event){
            if($request->typeFile === 'csv') {
                $headers = array(
                    "Content-Encoding" => "UTF-8",
                    "Content-type" => "text/csv; charset=utf-8",
                    "Content-Disposition" => "attachment; filename=file.csv",
                    "Pragma" => "no-cache",
                    "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
                    "Expires" => "0"
                );
                $callback = function() use ($event, $request)
                {
                    $file = fopen('php://output', 'w');
                    fputcsv($file, []);
                    fputcsv($file, ["Evento", utf8_decode($event->title)]);
                    fputcsv($file, ["Tipo", $event->event_category->name]);
                    fputcsv($file, ["Descripción", utf8_decode($event->description)]);

                    foreach($event->showings as $showing) {
                        fputcsv($file, ["Presentación",Dates::changeFormatUTC($showing->start, 'd-m-Y H:i')]);
                        fputcsv($file, ["No Cta.","Nombre", "Primer Apellido", "Segundo Apellido", "Asistencia", "Tipo de Alumno", "Carrera"]);
                        foreach($showing->students_inscribed as $student) {
                            $typeStudent;
                            if($student->isRole('student')){
                                $career = $student->dataStudent[0]->career->name;
                                $typeStudent = 'Alumno';
                            } else if($student->isRole('exstudent')){
                                $typeStudent = $career = "Exalumno";
                            } else if($student->isRole('employee')){
                                $typeStudent = $career = "Trabajador";
                            } else if($student->isRole('external')){
                                $typeStudent = $career = "Externo";  
                            }
                            fputcsv($file, [
                                $student->number_id,
                                utf8_decode($student->name),
                                utf8_decode($student->lastname),
                                utf8_decode($student->surname),
                                $student->pivot->status ? "Si":"No",
                                utf8_decode($typeStudent),
                                utf8_decode($career)
                            ]);
                        }
                        fputcsv($file, ["Total",count($showing->students_inscribed)]);
                    }
                    fclose($file);
                };
                return response()->stream($callback, 200, $headers);
            } else {
                $pdf = PDF::loadView('pdf.reportEvent' , ['event' => $event]); 
                return $pdf->download('reporte_event.pdf');
            }
        } else {
            abort(404);
        }
    }
}
