<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offering;
use Validator;
use Illuminate\Support\Facades\Storage;

class OfferingController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/offering",
     *      operationId="getOfferingList",
     *      tags={"Offering"},
     *      summary="Display a listing of the offerings",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Offering.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Offering")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', false);
        $orderBy = $request->query('orderBy', 'created_at');

        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Offering::withTrashed()->count();
            if($limit == -1)
                $offerings = Offering::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $offerings = Offering::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        } else {
            $totalRegister = Offering::count();
            if($limit == -1)
                $offerings = Offering::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $offerings = Offering::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }
        return response()->json($offerings)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * 
     * @OA\Post(
     *      path="/api/offering",
     *      summary="Store a offering",
     *      description="Store a offering object",
     *      operationId="storeOffering",
     *      tags={"Offering"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Offering")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'event_id' => 'required|numeric|exists:events,id',
            'title' => 'required|max:255|alpha_numeric_space',
            'description' => 'nullable|max:65535',
            'image' => 'required|max:255',
            'number' => 'required|integer',
        ];
        $input = $request->only(
            'event_id',
            'title',
            'description',
            'image',
            'number'
        );
        $messages = [
            'title.required' => 'Se require título',
            'title.alpha_numeric_space' => 'Título debe contener solo carácteres alfanuméricos',
            'title.max' => 'Título debe contener máximo 255 carácteres',
            'description.required' => 'Se requiere Descripción',
            'description.max' => 'Descripción debe contener máximo 65535 carácteres',
            'image.max' => 'Imágen debe contener máximo 255 carácteres',
            'image.required' => 'Imágen de ofrenda requerida',
            'number.required' => 'Número de ofrenda requerida',
            'number.integer' => 'Número de ofrenda debe ser númerico',
            'event_id.required' => 'Se requiere evento de ofrenda',
            'event_id.exists' => 'No existe el evento de ofrenda',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        if($request->image){
            Storage::disk('public')->put("img/offerings/$request->image", Storage::disk('public')->get("tmp/$request->image"));
        }
        Offering::create([
            'event_id' => $request->event_id,
            'title' => $request->title,
            'description' => $request->description,
            'image' => $request->image,
            'number' => $request->number
        ]);
        return response()->json(['success'=> true, 'message'=> 'Ofrenda registrada']);
    }

    /**
     * @OA\Get(
     *      path="/api/offering/{id}",
     *      operationId="getOffering",
     *      tags={"Offering"},
     *      summary="Display a offering",
     *      @OA\Parameter(
     *          description="ID of offering",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a offering.",
     *          @OA\JsonContent(ref="#/components/schemas/Offering")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $offering = Offering::find($id);
        if($offering){
            return response()->json($offering);
        }else{
            return response()->json(['success' => false, 'message' => 'Ofrenda no encontrada'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/offering/{id}",
     *      summary="Update a offering",
     *      description="Update a offering object",
     *      operationId="updateOffering",
     *      tags={"Offering"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Offering",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Offering")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $offering = Offering::withTrashed()->find($id);
        if($offering){
            $rules = [
                'event_id' => 'required|numeric|exists:events,id',
                'title' => 'required|max:255|alpha_numeric_space',
                'description' => 'nullable|max:65535',
                'image' => 'required|max:255',
                'number' => 'required|integer',
            ];
            $input = $request->only(
                'event_id',
                'title',
                'description',
                'image',
                'number'
            );
            $messages = [
                'title.required' => 'Se require título',
                'title.alpha_numeric_space' => 'Título debe contener solo carácteres alfanuméricos',
                'title.max' => 'Título debe contener máximo 255 carácteres',
                'description.required' => 'Se requiere Descripción',
                'description.max' => 'Descripción debe contener máximo 65535 carácteres',
                'image.max' => 'Imágen debe contener máximo 255 carácteres',
                'image.required' => 'Imágen de ofrenda requerida',
                'number.required' => 'Número de ofrenda requerida',
                'number.integer' => 'Número de ofrenda debe ser númerico',
                'event_id.required' => 'Se requiere evento de ofrenda',
                'event_id.exists' => 'No existe el evento de ofrenda',
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            if($request->image && $offering->image !== $request->image){
                Storage::disk('public')->delete('img/offerings/' . $offering->image);
                Storage::disk('public')->put("img/offerings/$request->image", Storage::disk('public')->get("tmp/$request->image"));
                $offering->image = $request->image;
            }
            $offering->event_id = $request->event_id;
            $offering->title = $request->title;
            $offering->description = $request->description;
            $offering->number = $request->number;
            $offering->save();
            return response()->json(['success'=> true, 'message'=> 'Ofrenda guardada']);
        }else{
            return response()->json(['success' => false, 'message' => 'Ofrenda no encontrada'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/offering/{id}",
     *      summary="Delete a offering",
     *      description="Delete a offering object",
     *      operationId="deleteOffering",
     *      tags={"Offering"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Offering",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Offering::destroy($id);
        return response()->json(['success'=> true, 'message'=> 'Ofrenda eliminada']);
    }

    /**
     * @OA\Post(
     *      path="/api/offering/{id}/restore",
     *      summary="Restore a offering",
     *      description="Restore a offering object",
     *      operationId="restoreOffering",
     *      tags={"Offering"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of offering",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $offering = Offering::withTrashed()->find($id);
        if($offering){
            $offering->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/offering/by-event/{event_id}",
     *      operationId="getByEvent",
     *      tags={"Offering"},
     *      summary="Display a listing of the offerings by event id",
     *      @OA\Parameter(
     *          description="Event Id",
     *          in="path",
     *          name="event_id",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Offering.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Offering")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource by event.
     *
     * @return \Illuminate\Http\Response
     */
    public function byEvent($event_id)
    {
        $offerings = Offering::where('event_id' , $event_id)->get();
        return response()->json($offerings);
    }
}
