<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classroom;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str as Str;

class ClassroomController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/classroom",
     *      operationId="getClassroomList",
     *      tags={"Classrooms"},
     *      summary="Display a listing of the classroom",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Classroom.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Classroom")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'name');
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Classroom::withTrashed()->count();
            if($limit == -1){
                $classrooms = Classroom::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            } else {
                $classrooms = Classroom::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        } else {
            $totalRegister = Classroom::count();
            if ($limit == -1){
                $classrooms = Classroom::orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->get();
            } else {
                $classrooms = Classroom::orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        }
        return response()->json($classrooms)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/classroom",
     *      summary="Store a classroom",
     *      description="Store a classroom object",
     *      operationId="storeClassroom",
     *      tags={"Classrooms"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Classroom")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|alpha_numeric_space|unique:classrooms',
            'quota' => 'required|integer'
        ];
        $input = $request->only(
            'name',
            'quota'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.alpha_numeric_space' => 'Nombre debe contener solo carácteres alfanuméricos',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'name.unique' => 'Nombre ya existe',
            'quota.required' => 'Se requiere cupo',
            'quota.integer' => 'Cupo debe ser numérico'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        Classroom::create([
            'name' => $request->name,
            'quota' => $request->quota
        ]);
        return response()->json(['message'=> 'Salón registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/classroom/{id}",
     *      operationId="getClassroom",
     *      tags={"Classrooms"},
     *      summary="Display a classroom",
     *      @OA\Parameter(
     *          description="ID of classroom",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a classroom.",
     *          @OA\JsonContent(ref="#/components/schemas/Classroom")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $classroom = Classroom::withTrashed()->find($id);
        } else {
            $classroom = Classroom::find($id);
        }
        if($classroom){
            return response()->json($classroom);
        }else{
            return response()->json(['success' => false, 'message' => 'Carrera no encontrada'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/classroom/{id}",
     *      summary="Update a classroom",
     *      description="Update a classroom object",
     *      operationId="updateClassroom",
     *      tags={"Classrooms"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Classroom",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Classroom")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $classroom = Classroom::withTrashed()->find($id);
        if($classroom){
            $rules = [
                'name' => ['required','max:255','alpha_numeric_space', Rule::unique('classrooms')->ignore($classroom->id)],
                'quota' => 'required|integer'
            ];
            $input = $request->only(
                'name',
                'quota'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.alpha_numeric_space' => 'Nombre debe contener solo carácteres alfanuméricos',
                'name.max' => 'Nombre debe contener máximo 255 carácteres',
                'quota.required' => 'Se requiere cupo',
                'quota.integer' => 'Cupo debe ser numérico'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $classroom->name = $request->name;
            $classroom->quota = $request->quota;
            $classroom->save();
            return response()->json(['message'=> 'Salón actualizado']);
        }else{
            return response()->json(['message' => 'Salón no encontrado'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/classroom/{id}",
     *      summary="Delete a classroom",
     *      description="Delete a classroom object",
     *      operationId="deleteClassroom",
     *      tags={"Classrooms"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Classroom",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $classroom = Classroom::withTrashed()->find($id);
        if ($classroom) {
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'force debe ser true o false, 1 ó 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $classroom->delete();
            } else {
                $classroom->forceDelete();
            }
            return response()->json(['message'=> 'Salón eliminada']);
        } else {
            return response()->json(['message'=> 'Salón no encontrado'], 404);
        }
    }
    /**
     * @OA\Post(
     *      path="/api/classroom/{id}/restore",
     *      summary="Restore a classroom",
     *      description="Restore a classroom object",
     *      operationId="restoreClassroom",
     *      tags={"Classrooms"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of classroom",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $classroom = Classroom::withTrashed()->find($id);
        if($classroom){
            $classroom->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/classroom/check-name/{id?}",
     *      summary="checkNameClassroom",
     *      description="Check if the name exist",
     *      operationId="checkNameClassroom",
     *      tags={"Classrooms"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Classroom",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * API Check Name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkName(Request $request, $id = false){
        if ($id === false) {
            $rules = [
                'name' => 'required|alpha_numeric_space|max:150|unique:classrooms',
            ];
        } else {
            $rules = [
                'name' => ['required', 'alpha_numeric_space', 'max:150', Rule::unique('classrooms')->ignore($id)],
            ];
        }
        $input = $request->only(
            'name'
        );
        $messages = [
            'name.required' => 'required',
            'name.unique' => 'unique'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['message'=> $validator->messages()], 400);
        }
        return response()->json(['message'=> "Don't exist"]);
    }
}
