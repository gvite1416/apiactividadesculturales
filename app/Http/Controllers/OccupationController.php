<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Occupation;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str as Str;

class OccupationController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/occupation",
     *      operationId="getOccupationList",
     *      tags={"Occupation"},
     *      summary="Display a listing of the occupations",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Occupation.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Occupation")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'created_at');
        
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Occupation::withTrashed()->count();
            if($limit == -1)
                $ocupations = Occupation::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $ocupations = Occupation::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        } else {
            $totalRegister = Occupation::count();
            if($limit == -1)
                $ocupations = Occupation::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $ocupations = Occupation::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }

        return response()->json($ocupations)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/occupation",
     *      summary="Store a occupation",
     *      description="Store a occupation object",
     *      operationId="storeOccupation",
     *      tags={"Occupation"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Occupation")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|alpha_numeric_space'
        ];
        $input = $request->only(
            'name'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.alpha_numeric_space' => 'Nombre debe contener solo carácteres alfanuméricos',
            'name.max' => 'Nombre debe contener máximo 255 carácteres'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        Occupation::create([
            'name' => $request->name
        ]);
        return response()->json(['success'=> true, 'message'=> 'Ocupación registrada']);
    }

    /**
     * @OA\Get(
     *      path="/api/occupation/{id}",
     *      operationId="getOccupation",
     *      tags={"Occupation"},
     *      summary="Display a occupation",
     *      @OA\Parameter(
     *          description="ID of occupation",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a occupation.",
     *          @OA\JsonContent(ref="#/components/schemas/Occupation")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $occupation = Occupation::find($id);
        if($occupation){
            return response()->json($occupation);
        }else{
            return response()->json(['success' => false, 'message' => 'Ocupación no encontrada'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/occupation/{id}",
     *      summary="Update a occupation",
     *      description="Update a occupation object",
     *      operationId="updateOccupation",
     *      tags={"Occupation"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Occupation",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Occupation")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $occupation = Occupation::withTrashed()->find($id);
        if($occupation){
            $rules = [
                'name' => 'required|max:255|alpha_numeric_space'
            ];
            $input = $request->only(
                'name'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.alpha_numeric_space' => 'Nombre debe contener solo carácteres alfanuméricos',
                'name.max' => 'Nombre debe contener máximo 255 carácteres'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            $occupation->name = $request->name;
            $occupation->save();
            return response()->json(['success'=> true, 'message'=> 'Ocupación actualizada']);
        }else{
            return response()->json(['success' => false, 'message' => 'Ocupación no encontrada'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/occupation/{id}",
     *      summary="Delete a occupation",
     *      description="Delete a occupation object",
     *      operationId="deleteOccupation",
     *      tags={"Occupation"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Occupation",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $occupation = Occupation::withTrashed()->find($id);
        if ($occupation) {
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'Debe ser true o false, 1 ó 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $occupation->delete();
            } else {
                $occupation->forceDelete();
            }
            return response()->json(['message'=> 'Ocupación eliminada']);
        } else {
            return response()->json(['message'=> 'Occupation Not Found'], 404);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/occupation/{id}/restore",
     *      summary="Restore a occupation",
     *      description="Restore a occupation object",
     *      operationId="restoreOccupation",
     *      tags={"Occupation"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of occupation",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $occupation = Occupation::withTrashed()->find($id);
        if($occupation){
            $occupation->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/occupation/check-name/{id?}",
     *      summary="checkNameOccupation",
     *      description="Check if the name exist",
     *      operationId="checkNameOccupation",
     *      tags={"Occupation"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Occupation",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * API Check Name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkName(Request $request, $id = false){
        if ($id === false) {
            $rules = [
                'name' => 'required|alpha_numeric_space|max:150|unique:occupations',
            ];
        } else {
            $rules = [
                'name' => ['required', 'alpha_numeric_space', 'max:150', Rule::unique('occupations')->ignore($id)],
            ];
        }
        $input = $request->only(
            'name'
        );
        $messages = [
            'name.required' => 'required',
            'name.unique' => 'unique'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 400);
        }
        return response()->json(['message'=> "Don't exist"]);
    }
}
