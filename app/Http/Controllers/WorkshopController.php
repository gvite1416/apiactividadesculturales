<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workshop;
use App\Period;
use App\WorkshopPeriod;
use App\Inscription;
use App\Captureline;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str as Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use QRCode;
use PDF;
use App\Libraries\FormatData;
use App\Libraries\Dates;
use App\Mail\CapturelineEmail;
use Validator;
use Mail;

class WorkshopController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/workshop",
     *      operationId="getWorkshopList",
     *      tags={"Workshop"},
     *      summary="Display a listing of the workshops",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show workshops.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Workshop")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', false);
        $orderBy = $request->query('orderBy', 'name');

        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Workshop::withTrashed()->count();
            if($limit == -1)
                $workshops = Workshop::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $workshops = Workshop::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        } else {
            $totalRegister = Workshop::count();
            if($limit == -1)
                $workshops = Workshop::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $workshops = Workshop::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }
        return response()->json($workshops)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/workshop",
     *      summary="Store a workshop",
     *      description="Store a workshop object",
     *      operationId="storeWorkshop",
     *      tags={"Workshop"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Workshop")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|alpha_numeric_dash_space|max:150|unique:workshops',
            'path' => 'nullable|max:255',
            'objective' => 'nullable|max:65535',
            'requirement' => 'nullable|max:65535',
            'information' => 'nullable|max:65535',
            'one_more_time' => 'nullable|boolean',
        ];
        $input = $request->only(
            'name',
            'objective',
            'requirement',
            'information',
            'one_more_time',
            'image'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.alpha_numeric_dash_space' => 'Nombre debe contener solo carácteres alfabéticos',
            'name.max' => 'Nombre debe contener máximo 150 carácteres',
            'name.unique' => 'El Nombre del taller ya existe',
            'path.max' => 'Imagen debe contener máximo 255 carácteres',
            'objective.max' => 'Objetivos debe contener máximo 65535 carácteres',
            'requirement.max' => 'Requisitos debe contener máximo 65535 carácteres',
            'information.max' => 'Información debe contener máximo 65535 carácteres',
            'one_more_time.boolean' => 'Campo debe ser booleano'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        if($request->image){
            Storage::disk('public')->put("img/workshops/$request->image", Storage::disk('public')->get("tmp/$request->image"));
        }
        Workshop::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'path' => $request->path,
            'image' => $request->image,
            'objective' => $request->objective,
            'requirement' => $request->requirement,
            'information' => $request->information,
            'one_more_time' => $request->one_more_time
        ]);
        return response()->json(['success'=> true, 'message'=> 'Taller registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/workshop/{id}",
     *      operationId="getWorkshop",
     *      tags={"Workshop"},
     *      summary="Display a workshop",
     *      @OA\Parameter(
     *          description="ID of workshop",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a workshop.",
     *          @OA\JsonContent(ref="#/components/schemas/Workshop")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $workshop = Workshop::find($id);
        if($workshop){
            return response()->json($workshop);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/workshop/by-slug/{slug}",
     *      operationId="getWorkshop",
     *      tags={"Workshop"},
     *      summary="Display a workshop",
     *      @OA\Parameter(
     *          description="slug of workshop",
     *          in="path",
     *          name="slug",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a workshop.",
     *          @OA\JsonContent(ref="#/components/schemas/Workshop")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource by slug.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function bySlug($slug)
    {
        $workshop = Workshop::where('slug' , $slug)->firstOrFail();
        if($workshop){
            return response()->json($workshop);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/workshop/{id}",
     *      summary="Update a workshop",
     *      description="Update a workshop object",
     *      operationId="updateWorkshop",
     *      tags={"Workshop"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Workshop",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Workshop")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $workshop = Workshop::withTrashed()->find($id);
        if($workshop){
            $rules = [
                'name' => ['required','alpha_numeric_dash_space','max:150',Rule::unique('workshops')->ignore($workshop->id)],
                'objective' => 'nullable|alpha_text|max:65535',
                'requirement' => 'nullable|alpha_text|max:65535',
                'information' => 'nullable|alpha_text|max:65535',
                'one_more_time' => 'nullable|boolean',
            ];
            $input = $request->only(
                'name',
                'objective',
                'requirement',
                'information',
                'one_more_time',
                'image'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.alpha_numeric_dash_space' => 'Nombre debe contener solo carácteres alfabéticos',
                'name.max' => 'Nombre debe contener máximo 150 carácteres',
                'name.unique' => 'El Nombre del taller ya existe',
                'objective.alpha_text' => 'Objetivos debe contener solo carácteres alfanuméricos',
                'objective.max' => 'Objetivos debe contener máximo 65535 carácteres',
                'requirement.alpha_text' => 'Requisitos debe contener solo carácteres alfanuméricos',
                'requirement.max' => 'Requisitos debe contener máximo 65535 carácteres',
                'information.alpha_text' => 'Información debe contener solo carácteres alfanuméricos',
                'information.max' => 'Información debe contener máximo 65535 carácteres',
                'one_more_time.boolean' => 'Campo debe ser booleano'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            if($request->image && $workshop->image !== $request->image){
                Storage::disk('public')->delete('img/workshops/' . $workshop->image);
                Storage::disk('public')->put("img/workshops/$request->image", Storage::disk('public')->get("tmp/$request->image"));
                $workshop->image = $request->image;
            }
            $workshop->name = $request->name;
            $workshop->slug = Str::slug($request->name);
            $workshop->objective = $request->objective;
            $workshop->requirement = $request->requirement;
            $workshop->information = $request->information;
            $workshop->one_more_time = ($request->one_more_time) ? true : false;
            $workshop->save();
            return response()->json(['success'=> true, 'message'=> 'Taller actualizado']);
        }else{
            return response()->json(['success'=> false, 'message'=> 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/workshop/{id}",
     *      summary="Delete a workshop",
     *      description="Delete a workshop object",
     *      operationId="deleteWorkshop",
     *      tags={"Workshop"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of workshop",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Workshop::destroy($id);
        return response()->json(['success'=> true, 'message'=> 'Se eliminó correctamente']);
    }

    /**
     * @OA\Post(
     *      path="/api/workshop/{id}/restore",
     *      summary="Restore a workshop",
     *      description="Restore a workshop object",
     *      operationId="restoreWorkshop",
     *      tags={"Workshop"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of workshop",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $workshop = Workshop::withTrashed()->find($id);
        if($workshop){
            $workshop->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/workshop/check-name/{id?}",
     *      summary="checkNameWorkshop",
     *      description="Check if the name exist",
     *      operationId="checkNameWorkshop",
     *      tags={"Workshop"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Id of workshop",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * API Check Name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkName(Request $request, $id = false){
        if ($id === false) {
            $rules = [
                'name' => 'required|alpha_numeric_dash_space|max:150|unique:workshops',
            ];
        } else {
            $rules = [
                'name' => ['required', 'alpha_numeric_dash_space', 'max:150', Rule::unique('workshops')->ignore($id)],
            ];
        }
        $input = $request->only(
            'name'
        );
        $messages = [
            'name.required' => 'required',
            'name.unique' => 'unique'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 400);
        }
        return response()->json(['success'=> true]);
    }
    /**
     * @OA\Get(
     *      path="/api/workshop/current-period",
     *      operationId="currentPeriod",
     *      tags={"Workshop"},
     *      summary="Display a listing of the workshops",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show workshops by current period.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Workshop")
     *          )
     *      )
     * )
     * Display a listing of the resource by current Period.
     *
     * @return \Illuminate\Http\Response
     */
    public function currentPeriod(Request $request)
    {
        $period = Period::current()->first();
        if($period) {
            $workshopsAux = $period->workshops()->where('workshop_periods.deleted_at', '=', null)->get();
            $workshops = [];
            $ids = [];
            if($request->limit) {
                $limit = $request->limit;
            } else {
                $limit = count($workshopsAux);
            }
            foreach($workshopsAux as $workshop) {
                if(array_search($workshop['id'],$ids) === false && count($ids) < $limit) {
                    $w = $workshop->toArray();
                    unset($w['pivot']);
                    $workshops[] = $w;
                    $ids[] = $workshop['id'];
                }
            }
        } else {
            if($request->limit) {
                $workshops = Workshop::limit($request->limit)->get();
            } else {
                $workshops = Workshop::get();
            }
        }
        return response()->json($workshops);
    }

    /**
     * @OA\Get(
     *      path="/api/workshop/by-slug-current-period/{slug}",
     *      operationId="bySlugCurrentPeriod",
     *      tags={"Workshop"},
     *      summary="Display a listing of the workshops",
     *      @OA\Parameter(
     *          description="slug",
     *          in="path",
     *          name="slug",
     *          required=true,
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a workshop.",
     *          @OA\JsonContent(ref="#/components/schemas/Workshop")              
     *      )
     * )
     * Display the specified resource by slug.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function bySlugCurrentPeriod($slug)
    {
        $workshopAux = Workshop::where('slug' , $slug)->firstOrFail();
        if($workshopAux){
            $period = Period::current()->first();
            if($period) {
                $workshop = $workshopAux->toArray();
                $wps = WorkshopPeriod::where('workshop_id','=',$workshop['id'])->where('period_id','=', $period->id)->get();
                $workshop['workshop_period'] = [];
                $workshop['inscribed'] = false;
                $user = auth()->user();
                if($user) {
                    $workshop['has_user'] = true;
                }
                foreach($wps as $key => $wp){
                    $workshop['workshop_period'][$key] = $this->getWorkshopPeriod($wp, $user);
                    if (isset($workshop['workshop_period'][$key]['inscribed']) && $workshop['workshop_period'][$key]['inscribed']) {
                        $workshop['inscribed'] = true;
                    }
                }
                if ($workshop['inscribed'] && $workshop['one_more_time'] == 0) {
                    foreach ($workshop['workshop_period'] as $key => $wp) {
                        if(!$workshop['workshop_period'][$key]['inscribed']) {
                            $workshop['workshop_period'][$key]['can'] = false;
                            $workshop['workshop_period'][$key]['cant_message'] = 'Ya no puedes inscribirte a más horarios';
                        }
                    }
                }
            } else {
                $workshop = $workshopAux->toArray();
            }
            return response()->json($workshop);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/workshop/inscribe",
     *      summary="Inscribe current user to workshop",
     *      description="Inscribe current user to workshop",
     *      operationId="inscribe",
     *      tags={"Workshop"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="workshop_period_id", type="integer"),
     *              @OA\Property(property="college_degree_option", type="boolean"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     */
    public function inscribe(Request $request){
        $rules = [
            'workshop_period_id' => 'required|exists:workshop_periods,id',
        ];
        $input = $request->only(
            'workshop_period_id'
        );
        $messages = [
            'workshop_period_id.required' => 'Se requiere taller por periodo',
            'workshop_period_id.exists' => 'No existe el taller periodo'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $user = auth()->user();
        if ($user) {
            $inscriptions = $user->inscriptions()->get();
            $canInsc = true;
            $message = '';
            $wp = WorkshopPeriod::find($request->workshop_period_id);
            foreach($inscriptions as $insc) {
                if ($wp->id == $insc->workshop_period_id) {
                    if($insc->status === 0) {
                        $canInsc = false;
                        $message = "Ya estás inscrito al Taller. Pero aún debes pasar al Departamento de Actividades Culturales y/o realizar el pago";
                    } else if($insc->status === 1) {
                        $canInsc = false;
                        $message = "Ya estás inscrito al Taller y se ha validado correctamente";
                    } else if($insc->status === 2) {
                        $canInsc = false;
                        $message = "Se te ha penalizado un día por no completar el registro, intentalo más tarde.";
                    } else if($insc->status === 4) {
                        $canInsc = false;
                        $message = "Línea de captura enviada, esperando que subas los archivos para validar inscripción";
                    } else if($insc->status === 5) {
                        $canInsc = false;
                        $message = "Archivos subidos, esperando que se valide por el departamento";
                    }

                } else if($insc->workshop_period->id == $wp->id && $inscription->workshop_period->period->id == $wp->period->id && $wp->workshop->one_more_time == 0 ){
                    $canInsc = false;
                    $message = "Sólo te puedes inscribir una vez al taller";
                }

            }
            if ($canInsc) {
                $workshop_period = $this->getWorkshopPeriod($wp, $user);
                if($workshop_period['can']) {
                    $folio = -1;
                    $b = true;
                    while ($b) {
                        $folio = mt_rand(1, 2147483647);
                        if (!Inscription::where('folio' , '=', $folio)->first()) {
                            $b = false;
                        }
                    }
                    $insc = Inscription::create([
                        'workshop_period_id' => $wp->id,
                        'user_id' => $user->id,
                        'folio' => $folio,
                        'quota' => $workshop_period['student_price'],
                        'college_degree_option' => $request->college_degree_option == 1 || $request->college_degree_option == true ? $request->college_degree_option : 0
                    ]);
                    if(($workshop_period['students_preinscribed_count'] + 1) >= 7 ||
                        ($workshop_period['students_inscribed_verify_count'] + 1) >= 7 ||
                        ($workshop_period['students_inscribed_count'] + 1) >= $workshop_period['quota']){
                        $wp->refresh();
                        $message = $this->sendCapturelines($wp);
                    }
                    return response()->json(['success'=> true, 'id' => $insc->id, "messages" => $message]);
                } else {
                    return response()->json(['success'=> false, 'message' => $workshop_period['cant_message']]);
                }
            } else {
                return response()->json(['success'=> false, 'message' => $message]);
            }
            
        } else {
            return response()->json(['success'=> false, 'message' => 'Debes hacer login']);
        }
    }

    private function getWorkshopPeriod($wp, $user = false) {
        $wp->teacher;
        $wp->classroom;
        $wp->schedules;
        $wp->students;
        $wp->period;
        $wp->students_inscribed_count;
        if($wp->all_campus == 0){
            $wp->campus;
        }
        $workshop_period = $wp->toArray();
        $workshop_period['inscribed'] = false;
        unset($workshop_period['teacher']['celphone']);
        unset($workshop_period['teacher']['email']);
        unset($workshop_period['teacher']['phone']);
        if( $user ) {
            // check if student can incribe by role
            $can = false;
            $role_user = '';
            $limit = -1;
            if(strtotime("now") >= strtotime($wp->period->inscription_start) &&
                strtotime("now") <= strtotime($wp->period->inscription_finish)){
                foreach($wp->students as $student) {
                    foreach($user->roles as $role) {
                        $role_user = $role->slug;
                        if($role->slug === $student->slug) {
                            $can = true;
                            $limit = $student->pivot->limit;
                            $workshop_period['student_price'] = $student->pivot->price;
                        }
                    }
                }
                
                if ($can) {
                    if($workshop_period['students_inscribed_count'] >= $workshop_period['quota']) {
                        $can = false;
                        $workshop_period['cant_message'] = 'Cupo lleno';
                    } else {
                        $count_role_user = 0;
                        $workshop_period['inscribed'] = false;
                        foreach($wp->students_inscribed as $student) {
                            if($student->id === $user->id){
                                if($student->pivot->status == 0 || $student->pivot->status == 1 || $student->pivot->status == 4 || $student->pivot->status == 5) {
                                    $workshop_period['inscribed'] = true;
                                    $can = false;
                                    $workshop_period['cant_message'] = 'Ya estás inscrito a éste taller';
                                } else if ($student->pivot->status == 2) {
                                    $workshop_period['inscribed'] = true;
                                    $can = false;
                                    $workshop_period['cant_message'] = 'Se ha penalizado un día por no terminar la inscripción a tiempo, intentalo más tarde.';
                                }
                            }
                            if($student->pivot->status == 0 || $student->pivot->status == 1 || $student->pivot->status == 4 || $student->pivot->status == 5) {
                                $has_role = false;
                                foreach($student->roles as $role) {
                                    if($role->slug == $role_user) {
                                        $has_role = true;
                                    }
                                }
                                if($has_role) {
                                    $count_role_user++;
                                }
                            }
                        }
                        if($can){
                            if ($limit !== -1 && $count_role_user >= $limit) {
                                $can = false;
                                $workshop_period['cant_message'] = 'Ya se cubrieron los lugares asignados a tu tipo de alumno';
                            }
                        }
                    }
                } else {
                    $workshop_period['cant_message'] = 'Tu tipo de alumno no puede inscribir este taller';
                }
            } else {
                $workshop_period['cant_message'] = 'Las inscripciones aún no comienzan';
            }
            $workshop_period['can'] = $can;
        }
        return $workshop_period;
    }

    function getInscribePdf(Request $request, $workshop_period_id) {
        $user = auth()->user();
        $inscription = Inscription::where('workshop_period_id', '=', $workshop_period_id)->where('user_id', '=', $user->id)->first();
        if($inscription && $inscription->user->id == $user->id) {
            $role = false;
            foreach($inscription->user->roles as $r) {
                $role = $r;
            }
            $inscription->workshop_period->workshop;
            $inscription->workshop_period->period->requirements;
            $data = ['inscription' => $inscription , 'role' => $role, 'token' => $request->query('token')];
            $pdf = PDF::loadView('pdf.inscription', $data);
            return $pdf->download('inscription.pdf');
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }

    private function sendCapturelines($wp) {
        $emailsToSend = [];
        foreach($wp->students_inscribed as $student) {
            if($student->pivot->status == 0){
                $inscription = Inscription::find($student->pivot->id);
                $captureline = Captureline::where("value", "=", $inscription->quota)->where("inscription_id", "=", null)->where("period_id", "=", $wp->period_id)->first();
                if($captureline) {
                    $captureline->inscription_id = $inscription->id;
                    $captureline->save();
                    $inscription->status = 4;
                    $inscription->captureline_id = $captureline->id;
                    $inscription->expiration = Dates::addNowTwoDays();
                    $inscription->save();
                    $emailsToSend[] = [
                        "inscription" => $inscription
                    ];
                }
            }
        }
        $error_message = "";
        $success = true;
        foreach($emailsToSend as $emailData) {
            try {
                Mail::to($emailData['inscription']->user)->queue(new CapturelineEmail($emailData['inscription'], env('URL_WEB', '') . 'mis-talleres/' . $emailData['inscription']->id));
            } catch(\Exception $e) {
                $error_message = $e->getMessage();
                $success = false;
            }
        }
        return ['success' =>$success, 'error' => $error_message];
    }
}
