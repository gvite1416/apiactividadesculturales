<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use DB, Hash, Mail;
use \HttpOz\Roles\Models\Role;
use App\User;
use Illuminate\Validation\Rule;
use App\DataEmployee;

class TeacherController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/teacher",
     *      operationId="getTeacherList",
     *      tags={"Teacher"},
     *      summary="Display a listing of the teachers",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Teacher.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/User")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'created_at');

        $role = Role::findBySlug('teacher');
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = $role->users()->withTrashed()->count();
            if($limit == -1)
                $teachers = $role->users()->withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $teachers = $role->users()->withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        } else {
            $totalRegister = $role->users()->count();
            if($limit == -1)
                $teachers = $role->users()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $teachers = $role->users()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }
        return response()->json($teachers)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }


    /**
     * @OA\Post(
     *      path="/api/teacher",
     *      summary="Store a teacher",
     *      description="Store a teacher object",
     *      operationId="storeTeacher",
     *      tags={"Teacher"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|alpha_space',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'lastname' => 'required|alpha_space|max:255',
            'surname' => 'nullable|alpha_space|max:255',
            'phone' => 'nullable',
            'gender' => 'required|max:50|alpha_space',
            'celphone' => 'nullable'
        ];
        $input = $request->only(
            'name',
            'email',
            'password',
            'password_confirmation',
            'lastname',
            'phone',
            'celphone',
            'surname',
            'gender'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.alpha_space' => 'Nombre debe contener solo carácteres alfabéticos',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'email.required' => 'Se require email',
            'email.email' => 'Introduce un email válido',
            'email.max' => 'El email debe tener máximo 150 caracteres',
            'email.unique' => 'El email ya existe',
            'password.required' => 'Se require contraseña',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'password.min' => 'La contraseña debe contener mínimo 6 caracteres',
            'lastname.required' => 'Se require primer apellido',
            'lastname.alpha_space' => 'Primer Apellido debe contener solo carácteres alfabéticos',
            'surname.alpha_space' => 'Segundo Apellido debe contener solo carácteres alfabéticos',
            'surname.max' => 'Segundo Apellido debe contener máximo 255 carácteres',
            'gender.required' => 'Se require Género',
            'gender.alpha_space' => 'Género debe contener solo carácteres alfabéticos',
            'gender.max' => 'Género debe contener máximo 50 carácteres'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $name = $request->name;
        $email = $request->email;
        $user = User::create([
            'name' => $name, 
            'email' => $email, 
            'password' => Hash::make($request->password),
            'lastname' => $request->lastname,
            'surname' => $request->surname,
            'phone' => $request->phone,
            'celphone' => $request->celphone,
            'gender' => $request->gender,
            'is_verified' => 1,
            'is_verified_dept' => 1
        ]);
        DataEmployee::Create([
            'user_id' => $user->id,
            'workshift' => 'Ambos',
            'area' => 'Extención Universitaria',
            'campus_id' => 1
        ]);
        $teacherRole = Role::findBySlug('teacher');
        $user->attachRole($teacherRole);
        return response()->json(['success'=> true, 'message'=> 'Profesor registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/teacher/{id}",
     *      operationId="getTeacher",
     *      tags={"Teacher"},
     *      summary="Display a teacher",
     *      @OA\Parameter(
     *          description="ID of teacher",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a teacher.",
     *          @OA\JsonContent(ref="#/components/schemas/User")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = User::find($id);
        if($teacher && $teacher->hasRole('teacher')){
            return response()->json($teacher);
        }else{
            return response()->json(['success' => false, 'message' => 'Profesor no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/teacher/{id}",
     *      summary="Update a teacher",
     *      description="Update a teacher object",
     *      operationId="updateTeacher",
     *      tags={"Teacher"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Teacher",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = User::withTrashed()->find($id);
        if($teacher && $teacher->hasRole('teacher')){
            $rules = [
                'name' => 'required|max:255|alpha_space',
                'email' => ['required','email','max:255',Rule::unique('users')->ignore($teacher->id)],
                'lastname' => 'required|alpha|max:255',
                'surname' => 'nullable|alpha|max:255',
                'phone' => 'nullable',
                'celphone' => 'nullable',
                'gender' => 'required|max:50|alpha_space'
            ];
            $input = $request->only(
                'name',
                'email',
                'lastname',
                'surname',
                'number_id',
                'phone',
                'celphone',
                'gender'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.alpha_space' => 'Nombre debe contener solo carácteres alfabéticos',
                'name.max' => 'Nombre debe contener máximo 255 carácteres',
                'email.required' => 'Se require email',
                'email.email' => 'Introduce un email válido',
                'email.max' => 'El email debe tener máximo 150 caracteres',
                'email.unique' => 'El email ya existe',
                'password.required' => 'Se require contraseña',
                'password.confirmed' => 'Las contraseñas no coinciden',
                'password.min' => 'La contraseña debe contener mínimo 6 caracteres',
                'lastname.required' => 'Se require primer apellido',
                'lastname.alpha' => 'Primer Apellido debe contener solo carácteres alfabéticos',
                'surname.alpha' => 'Segundo Apellido debe contener solo carácteres alfabéticos',
                'surname.max' => 'Segundo Apellido debe contener máximo 255 carácteres',
                'gender.required' => 'Se require Género',
                'gender.alpha_space' => 'Género debe contener solo carácteres alfabéticos',
                'gender.max' => 'Género debe contener máximo 50 carácteres'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> $request->name, 'error'=> $error]);
            }
            
            $teacher->name = $request->name; 
            $teacher->email = $request->email;
            $teacher->lastname = $request->lastname;
            $teacher->surname = $request->surname;
            $teacher->number_id = $request->number_id;
            $teacher->phone = $request->phone;
            $teacher->celphone = $request->celphone;
            $teacher->gender = $request->gender;
            $teacher->save();
            return response()->json(['success'=> true, 'message'=> 'El profesor se editó correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Profesor no encontrado'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/teacher/{id}",
     *      summary="Delete a teacher",
     *      description="Delete a teacher object",
     *      operationId="deleteTeacher",
     *      tags={"Teacher"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Teacher",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response()->json(['success'=> true, 'message'=> 'Se eliminó correctamente']);
    }

    /**
     * @OA\Post(
     *      path="/api/teacher/{id}/restore",
     *      summary="Restore a teacher",
     *      description="Restore a teacher object",
     *      operationId="restoreTeacher",
     *      tags={"Teacher"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of teacher",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $user = User::withTrashed()->find($id);
        if($user){
            $user->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Profesor no encontrado'], 404);
        }
    }
}
