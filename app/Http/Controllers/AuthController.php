<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\DataStudent;
use App\DataEmployee;
use App\DataExternal;
use App\Mail\RecoverPasswordEmail;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

use HttpOz\Roles\Models\Role;
use Illuminate\Support\Facades\Crypt;
use QRCode;
use Illuminate\Support\Facades\Storage;
use App\Mail\VerifyUserEmail;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * @OA\Post(
     *      path="/api/register",
     *      summary="register",
     *      description="Register a user into the system and create an QR code file",
     *      operationId="registerAuth",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string"),
     *              @OA\Property(property="email", type="string"),
     *              @OA\Property(property="password", type="string"),
     *              @OA\Property(property="lastname", type="string"),
     *              @OA\Property(property="surname", type="string"),
     *              @OA\Property(property="phone", type="string"),
     *              @OA\Property(property="celphone", type="string"),
     *              @OA\Property(property="student_type", type="string"),
     *              @OA\Property(property="gender", type="string"),
     *              @OA\Property(property="year", type="integer", description="Only if is student or exstudent"),
     *              @OA\Property(property="career_id", type="integer", description="Only if is student or exstudent"),
     *              @OA\Property(property="number_id", type="integer", description="Only if is student, exstudent or employee"),
     *              @OA\Property(property="campus_id", type="integer", description="Only if is student, exstudent or employee"),
     *              @OA\Property(property="semester", type="integer", description="Only if is student"),
     *              @OA\Property(property="system", type="enum:ESC,SUA", description="Only if is student"),
     *              @OA\Property(property="workshift", type="string", description="Only if is employee"),
     *              @OA\Property(property="area", type="string", description="Only if is employee"),
     *              @OA\Property(property="address", type="string", description="Only if is external"),
     *              @OA\Property(property="occupation_id", type="integer", description="Only if is external")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean",
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *              ),
     *          )
     *      )
     * )
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request){
        $rules = [
            'name' => 'required|max:255|alpha_space',
            'email' => 'required|email|confirmed|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'lastname' => 'required|alpha_space|max:255',
            'surname' => 'nullable|alpha_space|max:255',
            'phone' => 'nullable',
            'celphone' => 'required',
            'student_type' => 'required|alpha|enum:student,exstudent,employee,external',
            'gender' => 'required|max:50|alpha_space',
        ];
        $numAux = 0;
        $numeroStr = "";
        switch($request->student_type){
            case 'student': case 'exstudent':
                $numAux = 9;
                $rules['number_id'] = 'required|alpha_num|max:9|unique:users';
                $numeroStr = "cuenta";
                break;
            case 'employee':
                $numAux = 15;
                $rules['number_id'] = 'required|alpha_num|max:15|unique:users';
                $numeroStr = "trabajador";
                break;
        }
        $input = $request->only(
            'name',
            'email',
            'email_confirmation',
            'password',
            'password_confirmation',
            'lastname',
            'surname',
            'number_id',
            'phone',
            'celphone',
            'student_type',
            'gender'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.alpha_space' => 'Nombre debe contener solo carácteres alfabéticos',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'email.required' => 'Se require email',
            'email.email' => 'Introduce un email válido',
            'email.confirmed' => 'Los email\'s no coinciden',
            'email.max' => 'El email debe tener máximo 150 caracteres',
            'email.unique' => 'El email ya existe',
            'password.required' => 'Se require contraseña',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'password.min' => 'La contraseña debe contener mínimo 6 caracteres',
            'lastname.required' => 'Se require primer apellido',
            'lastname.alpha_space' => 'Primer Apellido debe contener solo carácteres alfabéticos',
            'surname.alpha_space' => 'Segundo Apellido debe contener solo carácteres alfabéticos',
            'surname.max' => 'Segundo Apellido debe contener máximo 255 carácteres',
            'student_type.required' => 'Se requiere tipo de usuario',
            'student_type.alpha' => 'Tipo de usuario debe contener caracteres alfabéticos',
            'student_type.enum' => 'Tipo de usuario no válido',
            'number_id.required' => "Se require el número de $numeroStr",
            'number_id.alpha_num' => "El número de $numeroStr debe contener solo carácteres alfanuméricos",
            'number_id.max' => "El número de $numeroStr Debe contener menos de $numAux carácteres",
            'number_id.unique' => "El número de $numeroStr ya existe",
            'gender.required' => 'Se require Género',
            'gender.alpha_space' => 'Género debe contener solo carácteres alfabéticos',
            'gender.max' => 'Género debe contener máximo 50 carácteres'
        ];
        $validator = Validator::make($input, $rules, $messages);

        switch($request->student_type){
            case 'student':
                $rulesStudent = [
                    'year' => 'required|integer|min:1900|max:' . Date("Y"),
                    'career_id' => 'integer',
                    'campus_id' => 'required|integer',
                    'semester' => 'required|integer',
                    'system' => 'required|alpha|enum:ESC,SUA'
                ];
                $inputStudent = $request->only(
                    'year',
                    'career_id',
                    'campus_id',
                    'semester',
                    'system'
                );
                $messagesStudent = [
                    'year.required' => 'Se require año de ingreso',
                    'year.integer' => 'Debe ser un año válido',
                    'year.min' => 'Debe ser un año válido',
                    'year.max' => 'Debe ser un año válido',
                    'career_id.integer' => 'Ingresa una carrera válida',
                    'campus_id.required' => 'Debes elegir un plantel',
                    'campus_id.integer' => 'Debes elegir un plantel válido',
                    'semester.required' => 'Debes ingresar un semestre válido',
                    'semester.integer' => 'Debes ingresar un semestre válido',
                    'system.required' => 'Se require tipo de sistema',
                    'system.alpha' => 'Debes ingresar un tipo de sistema válido',
                    'system.enum' => 'Debes ingresar un tipo de sistema válido'
                ];
                $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                break;
            case 'exstudent':
                $rulesStudent = [
                    'year' => 'required|integer|min:1900|max:' . Date("Y"),
                    'career_id' => 'integer',
                    'campus_id' => 'required|integer'
                ];
                $inputStudent = $request->only(
                    'year',
                    'career_id',
                    'campus_id'
                );
                $messagesStudent = [
                    'year.required' => 'Se require año de ingreso',
                    'year.integer' => 'Debe ser un año válido',
                    'year.min' => 'Debe ser un año válido',
                    'year.max' => 'Debe ser un año válido',
                    'career_id.integer' => 'Ingresa una carrera válida',
                    'campus_id.required' => 'Debes elegir un plantel',
                    'campus_id.integer' => 'Debes elegir un plantel válido'
                ];
                $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                break;
            case 'employee':
                $rulesStudent = [
                    'workshift' => 'required|alpha|enum:Matutino,Vespertino,Ambos',
                    'campus_id' => 'required|integer',
                    'area' => 'required|alpha_numeric_space'
                ];
                $inputStudent = $request->only(
                    'workshift',
                    'campus_id',
                    'area'
                );
                $messagesStudent = [
                    'workshift.required' => 'Se require turno',
                    'workshift.alpha' => 'Debes ingresar un turno válido',
                    'workshift.enum' => 'Debes ingresar un turno válido',
                    'campus_id.required' => 'Debes elegir un plantel',
                    'campus_id.integer' => 'Debes elegir un plantel válido',
                    'area.required' => 'Debes ingresar una área válida',
                    'area.alpha_numeric_space' => 'Debes ingresar una área válida',
                ];
                $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                break;
            case 'external':
                $rulesStudent = [
                    'address' => 'required|alpha_address|max:255',
                    'occupation_id' => 'required|integer'
                ];
                $inputStudent = $request->only(
                    'address',
                    'occupation_id'
                );
                $messagesStudent = [
                    'address.required' => 'Se requiere número de cuenta',
                    'address.alpha_address' => 'Solo se aceptan caracteres alfanuméricos',
                    'address.max' => 'Solo se aceptan caracteres alfanuméricos',
                    'occupation_id.required' => 'Debes elegir una ocupación válida',
                    'occupation_id.integer' => 'Debes elegir una acupación válida'
                ];
                $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                break;
            default:
                $validatorStudent = Validator::make($request->only('data'),['data' => 'required'], ['data.required' => 'Ingresa un tipo de usuario válido']);
                break;
        }
        if($validator->fails() || $validatorStudent->fails()) {
            $error = ($validator->fails()) ? $validator->messages() : $validatorStudent->messages();
            return response()->json(['message'=> $error], 400);
        }
        $name = $request->name;
        $email = $request->email;
        $user = User::create([
            'name' => $name, 
            'email' => $email, 
            'password' => Hash::make($request->password),
            'lastname' => $request->lastname,
            'surname' => $request->surname,
            'number_id' => $request->number_id,
            'phone' => $request->phone,
            'celphone' => $request->celphone,
            'gender' => $request->gender,
            'is_verified' => 0
        ]);

        switch($request->student_type){
            case 'student': case 'exstudent':
                DataStudent::Create([
                    'user_id' => $user->id,
                    'year' => $request->year,
                    'career_id' => $request->career_id,
                    'campus_id' => $request->campus_id,
                    'semester' => $request->semester
                ]);
                break;
            case 'employee':
                DataEmployee::Create([
                    'user_id' => $user->id,
                    'workshift' => $request->workshift,
                    'area' => $request->area,
                    'campus_id' => $request->campus_id
                ]);
                break;
            case 'external':
                DataExternal::Create([
                    'user_id' => $user->id,
                    'address' => $request->address,
                    'occupation_id' => $request->occupation_id
                ]);
        }
        $studentRole = Role::findBySlug($request->student_type);
        $user->attachRole($studentRole);
        $encrypt_code = Crypt::encrypt(json_encode([
            'type' => 'profile',
            'id' => $user->id
        ]));
        $qr = QRCode::text($encrypt_code);
        if(!Storage::exists("users/$user->id")) {
            Storage::makeDirectory("users/$user->id", 0775, true); //creates directory
        }
        $qr->setOutfile(storage_path() . "/app/users/$user->id/qr.svg")->svg();
        $verification_code = str_random(30); //Generate verification code
        DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);
        try {
            Mail::to($user)->send(new VerifyUserEmail($user->name, env('URL_WEB', '') . 'registro/verifica-usuario/' . $verification_code));
        } catch(\ErrorException $e){
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
        return response()->json(['success'=> true, 'message'=> 'Se envió un email para completar tu registro']);
    }

    /**
     * @OA\Get(
     *      path="/api/verify/{verification_code}",
     *      operationId="verifyCode",
     *      tags={"Auth"},
     *      summary="Verify an account email",
     *      @OA\Parameter(
     *          description="verification code sent by email",
     *          in="path",
     *          name="verification_code",
     *          required=true,
     *          example="1asdasd32asd3asd33",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a Line.",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean",
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *              ),
     *          )              
     *      )
     * )
     * API Verify User
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyUser($verification_code){
        $check = DB::table('user_verifications')->where('token',$verification_code)->first();
        if(!is_null($check)){
            $user = User::find($check->user_id);
            if($user->is_verified == 1){
                return response()->json([
                    'success'=> true,
                    'message'=> 'La cuenta ya ha sido verificada con anterioridad.'
                ]);
            }
            $user->update(['is_verified' => 1]);
            DB::table('user_verifications')->where('token',$verification_code)->delete();
            return response()->json([
                'success'=> true,
                'message'=> 'El email ha sido verificado correctamente.'
            ]);
        }
        return response()->json(['success'=> false, 'message'=> "El código de verificación no es válido."]);
    }

    /**
     * @OA\Post(
     *      path="/api/login",
     *      summary="login",
     *      description="Get a JWT (Json Web Token) to authenticate the requests",
     *      operationId="loginAuth",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="user", type="string"),
     *              @OA\Property(property="password", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="access_token",
     *                  type="string",
     *              ),
     *              @OA\Property(
     *                  property="userType",
     *                  type="object",
     *              ),
     *              @OA\Property(
     *                  property="expires_in",
     *                  type="integer",
     *                  description="In minutes"
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Returns when login is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="User or password not valid."),
     *          )
     *      )
     * )
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        $rules = [
            'user' => 'required',
            'password' => 'required',
        ];
        $input = $request->only('user', 'password');
        $messages = [
            'user.required' => 'Se require email',
            'password.required' => 'Se require contraseña'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $credentials = [
            'email' => $request->user,
            'password' => $request->password,
            'is_verified' => 1
        ];

        try {
            // attempt to verify the credentials and create a token for the user
            $token = auth()->attempt($credentials);
            if (!$token) {
                $credentials = [
                    'number_id' => $request->user,
                    'password' => $request->password,
                    'is_verified' => 1
                ];
                $token = auth()->attempt($credentials);
                if (!$token) {
                    return response()->json(['message' => 'Usuario y/o contraseña no válidos.'], 400);
                }
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['message' => 'could_not_create_token'], 500);
        }
        // all good so return the token
        $user = auth()->user();
        if(!Storage::exists("users/$user->id") && $user->role) {
            $encrypt_code = Crypt::encrypt(json_encode([
                'id' => $user->id,
                'number_id' => $user->number_id,
                'name' => $user->name,
                'lastname' => $user->lastname,
                'surname' => $user->surname,
                'user_type' => $user->role->slug
            ]));
            $qr = QRCode::text($encrypt_code);
            Storage::makeDirectory("users/$user->id", 0775, true); //creates directory
            $qr->setOutfile(storage_path() . "/app/users/$user->id/qr.svg")->svg();
        }
        return response()->json(['access_token' => $token, 'userType' => $user->roles , 'expires_in' => auth()->factory()->getTTL()]);
    }

    /**
     * @OA\Post(
     *      path="/api/logout",
     *      summary="logout",
     *      description="Invalidate a token to logout",
     *      operationId="logoutAuth",
     *      tags={"Auth"},
     *      security={ {"bearer": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="token sent to blacklist",
     *          @OA\JsonContent(
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean",
     *              )
     *          )
     *      )
     * )
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {
        auth()->logout();
        return response()->json(['success' => true]);
    }

    /**
     * @OA\Post(
     *      path="/api/recover",
     *      summary="recover",
     *      description="Send an email to recover the password",
     *      operationId="recoverAuth",
     *      tags={"Auth"},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="email", type="string", description="Email or number_id"),
     *              @OA\Property(property="number_id", type="string", description="Email or number_id"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="A reset email has been sent! Please check your email."),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when email not exist",
     *          @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean", example="false"),
     *              @OA\Property(property="error", type="string", example="Your email address was not found."),
     *          )
     *      )
     * )
     * API Recover Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recover(Request $request)
    {
        $user = User::where('email', $request->user)->first();
        if (!$user) {
            $user = User::where('number_id', $request->user)->first();
            if (!$user) {
                $error_message = "El usuario no fue encontrado.";
                return response()->json(['success' => false, 'message' => $error_message]);
            }
        }
        try {
            $token = Password::broker()->createToken($user);
            Mail::to($user)->send(new RecoverPasswordEmail($user->name, env('URL_WEB', '') . 'contra/cambia/' . $token));
        } catch(\ErrorException $e){
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        } catch (\Exception $e) {
            //Return with error
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 401);
        }
        return response()->json(['success' => true, 'message'=> 'Revisa tu email se ha mandado un link para reestablecer tu contraseña.']);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $rules = [
            'token' => 'required',
            'password' => 'required|confirmed|min:6',
        ];
        $input = $request->only(
            'token',
            'password',
            'password_confirmation'
        );
        $messages = [
            'token.required' => 'Se require token',
            'password.required' => 'Se require contraseña',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'password.min' => 'La contraseña debe contener mínimo 6 caracteres'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $response = Password::broker()->reset(
            Password::credentials($request), function ($user, $password) {
                Password::resetPassword($user, $password);
            }
        );
        if ($response == Password::PASSWORD_RESET) {
            return response()->json(['success' => true, 'message'=> 'Revisa tu email se ha mandado un link para reestablecer tu contraseña.']);
        } else {
            return response()->json(['success'=> false, 'message'=> 'No se pudo cambiar la contraseña']);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/register/check-email/{id?}",
     *      summary="checkEmail",
     *      description="Check if the email exist",
     *      operationId="checkEmail",
     *      tags={"Auth"},
     *      @OA\Parameter(
     *          description="id to ignore",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="email", type="string", description="Email")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean"),
     *              @OA\Property(property="message", type="string"),
     *          )
     *      )
     * )
     * API Check Email
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEmail(Request $request , $id = false){
        if ($id === false){
            $rules = [
                'email' => 'required|email|max:255|unique:users'
            ];
        } else {
            $rules = [
                'email' => ['required','email','max:255',Rule::unique('users')->ignore($id)]
            ];
        }
        $input = $request->only(
            'email'
        );
        $messages = [
            'email.required' => 'required',
            'email.email' => 'email',
            'email.max' => 'max',
            'email.unique' => 'unique'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        return response()->json(['success'=> true]);
    }
    /**
     * @OA\Post(
     *      path="/api/register/check-number-id/{id?}",
     *      summary="checkNumberId",
     *      description="Check if the number id exist",
     *      operationId="checkNumberId",
     *      tags={"Auth"},
     *      @OA\Parameter(
     *          description="id to ignore",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="number_id", type="string", description="Number or cta")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent(
     *              @OA\Property(property="success", type="boolean"),
     *              @OA\Property(property="message", type="string"),
     *          )
     *      )
     * )
     * API Check Number id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkNumberId(Request $request, $id = false){
        if($id === false) {
            $rules = [
                'number_id' => 'required|unique:users'
            ];
        } else {
            $rules = [
                'number_id' => ['required', Rule::unique('users')->ignore($id)]
            ];
        }
        $input = $request->only(
            'number_id'
        );
        $messages = [
            'number_id.required' => 'required',
            'number_id.unique' => 'unique'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        return response()->json(['success'=> true]);
    }
}
