<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Libraries\Dates;
use Validator;
use App\EventStudent;
use App\EventShowing;
use App\EventInscription;
use App\EventTicket;
use App\User;
use Illuminate\Support\Str as Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Crypt;
use QRCode;
use PDF;
use App\Libraries\FormatData;
use HttpOz\Roles\Models\Role;

class EventController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/event",
     *      operationId="getEventList",
     *      tags={"Events"},
     *      summary="Display a listing of the event",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Event.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Event")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:title,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be title,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'name');
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Event::withTrashed()->count();
            if($limit == -1){
                $events = Event::withTrashed()->with('event_category')->with('showings')->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            } else {
                $events = Event::withTrashed()->with('event_category')->with('showings')->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        } else {
            $totalRegister = Event::count();
            if ($limit == -1){
                $events = Event::with('event_category')->with('showings')->orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->get();
            } else {
                $events = Event::with('event_category')->with('showings')->orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        }
        return response()->json($events)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/event",
     *      summary="Store an event",
     *      description="Store an event object",
     *      operationId="storeEvent",
     *      tags={"Events"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Event")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:150|alpha_numeric_space|unique:events',
            'image' => 'required|max:255',
            'thumbnail' => 'required|max:255',
            'ticketimage' => 'nullable|max:255',
            'description' => 'required|max:65535',
            'inscription_start' => 'nullable|date',
            'inscription_finish' => 'nullable|date',
            'quota' => 'required|numeric',
            'init_folio' => 'nullable|numeric',
            'all_campus' => 'required|boolean',
            'classroom_id' => 'required|numeric|exists:classrooms,id',
            'event_category_id' => 'required|numeric|exists:event_categories,id',
            //'autopdf' => 'required',
            'students.*.id' => 'required|exists:roles,id',
            'students.*.pivot.limit' => 'numeric',
            'students.*.pivot.price' => 'numeric',
            'showings.*.start' => 'required|date',
            'showings.*.finish' => 'required|date',
            'campus.*.id' => 'numeric|exists:campus'
        ];
        $input = $request->only(
            'title',
            'image',
            'thumbnail',
            'ticketimage',
            'description',
            'quota',
            'init_folio',
            'classroom_id',
            'event_category_id',
            'inscription_start',
            'inscription_finish',
            'students',
            'showings',
            'campus',
            'all_campus',
            'autopdf'
        );
        $messages = [
            'title.required' => 'Se requiere título',
            'title.max' => 'Título debe contener máximo 150 carácteres',
            'title.alpha_numeric_space' => 'Título debe ser alfanumérico',
            'title.unique' => 'Título ya existe',
            'image.required' => 'Se requiere imágen',
            'image.max' => 'Imágen debe contener máximo 255 carácteres',
            'thumbnail.required' => 'Se requiere thumbnail',
            'thumbnail.max' => 'Thumbnail debe contener máximo 255 carácteres',
            'ticketimage.max' => 'Thumbnail debe contener máximo 255 carácteres',
            'description.required' => 'Se requiere Descripción',
            'description.max' => 'Descripción debe contener máximo 65535 carácteres',
            'inscription_start.date' => 'La fecha de inicio de inscripción no es válida',
            'inscription_finish.date' => 'La fecha de termino de inscripción no es válida',
            'autopdf.required' => 'Se requiere saber si se genera automáticamente el PDF',
            'quota.required' => 'Se requiere cupo',
            'quota.numeric' => 'Cupo debe ser numérico',
            'init_folio.numeric' => 'init_folio debe ser numérico',
            'all_campus.required' => 'Se requiere las facultades participantes',
            'all_campus.boolean' => 'Facultades participantes debe ser booleano',
            'classroom_id.required' => 'Se necesita un lugar',
            'classroom_id.numeric' => 'Sólo se permite números',
            'classroom_id.exists' => 'No existe ese lugar',
            'event_category_id.required' => 'Se necesita una categoría',
            'event_category_id.numeric' => 'Sólo se permite números',
            'event_category_id.exists' => 'No existe esa categoría',
            'students.*.id.required' => 'Se requiere un tipo de estudiante',
            'students.*.id.exists' => 'El tipo de estudiante no existe',
            'students.*.pivot.limit.numeric' => 'El límite debe ser numérico',
            'students.*.pivot.price.numeric' => 'El precio debe ser numérico',
            'showings.*.start.required' => 'Se requiere La fecha de inicio',
            'showings.*.start.date' => 'La fecha no es válida',
            'showings.*.finish.required' => 'Se requiere La fecha de inicio',
            'showings.*.finish.date' => 'La fecha no es válida',
            'campus.*.id.exists' => 'La facultad no existe'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        if($request->image && $request->image !== 'default'){
            Storage::disk('public')->put("img/events/$request->image", Storage::disk('public')->get("tmp/$request->image"));
        }
        if($request->thumbnail && $request->thumbnail !== 'default'){
            Storage::disk('public')->put("img/events/$request->thumbnail", Storage::disk('public')->get("tmp/$request->thumbnail"));
        }
        if($request->ticketimage && $request->ticketimage !== 'default'){
            Storage::disk('public')->put("img/events/$request->ticketimage", Storage::disk('public')->get("tmp/$request->ticketimage"));
        }
        $event = Event::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title),
            'image' => $request->image,
            'thumbnail' => $request->thumbnail,
            'description' => $request->description,
            'all_campus' => $request->all_campus,
            'inscription_start' => $request->inscription_start ? Dates::changeFormat($request->inscription_start) : null,
            'inscription_finish' => $request->inscription_finish ? Dates::changeFormat($request->inscription_finish) : null,
            'quota' => $request->quota,
            'init_folio' => $request->init_folio ? $request->init_folio : 0,
            'classroom_id' => $request->classroom_id,
            'event_category_id' => $request->event_category_id,
            'autopdf' => $request->autopdf == 1 || $request->autopdf == true ? $request->autopdf : 0
        ]);
        $students = [];
        foreach($request->students as $student) {
            $students[$student['id']] = [
                'limit' => $student['pivot']['limit'],
                'price' => $student['pivot']['price']
            ];
        }
        $event->students()->attach($students);
        foreach($request->showings as $showing) {
            EventShowing::create([
                'event_id' => $event->id,
                'start' => Dates::changeFormat($showing['start']),
                'finish' => Dates::changeFormat($showing['finish'])
            ]);
        }
        if (!$event->all_campus) {
            $campus = [];
            foreach($request->campus as $camp) {
                $campus[] = $camp['id'];
            }
            $event->campus()->attach($campus);
        }
        return response()->json(['message'=> 'Evento registrado', 'event' => ['id' => $event->id, 'slug' => $event->slug]]);
    }

    /**
     * @OA\Get(
     *      path="/api/event/{id}",
     *      operationId="getEvent",
     *      tags={"Events"},
     *      summary="Display an event",
     *      @OA\Parameter(
     *          description="ID of event",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show an event.",
     *          @OA\JsonContent(ref="#/components/schemas/Event")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $event = Event::with('showings')->with('campus')->with('event_category')->withTrashed()->find($id);
        } else {
            $event = Event::with('showings')->with('campus')->with('event_category')->find($id);
        }
        if($event){
            return response()->json($event);
        }else{
            return response()->json(['success' => false, 'message' => 'Evento no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/event/{id}",
     *      summary="Update an event",
     *      description="Update an event object",
     *      operationId="updateEvent",
     *      tags={"Events"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Event",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Event")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::withTrashed()->find($id);
        if($event){
            $rules = [
                'title' => ['required','alpha_numeric_space','max:150',Rule::unique('events')->ignore($event->id)],
                'image' => 'required|max:255',
                'thumbnail' => 'required|max:255',
                'ticketimage' => 'nullable|max:255',
                'description' => 'required|max:65535',
                'all_campus' => 'required|boolean',
                'inscription_start' => 'nullable|date',
                'inscription_finish' => 'nullable|date',
                'quota' => 'required|numeric',
                'init_folio' => 'nullable|numeric',
                'classroom_id' => 'required|numeric|exists:classrooms,id',
                'event_category_id' => 'required|numeric|exists:event_categories,id',
                //'autopdf' => 'required',
                'students.*.id' => 'required|exists:roles',
                'students.*.pivot.limit' => 'numeric',
                'students.*.pivot.price' => 'numeric',
                'showings.*.id' => 'nullable|numeric|exists:event_showings',
                'showings.*.start' => 'required|date',
                'showings.*.finish' => 'required|date',
                'campus.*.id' => 'required_if:all_campus,true|numeric|exists:campus'
            ];
            $input = $request->only(
                'title',
                'image',
                'thumbnail',
                'description',
                'quota',
                'classroom_id',
                'event_category_id',
                'inscription_start',
                'inscription_finish',
                'students',
                'showings',
                'all_campus',
                'campus',
                'ticketimage',
                'init_folio',
                'autopdf'
            );
            $messages = [
                'title.required' => 'Se requiere título',
                'title.max' => 'Título debe contener máximo 150 carácteres',
                'title.alpha_numeric_space' => 'Título debe ser alfanumérico',
                'title.unique' => 'Título ya existe',
                'image.required' => 'Se requiere imágen',
                'image.max' => 'Imágen debe contener máximo 255 carácteres',
                'thumbnail.required' => 'Se requiere thumbnail',
                'thumbnail.max' => 'Thumbnail debe contener máximo 255 carácteres',
                'ticketimage.max' => 'ticketimage debe contener máximo 255 carácteres',
                'description.required' => 'Se requiere Descripción',
                'description.max' => 'Descripción debe contener máximo 65535 carácteres',
                'inscription_start.required' => 'Se require fecha de inicio de inscripción',
                'inscription_start.date_format' => 'La fecha de inicio de inscripción no es válida',
                'inscription_finish.required' => 'Se require fecha de termino de inscripción',
                'inscription_finish.date_format' => 'La fecha de termino de inscripción no es válida',
                'quota.required' => 'Se requiere cupo',
                'quota.numeric' => 'Cupo debe ser numérico',
                'init_folio.numeric' => 'init_folio debe ser numérico',
                'all_campus.required' => 'Se requiere las facultades participantes',
                'all_campus.boolean' => 'Facultades participantes debe ser booleano',
                'classroom_id.required' => 'Se necesita un lugar',
                'classroom_id.numeric' => 'Sólo se permite números',
                'classroom_id.exists' => 'No existe ese lugar',
                'autopdf.required' => 'Se requiere saber si se genera automáticamente el PDF',
                'event_category_id.required' => 'Se necesita una categoría',
                'event_category_id.numeric' => 'Sólo se permite números',
                'event_category_id.exists' => 'No existe esa categoría',
                'students.*.id.required' => 'Se requiere un tipo de estudiante',
                'students.*.id.exists' => 'El tipo de estudiante no existe',
                'students.*.pivot.limit.numeric' => 'El límite debe ser numérico',
                'students.*.pivot.price.numeric' => 'El precio debe ser numérico',
                'showings.*.id.numeric' => 'Id debe ser numérico',
                'showings.*.id.exists' => 'El horario no existe',
                'showings.*.start.required' => 'Se requiere La fecha de inicio',
                'showings.*.start.date' => 'La fecha no es válida',
                'showings.*.finish.required' => 'Se requiere La fecha de inicio',
                'showings.*.finish.date' => 'La fecha no es válida',
                'campus.*.id.exists' => 'La facultad no existe',
                'campus.*.id.required_if' => 'Se necesita ingresar una facultad'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['error'=> $error], 400);
            }
            if($request->image && $event->image !== $request->image){
                Storage::disk('public')->delete('img/events/' . $event->image);
                Storage::disk('public')->put("img/events/$request->image", Storage::disk('public')->get("tmp/$request->image"));
                $event->image = $request->image;
            }
            if($request->thumbnail && $event->thumbnail !== $request->thumbnail){
                Storage::disk('public')->delete('img/events/' . $event->thumbnail);
                Storage::disk('public')->put("img/events/$request->thumbnail", Storage::disk('public')->get("tmp/$request->thumbnail"));
                $event->thumbnail = $request->thumbnail;
            }
            if($request->ticketimage && $event->ticketimage !== $request->ticketimage){
                Storage::disk('public')->delete('img/events/' . $event->ticketimage);
                Storage::disk('public')->put("img/events/$request->ticketimage", Storage::disk('public')->get("tmp/$request->ticketimage"));
                $event->ticketimage = $request->ticketimage;
            }
            $event->title = $request->title;
            $event->slug = Str::slug($request->title);
            $event->description = $request->description;
            $event->all_campus = $request->all_campus;
            $event->inscription_start = $request->inscription_start ? Dates::changeFormat($request->inscription_start) : null;
            $event->inscription_finish = $request->inscription_finish ? Dates::changeFormat($request->inscription_finish) : null;
            $event->quota = $request->quota;
            $event->init_folio = $request->init_folio ? $request->init_folio : 0;
            $event->classroom_id = $request->classroom_id;
            $event->event_category_id = $request->event_category_id;
            $event->autopdf = $request->autopdf == 1 || $request->autopdf == true ? $request->autopdf : 0;
            $event->save();

            // Sync Showings
            $idsShowings = [];
            foreach ($request->showings as $showing) {
                if ($showing['id']) {
                    $showingDB = EventShowing::find($showing['id']);
                    $showingDB->start = Dates::changeFormat($showing['start']);
                    $showingDB->finish = Dates::changeFormat($showing['finish']);
                    $showingDB->save();
                } else {
                    $showingDB = EventShowing::create([
                        'event_id' => $event->id,
                        'start' => Dates::changeFormat($showing['start']),
                        'finish' => Dates::changeFormat($showing['finish'])
                    ]);
                }
                $idsShowings[] = $showingDB->id;
            }
            EventShowing::where('event_id', $event->id)->whereNotIn('id', $idsShowings)->delete();

            //sync roles
            $studentSync = [];
            foreach($request->students as $student) {
                $studentSync[$student['id']] = [
                    'limit' => $student['pivot']['limit'],
                    'price' => $student['pivot']['price']
                ];
            }
            $event->students()->sync($studentSync);
            //sync campus
            if (!$event->all_campus) {
                $campus = [];
                foreach($request->campus as $camp) {
                    $campus[] = $camp['id'];
                }
                $event->campus()->sync($campus);
            } else if(count($event->campus) > 0){
                $event->campus()->detach();
            }
            return response()->json(['success'=> true, 'message'=> 'Evento actualizado']);
        }else{
            return response()->json(['success' => false, 'message' => 'Evento no encontrado'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/event/{id}",
     *      summary="Delete an event",
     *      description="Delete an event object",
     *      operationId="deleteEvent",
     *      tags={"Events"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Event",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $event = Event::withTrashed()->find($id);
        if ($event) {
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'force must be true or false, 1 or 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $event->delete();
            } else {
                $event->forceDelete();
            }
            return response()->json(['message'=> 'Evento eliminada']);
        } else {
            return response()->json(['message'=> 'Evento no encontrada'], 404);
        }
    }
    /**
     * @OA\Post(
     *      path="/api/event/{id}/restore",
     *      summary="Restore an event",
     *      description="Restore an event object",
     *      operationId="restoreEvent",
     *      tags={"Events"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of event",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $event = Event::withTrashed()->find($id);
        if($event){
            $event->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/event/by-slug/{slug}",
     *      operationId="getEventBySlug",
     *      tags={"Events"},
     *      summary="Display an event by slug",
     *      @OA\Parameter(
     *          description="slug of event",
     *          in="path",
     *          name="slug",
     *          required=true,
     *          example="",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show an event.",
     *          @OA\JsonContent(ref="#/components/schemas/Event")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource by slug.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function bySlug($slug)
    {
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $event = Event::where('slug' , $slug)->with('showings')->with('students')->with('classroom')->with('campus')->with('event_category')->withTrashed()->first();
        } else {
            $event = Event::where('slug' , $slug)->with('showings')->with('students')->with('classroom')->with('campus')->with('event_category')->first();
        }
        if($event){
            $showings = [];
            $user = auth()->user();
            $eventArray = $event->toArray();
            $eventArray['inscribed'] = false;
            if($user) {
                $eventArray['has_user'] = true;
            }
            foreach($event->showings as $key => $showing) {
                if ($event->deleted_at == null){
                    $showings[$key] = $this->getShowing($showing , $user);
                    if ($showings[$key]['inscribed']) {
                        $eventArray['inscribed'] = true;
                    }
                } else {
                    $showings[$key] = $showing->toArray();
                    $showings[$key]['can'] = false;
                    $showings[$key]['cant_message'] = 'Ya no puedes inscribirte a las funciones';
                    $showings[$key]['inscribed'] = false;
                    $showings[$key]['students_inscribed_count'] = 0;
                }
            }
            if ($eventArray['inscribed'] && $eventArray['one_more_time'] == 0) {
                foreach ($showings as $key => $wp) {
                    if(!$showings[$key]['inscribed']) {
                        $showings[$key]['can'] = false;
                        $showings[$key]['cant_message'] = 'Ya no puedes inscribirte a más funciones';
                    }
                }
            }
            $eventArray['showings'] = $showings;
            return response()->json($eventArray);
        }else{
            return response()->json(['message' => 'Evento no encontrado'], 404);
        }
    }
    
    /**
     * @OA\Post(
     *      path="/api/event/inscribe",
     *      summary="Restore an event",
     *      description="Inscribe an user with login to event",
     *      operationId="restoreEvent",
     *      tags={"Events"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="event_showing_id", type="integer", example="1")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function inscribe(Request $request)
    {
        $rules = [
            'event_showing_id' => 'required|exists:event_showings,id',
        ];
        $input = $request->only(
            'event_showing_id'
        );
        $messages = [
            'event_showing_id.required' => 'Se requiere Evento',
            'event_showing_id.exists' => 'No existe el Evento'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['message'=> $validator->messages(), 400]);
        }
        $user = auth()->user();
        if($user) {
            $showing = EventShowing::find($request->event_showing_id);
            if($showing){
                $inscriptions = $user->event_inscriptions;
                $canInsc = true;
                $message = '';
                foreach($inscriptions as $insc) {
                    if ($showing->id == $insc->event_showing_id) {
                        if($insc->status === 0) {
                            $canInsc = false;
                            $message = "Ya asistirás al Evento";
                        } else if($insc->status === 1) {
                            $canInsc = false;
                            $message = "Ya se validó tu entrada";
                        }
                    } else if($insc->event_showing->event && $insc->event_showing->event->id == $showing->event->id && $showing->event->one_more_time == 0 ){
                        $canInsc = false;
                        $message = "Sólo puedes asistir a una función";
                    }

                }
                if ($canInsc) {
                    $folio = $showing->event->init_folio + count($showing->students_inscribed);
                    $showing = $this->getShowing($showing, $user);
                    if($showing['can']) {
                        $insc = EventInscription::create([
                            'event_showing_id' => $showing['id'],
                            'user_id' => $user->id,
                            'folio' => $folio,
                            'quota' => $showing['student_price']
                        ]);
                        return response()->json(['message'=> 'Inscribed']);
                    } else {
                        return response()->json(['message' => $showing['cant_message']], 400);
                    }
                } else {
                    return response()->json(['message' => $message], 400);
                }
            }else{
                return response()->json(['message' => 'Evento no encontrado'], 404);
            }
        } else {
            return response()->json(['message' => 'Debes hacer login'], 400);
        }
    }

    /**
     * Inscribe to events
     */
    private function getShowing($showing , $user = false)
    {
        $showingArray = $showing->toArray();
        $showingArray['students_inscribed_count'] = 0;
        $showingArray['inscribed'] = false;
        foreach ($showing->students_inscribed as $student) {
            if ($student->pivot->status == 0 || $student->pivot->status == 1) {
                $showingArray['students_inscribed_count']++;
            }
        }
        if ($user) {
            $can = false;
            $role_user = '';
            $limit = -1;
            foreach($showing->event->students as $student) {
                foreach($user->roles as $role) {
                    $role_user = $role->slug;
                    if($role->slug === $student->slug) {
                        $can = true;
                        $limit = $student->pivot->limit;
                        $showingArray['student_price'] = $student->pivot->price;
                    }
                }
            }
            if ($can) {
                if($showing->event->all_campus === 0 && ($user->isRole('student') || $user->isRole('exstudent') || $user->isRole('employee') ||  $user->isRole('teacher'))) {
                    if($user->isRole('student') || $user->isRole('exstudent')){
                        $campus = $user->dataStudent[0]->campus;
                    } else if($user->isRole('employee') ||  $user->isRole('teacher')) {
                        $campus = $user->dataEmployee[0]->campus;
                    }
                    if($campus){
                        $can = false;
                        foreach($showing->event->campus as $c){
                            if($c->id === $campus->id) {
                                $can = true;
                            }
                        }
                        if ($can === false) {
                            $showingArray['cant_message'] = 'Tu plantel no puede asistir al evento';
                        }
                    } else {
                        $can = false;
                        $showingArray['cant_message'] = 'No se asigno algún plantel a tu usuario';
                    }
                }
                if ($can) {
                    if($showingArray['students_inscribed_count'] >= $showing->event->quota) {
                        $can = false;
                        $showingArray['cant_message'] = 'Cupo lleno';
                    } else {
                        $count_role_user = 0;
                        foreach($showing->students_inscribed as $student) {
                            if($student->id === $user->id){
                                if($student->pivot->status == 0 || $student->pivot->status == 1) {
                                    $showingArray['inscribed'] = true;
                                    $showingArray['insc_id'] = $student->pivot->id;
                                    $can = false;
                                    $showingArray['cant_message'] = 'Ya estás inscrito a éste evento';
                                }
                            }
                            if($student->pivot->status == 0 || $student->pivot->status == 1) {
                                $has_role = false;
                                foreach($student->roles as $role) {
                                    if($role->slug == $role_user) {
                                        $has_role = true;
                                    }
                                }
                                if($has_role) {
                                    $count_role_user++;
                                }
                            }
                        }
                        if($can){
                            if ($limit !== -1 && $count_role_user >= $limit) {
                                $can = false;
                                $showingArray['cant_message'] = 'Ya se cubrieron los lugares asignados a tu tipo de alumno';
                            }
                        }
                    }
                }
            } else {
                $showingArray['cant_message'] = 'Tu tipo de alumno no puede asistir a éste evento.';
                $showingArray['role_user'] = $role_user;
            }
            $showingArray['can'] = $can;
        }
        return $showingArray;
    }



    /**
     * @OA\Get(
     *      path="/api/event/offering-by-slug/{slug}",
     *      operationId="getEventBySlug",
     *      tags={"Events"},
     *      summary="Display an event by slug",
     *      @OA\Parameter(
     *          description="slug of event",
     *          in="path",
     *          name="slug",
     *          required=true,
     *          example="",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show an event.",
     *          @OA\JsonContent(ref="#/components/schemas/Event")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource offering by slug.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function offeringBySlug($slug)
    {
        $event = Event::where('slug' , $slug)->where('event_category_id', 2)->first();
        if($event){
            $event->offerings;
            return response()->json($event);
        }else{
            return response()->json(['message' => 'event not found'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/event/check-name/{id?}",
     *      summary="checkNameEvent",
     *      description="Check if the name exist",
     *      operationId="checkNameEvent",
     *      tags={"Events"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Event",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * API Check Name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkName(Request $request, $id = false){
        if ($id === false) {
            $rules = [
                'title' => 'required|alpha_numeric_space|max:150|unique:events',
            ];
        } else {
            $rules = [
                'title' => ['required', 'alpha_numeric_space', 'max:150', Rule::unique('events')->ignore($id)],
            ];
        }
        $input = $request->only(
            'title'
        );
        $messages = [
            'title.required' => 'required',
            'title.unique' => 'unique',
            'title.alpha_numeric_space' => 'alpha_numeric_space'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['message'=> $validator->messages()], 400);
        }
        return response()->json(['message'=> "Don't exist"]);
    }

    function getInscribePdf(Request $request, $id) {
        $user = auth()->user();
        $inscription = EventInscription::where('id', '=', $id)->where('user_id', '=', $user->id)->first();
        if($inscription && $inscription->user->id == $user->id) {
            if ($inscription->event_showing->event->autopdf){
                $role = false;
                foreach($inscription->user->roles as $r) {
                    $role = $r;
                }
                $inscription->event_showing->event;
                $data = ['inscription' => $inscription , 'role' => $role, 'token' => $request->query('token')];
                $pdf = PDF::loadView('pdf.ticket', $data);
                return $pdf->download('ticket.pdf');
            } else if ($inscription->event_ticket_id !== null){
                return Storage::disk("event_tickets")->download($inscription->event_ticket->file);
            } else{
                $pdf = EventTicket::where("event_id", "=", $inscription->event_showing->event->id)->where("event_inscription_id", '=', null)->first();
                if ($pdf){
                    $pdf->event_inscription_id = $id;
                    $pdf->save();
                    $inscription->event_ticket_id = $pdf->id;
                    $inscription->save();
                    return Storage::disk("event_tickets")->download($pdf->file);
                } else {
                    return response()->json(['success' => false, 'message' => 'Ya no hay disponibles cortesias'], 404);
                }
            }
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }

    function getInscribePdfAdmin(Request $request, $id) {
        $user = auth()->user();
        $event_showing = EventShowing::find($id);
        $role = false;
        foreach($user->roles as $r) {
            $role = $r;
        }
        $event_showing->event;
        $data = ['event_showing' => $event_showing , 'role' => $role, 'user' => $user, 'token' => $request->query('token')];
        $pdf = PDF::loadView('pdf.ticketAdmin', $data);
        return $pdf->download('ticket.pdf');
    }

    function verify(Request $request,$event_showing_id, $code) {
        $event_showing = EventShowing::find($event_showing_id);
        if($event_showing){
            if($event_showing->event->event_category_id == 1){
                if(strlen($code) > 13) {
                    $message = json_decode(Crypt::decrypt($code));
                    $user = User::find($message->id);
                } else {
                    $user = User::where('number_id', '=', substr($code,0,9))->first();
                }
                if($user) {
                    $inscription = EventInscription::where('user_id', '=', $user->id)->where('event_showing_id', '=', $event_showing_id)->first();
                } else {
                    $inscription = null;
                }
                if($inscription && $event_showing && $event_showing->id == $inscription->event_showing->id) {
                    if($inscription->status == 0) {
                        $inscription->status = 1;
                        $inscription->save();
                        $inscription->user->roles;
                        return response()->json(['success' => true, 'inscription' => $inscription]);
                    } else {
                        return response()->json(['success' => false, 'message' => 'Ya se validó anteriormente']);    
                    }
                } else {
                    return response()->json(['success' => false, 'message' => 'Inscripción o alumno no existe']);
                }
            } else if($event_showing->event->event_category_id == 3){
                $user = User::where('number_id', '=', substr($code,0,9))->first();
                if($user) {
                    $inscription = EventInscription::where('user_id', '=', $user->id)->where('event_showing_id', '=', $event_showing_id)->first();
                    if(!$inscription) {
                        $folio = $event_showing->event->init_folio + count($event_showing->students_inscribed);
                        $inscription = EventInscription::create([
                            'event_showing_id' => $event_showing_id,
                            'user_id' => $user->id,
                            'folio' => $folio,
                            'quota' => 0,
                            'status' => 1
                        ]);
                        $inscription->user->roles;
                        return response()->json(['success' => true, 'inscription' => $inscription]);
                    } else {
                        return response()->json(['success' => false, 'message' => 'Ya se validó anteriormente']);    
                    }
                } else {
                    return response()->json(['success' => false, 'message' => 'Alumno no existe']);
                }
            }
        } else {
            return response()->json(['success' => false, 'message' => 'El evento no es válido']);
        }
    }

    function getCodes(Request $request) {
        $codes = [];
        $studentRole = Role::findBySlug('student');
        $students = $studentRole->users;
        foreach($students as $student) {
            if ($student->number_id) {
                $codes[] = $student->number_id;
            }
        }
        $studentRole = Role::findBySlug('exstudent');
        $students = $studentRole->users;
        foreach($students as $student) {
            if ($student->number_id) {
                $codes[] = $student->number_id;
            }
        }
        return response()->json($codes);
    }

    /**
     * @OA\Post(
     *      path="/api/event/{id}/add-showing",
     *      summary="Add showing event",
     *      description="add a showing to event",
     *      operationId="addShowingEvent",
     *      tags={"Events"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of event",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(
     *                  @OA\Property(property="id", type="integer", example="1"),
     *                  @OA\Property(property="start", type="string", example="1",format="date-time"),
     *                  @OA\Property(property="finish", type="string", example="1",format="date-time")
     *              )
     *              
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Add showing the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addShowing(Request $request, $id)
    {
        $event = Event::find($id);
        if($event){
            $rules = [
                'showings.*.id' => 'nullable|numeric|exists:event_showings',
                'showings.*.start' => 'required|date',
                'showings.*.finish' => 'required|date',
            ];
            $input = $request->only(
                'showings'
            );
            $messages = [
                'showings.*.id.numeric' => 'Id debe ser numérico',
                'showings.*.id.exists' => 'El horario no existe',
                'showings.*.start.required' => 'Se requiere La fecha de inicio',
                'showings.*.start.date' => 'La fecha no es válida',
                'showings.*.finish.required' => 'Se requiere La fecha de inicio',
                'showings.*.finish.date' => 'La fecha no es válida'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }

            foreach($request->showings as $showing) {
                EventShowing::create([
                    'event_id' => $event->id,
                    'start' => Dates::changeFormat($showing['start']),
                    'finish' => Dates::changeFormat($showing['finish'])
                ]);
            }
            return response()->json(['success'=> true, 'message'=> 'Evento actualizado']);
        }else{
            return response()->json(['success' => false, 'message' => 'Evento no encontrado'], 404);
        }
    }
    /**
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function syncInscriptions(Request $request, $id) {
        if(isset($request->showings) && is_array($request->showings)){
            foreach($request->showings as $showing){
                $event_showing = EventShowing::find($showing["id"]);
                if($event_showing){
                    foreach($showing["inscriptions"] as $code){
                        $user = User::where('number_id', '=', $code)->first();
                        if($user) {
                            $inscription = EventInscription::where('user_id', '=', $user->id)->where('event_showing_id', '=', $event_showing->id)->first();
                            if(!$inscription) {
                                $folio = $event_showing->event->init_folio + count($event_showing->students_inscribed);
                                $inscription = EventInscription::create([
                                    'event_showing_id' => $event_showing->id,
                                    'user_id' => $user->id,
                                    'folio' => $folio,
                                    'quota' => 0,
                                    'status' => 1
                                ]);
                            }
                        }
                    }
                }
            }
        }
        return response()->json(['success' => true]);
    }*/

    /**
     * @OA\Get(
     *      path="/api/event/{id}/tickets",
     *      operationId="getEventTicketsList",
     *      tags={"EventEventTickets"},
     *      summary="Display a listing of the event",
     *      @OA\Parameter(
     *          description="ID of event",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show EventTicket.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/EventTicket")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tickets(Request $request, $id)
    {
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $event = Event::with('showings')->with('campus')->with('event_category')->withTrashed()->find($id);
        } else {
            $event = Event::with('showings')->with('campus')->with('event_category')->find($id);
        }
        if($event){
            $rules = [
                'limit' => 'nullable|integer',
                'page' => 'nullable|integer',
                'orderAsc' => 'nullable|boolean_get',
                'orderBy' => 'nullable|in:title,created_at,updated_at',
            ];
            $input = $request->only(
                'limit',
                'page',
                'orderAsc',
                'orderBy'
            );
            $messages = [
                'limit.integer' => 'limit must be integer',
                'page.integer' => 'page must be integer',
                'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
                'orderBy.in' => 'orderBy must be title,created_at or updated_at',
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $limit = $request->query('limit', -1);
            $page = $request->query('page', 1);
            $orderAsc = $request->query('orderAsc', true);
            $orderBy = $request->query('orderBy', 'file');
            $user = auth()->user();
            $totalRegister = EventTicket::count();
            if ($limit == -1){
                $event_tickets = EventTicket::where('event_id', '=', $event->id)->orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->get();
            } else {
                $event_tickets = EventTicket::where('event_id', '=', $event->id)->orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
            return response()->json($event_tickets)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
        }else{
            return response()->json(['success' => false, 'message' => 'Evento no encontrado'], 404);
        }
    }
}
