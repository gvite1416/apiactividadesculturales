<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventCategory;
use App\Event;
use Validator;
use Illuminate\Validation\Rule;
class EventCategoryController extends Controller
{
    /**
     * 
     * @OA\Get(
     *      path="/api/event-category",
     *      operationId="getEventCategoryList",
     *      tags={"EventCategories"},
     *      summary="Display a listing of the eventCategory",
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show EventCategory.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/EventCategory")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'name');
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = EventCategory::withTrashed()->count();
            if($limit == -1){
                $eventCategories = EventCategory::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            } else {
                $eventCategories = EventCategory::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        } else {
            $totalRegister = EventCategory::count();
            if ($limit == -1){
                $eventCategories = EventCategory::orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->get();
            } else {
                $eventCategories = EventCategory::orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        }
        return response()->json($eventCategories)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * 
     * @OA\Get(
     *      path="/api/event-category/{id}/events",
     *      operationId="getEventsByCategoryList",
     *      tags={"EventCategories"},
     *      summary="Display a listing of the eventCategory",
     *      @OA\Parameter(
     *          description="ID of eventCategory",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="title",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show EventCategory.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/EventCategory")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEventsByCategory(Request $request, $id)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:title,created_at,updated_at]',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be title,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'title');
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Event::where("event_category_id" , $id)->withTrashed()->count();
            if($limit == -1){
                $eventCategories = Event::with('showings')->with('event_category')->where("event_category_id" , $id)->withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            } else {
                $eventCategories = Event::with('showings')->with('event_category')->where("event_category_id" , $id)->withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        } else {
            $totalRegister = Event::where("event_category_id" , $id)->count();
            if ($limit == -1){
                $eventCategories = Event::with('showings')->with('event_category')->where("event_category_id" , $id)->orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->get();
            } else {
                $eventCategories = Event::with('showings')->with('event_category')->where("event_category_id" , $id)->orderBy($orderBy, $orderAsc == 'true' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
            }
        }
        return response()->json($eventCategories)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/event-category",
     *      summary="Store an eventCategory",
     *      description="Store an eventCategory object",
     *      operationId="storeEventCategory",
     *      tags={"EventCategories"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/EventCategory")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|regex:/^[a-zA-Z0-9 -\.]+$/i|unique:event_categories'
        ];
        $input = $request->only(
            'name',
            'code'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.regex' => 'Nombre debe contener solo carácteres alfanuméricos y los simbolos . -',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'name.unique' => 'Nombre ya existe',
            'code.alpha_num' => 'El código debe ser alfanumérico',
            'code.max' => 'Nombre debe contener máximo 3 carácteres',
            'code.unique' => 'Código ya existe'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        EventCategory::create([
            'name' => $request->name,
            'code' => $request->code,
            'college_degree_option' => $request->college_degree_option == 1 || $request->college_degree_option == true ? $request->college_degree_option : 0
        ]);
        return response()->json(['message'=> 'Carrera registrada']);
    }

    /**
     * @OA\Get(
     *      path="/api/event-category/{id}",
     *      operationId="getEventCategory",
     *      tags={"EventCategories"},
     *      summary="Display an eventCategory",
     *      @OA\Parameter(
     *          description="ID of eventCategory",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show an eventCategory.",
     *          @OA\JsonContent(ref="#/components/schemas/EventCategory")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $eventCategory = EventCategory::withTrashed()->find($id);
        } else {
            $eventCategory = EventCategory::find($id);
        }
        if($eventCategory){
            return response()->json($eventCategory);
        }else{
            return response()->json(['success' => false, 'message' => 'Carrera no encontrada'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/event-category/{id}",
     *      summary="Update an eventCategory",
     *      description="Update an eventCategory object",
     *      operationId="updateEventCategory",
     *      tags={"EventCategories"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of EventCategory",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/EventCategory")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $eventCategory = EventCategory::withTrashed()->find($id);
        if($eventCategory){
            $rules = [
                'name' => ['required','max:255','regex:/^[a-zA-Z0-9 -\.]+$/i',Rule::unique('event_categories')->ignore($eventCategory->id)],
                'code' => 'nullable|max:3|alpha_num'
            ];
            $input = $request->only(
                'name',
                'code'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.regex' => 'Nombre debe contener solo carácteres alfanuméricos y los simbolos . -',
                'name.max' => 'Nombre debe contener máximo 255 carácteres',
                'code.alpha_num' => 'El código debe ser alfanumérico',
                'code.max' => 'Nombre debe contener máximo 3 carácteres'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error]);
            }
            $eventCategory->name = $request->name;
            $eventCategory->code = $request->code;
            $eventCategory->college_degree_option = $request->college_degree_option == 1 || $request->college_degree_option == true ? $request->college_degree_option : 0;
            $eventCategory->save();
            return response()->json(['message'=> 'Carrera actualizada']);
        }else{
            return response()->json(['message' => 'Carrera no encontrada'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/event-category/{id}",
     *      summary="Delete an eventCategory",
     *      description="Delete an eventCategory object",
     *      operationId="deleteEventCategory",
     *      tags={"EventCategories"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of EventCategory",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $eventCategory = EventCategory::withTrashed()->find($id);
        if ($eventCategory) {
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'force must be true or false, 1 or 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $eventCategory->delete();
            } else {
                $eventCategory->forceDelete();
            }
            return response()->json(['message'=> 'Carrera eliminada']);
        } else {
            return response()->json(['message'=> 'Carrera no encontrada'], 404);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/event-category/{id}/restore",
     *      summary="Restore an eventCategory",
     *      description="Restore an eventCategory object",
     *      operationId="restoreEventCategory",
     *      tags={"EventCategories"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of event category",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $eventCategory = EventCategory::withTrashed()->find($id);
        if($eventCategory){
            $eventCategory->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/event-category/check-name/{id?}",
     *      summary="checkNameEventCategory",
     *      description="Check if the name exist",
     *      operationId="checkNameEventCategory",
     *      tags={"EventCategories"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of EventCategory",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * API Check Name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkName(Request $request, $id = false){
        if ($id === false) {
            $rules = [
                'name' => 'required|regex:/^[a-zA-Z0-9 -\.]+$/i|max:150|unique:event_categories',
            ];
        } else {
            $rules = [
                'name' => ['required', 'regex:/^[a-zA-Z0-9 -\.]+$/i', 'max:150', Rule::unique('event_categories')->ignore($id)],
            ];
        }
        $input = $request->only(
            'name'
        );
        $messages = [
            'name.required' => 'required',
            'name.unique' => 'unique',
            'name.regex' => 'Nombre debe contener solo carácteres alfanuméricos  y los simbolos . -'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['message'=> $validator->messages()], 400);
        }
        return response()->json(['message'=> "Don't exist"]);
    }
}
