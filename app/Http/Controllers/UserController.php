<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Period;
use App\Imports\ImportUsers;
use Validator;
use DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use HttpOz\Roles\Models\Role;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Crypt;
use QRCode;

use App\Career;
use App\DataEmployee;
use App\DataStudent;
use App\DataExternal;
use App\Inscription;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use PDF;
use Excel;

class UserController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/users",
     *      operationId="getUserList",
     *      tags={"User"},
     *      summary="Display a listing of the users",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show User.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/User")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'created_at');
        $totalRegister = User::withTrashed()->count();
            if($limit == -1)
                $users = User::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $users = User::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        return response()->json($users)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/users",
     *      summary="Store a user",
     *      description="Store a user object",
     *      operationId="storeUser",
     *      tags={"User"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|alpha_space',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'lastname' => 'required|alpha_space|max:255',
            'surname' => 'nullable|alpha_space|max:255',
            'phone' => 'nullable',
            'celphone' => 'nullable',
            'gender' => 'required|max:50|alpha_space',
            'user_type' => 'required|alpha|enum:admin,socialservice,talent'
        ];
        $input = $request->only(
            'name',
            'email',
            'password',
            'password_confirmation',
            'lastname',
            'surname',
            'phone',
            'celphone',
            'user_type',
            'gender'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.alpha_space' => 'Nombre debe contener solo carácteres alfabéticos',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'email.required' => 'Se require email',
            'email.email' => 'Introduce un email válido',
            'email.max' => 'El email debe tener máximo 150 caracteres',
            'email.unique' => 'El email ya existe',
            'password.required' => 'Se require contraseña',
            'password.confirmed' => 'Las contraseñas no coinciden',
            'password.min' => 'La contraseña debe contener mínimo 6 caracteres',
            'lastname.required' => 'Se require primer apellido',
            'lastname.alpha_space' => 'Primer Apellido debe contener solo carácteres alfabéticos',
            'surname.alpha_space' => 'Segundo Apellido debe contener solo carácteres alfabéticos',
            'surname.max' => 'Segundo Apellido debe contener máximo 255 carácteres',
            'user_type.required' => 'Se requiere tipo de usuario',
            'user_type.alpha' => 'Tipo de usuario debe contener caracteres alfabéticos',
            'user_type.enum' => 'Tipo de usuario no válido',
            'gender.required' => 'Se require Género',
            'gender.alpha_space' => 'Género debe contener solo carácteres alfabéticos',
            'gender.max' => 'Género debe contener máximo 50 carácteres'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $name = $request->name;
        $email = $request->email;
        $user = User::create([
            'name' => $name, 
            'email' => $email, 
            'password' => Hash::make($request->password),
            'lastname' => $request->lastname,
            'surname' => $request->surname,
            'phone' => $request->phone,
            'celphone' => $request->celphone,
            'gender' => $request->gender,
            'is_verified' => 1,
            'is_verified_dept' => 1
        ]);
        $student_type = Role::findBySlug($request->user_type);
        $user->attachRole($student_type);
        $this->generateQrUser($user);
        /*$verification_code = str_random(30); //Generate verification code
        DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);

        try {
            Mail::to($user)->send(new VerifyUserEmail($user->name, env('URL_WEB', '') . 'registro/verifica-usuario/' . $verification_code));
        } catch(\ErrorException $e){
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }*/
        return response()->json(['success'=> true, 'message'=> 'Usuario registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/users/{id}",
     *      operationId="getUser",
     *      tags={"User"},
     *      summary="Display a user",
     *      @OA\Parameter(
     *          description="ID of user",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a user.",
     *          @OA\JsonContent(ref="#/components/schemas/User")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $user->roles;
        if(count($user->dataStudent) > 0){
            $user->dataStudent[0]->career;
            $user->dataStudent[0]->campus;
        }
        if(count($user->dataEmployee) > 0){
            $user->dataEmployee[0]->campus;
        }
        $user->dataExternal;
        return response()->json($user);
    }
    /**
     * @OA\Put(
     *      path="/api/users/{id}",
     *      summary="Update a user",
     *      description="Update a user object",
     *      operationId="updateUser",
     *      tags={"User"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of User",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/User")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::withTrashed()->find($id);
        if($user){
            $rules = [
                'name' => 'required|max:255|alpha_space',
                'email' => ['required','email','max:255',Rule::unique('users')->ignore($user->id)],
                'lastname' => 'required|alpha_space|max:255',
                'surname' => 'nullable|alpha_space|max:255',
                'phone' => 'nullable',
                'celphone' => 'nullable',
                'student_type' => 'required|alpha|exists:roles,slug',
                'gender' => 'required|max:50|alpha_space',
            ];
            $numAux = 0;
            $numeroStr = "";
            switch($request->student_type){
                case 'student': case 'exstudent':
                    $numAux = 9;
                    $rules['number_id'] = ['required','alpha_num','max:9', Rule::unique('users')->ignore($user->id)];
                    $numeroStr = "cuenta";
                    break;
                case 'employee':
                    $numAux = 15;
                    $rules['number_id'] = ['required','alpha_num','max:15', Rule::unique('users')->ignore($user->id)];
                    $numeroStr = "trabajador";
                    break;
            }
            $input = $request->only(
                'name',
                'email',
                'lastname',
                'surname',
                'number_id',
                'phone',
                'celphone',
                'student_type',
                'gender'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.alpha_space' => 'Nombre debe contener solo carácteres alfabéticos',
                'name.max' => 'Nombre debe contener máximo 255 carácteres',
                'email.required' => 'Se require email',
                'email.email' => 'Introduce un email válido',
                'email.max' => 'El email debe tener máximo 150 caracteres',
                'email.unique' => 'El email ya existe',
                'password.required' => 'Se require contraseña',
                'password.confirmed' => 'Las contraseñas no coinciden',
                'password.min' => 'La contraseña debe contener mínimo 6 caracteres',
                'lastname.required' => 'Se require primer apellido',
                'lastname.alpha_space' => 'Primer Apellido debe contener solo carácteres alfabéticos',
                'surname.alpha_space' => 'Segundo Apellido debe contener solo carácteres alfabéticos',
                'surname.max' => 'Segundo Apellido debe contener máximo 255 carácteres',
                'student_type.required' => 'Se requiere tipo de usuario',
                'student_type.alpha' => 'Tipo de usuario debe contener caracteres alfabéticos',
                'student_type.exists' => 'Tipo de usuario no existe',
                'number_id.required' => "Se require el número de $numeroStr",
                'number_id.alpha_num' => "El número de $numeroStr debe contener solo carácteres alfanuméricos",
                'number_id.max' => "El número de $numeroStr Debe contener menos de $numAux carácteres",
                'number_id.unique' => "El número de $numeroStr ya existe",
                'gender.required' => 'Se require Género',
                'gender.alpha_space' => 'Género debe contener solo carácteres alfabéticos',
                'gender.max' => 'Género debe contener máximo 50 carácteres'
            ];
            $validator = Validator::make($input, $rules, $messages);

            switch($request->student_type){
                case 'student': case 'exstudent':
                    $rulesStudent = [
                        'data_student.year' => 'required|integer|min:1900|max:' . Date("Y"),
                        'data_student.career_id' => 'integer|exists:careers,id',
                        'data_student.campus_id' => 'required|integer|exists:campus,id',
                        'data_student.semester' => 'required|integer',
                        'data_student.system' => 'required|alpha|enum:ESC,SUA'
                    ];
                    $inputStudent = $request->only(
                        'data_student.year',
                        'data_student.career_id',
                        'data_student.campus_id',
                        'data_student.semester',
                        'data_student.system'
                    );
                    $messagesStudent = [
                        'data_student.year.required' => 'Se require año de ingreso',
                        'data_student.year.integer' => 'Debe ser un año válido',
                        'data_student.year.min' => 'Debe ser un año válido',
                        'data_student.year.max' => 'Debe ser un año válido',
                        'data_student.career_id.integer' => 'Ingresa una carrera válida',
                        'data_student.career_id.exists' => 'La carrera no existe',
                        'data_student.campus_id.required' => 'Debes elegir un plantel',
                        'data_student.campus_id.integer' => 'Debes elegir un plantel válido',
                        'data_student.campus_id.exists' => 'El plantel no existe',
                        'data_student.semester.required' => 'Debes ingresar un semestre válido',
                        'data_student.semester.integer' => 'Debes ingresar un semestre válido',
                        'data_student.system.required' => 'Se require tipo de sistema',
                        'data_student.system.alpha' => 'Debes ingresar un tipo de sistema válido',
                        'data_student.system.enum' => 'Debes ingresar un tipo de sistema válido'
                    ];
                    $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                    break;
                case 'employee':
                    $rulesStudent = [
                        'data_employee.workshift' => 'required|alpha|enum:Matutino,Vespertino,Ambos',
                        'data_employee.campus_id' => 'required|integer|exists:campus,id',
                        'data_employee.area' => 'required|alpha_num'
                    ];
                    $inputStudent = $request->only(
                        'data_employee.workshift',
                        'data_employee.campus_id',
                        'data_employee.area'
                    );
                    $messagesStudent = [
                        'data_employee.workshift.required' => 'Se require turno',
                        'data_employee.workshift.alpha' => 'Debes ingresar un turno válido',
                        'data_employee.workshift.enum' => 'Debes ingresar un turno válido',
                        'data_employee.campus_id.required' => 'Debes elegir un plantel',
                        'data_employee.campus_id.integer' => 'Debes elegir un plantel válido',
                        'data_employee.campus_id.exists' => 'El plantel no existe',
                        'data_employee.area.required' => 'Debes ingresar una área válida',
                        'data_employee.area.alpha_num' => 'Debes ingresar una área válida',
                    ];
                    $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                    break;
                case 'external':
                    $rulesStudent = [
                        'data_external.address' => 'required|alpha_address|max:255',
                        'data_external.occupation_id' => 'required|integer|exists:occupations,id'
                    ];
                    $inputStudent = $request->only(
                        'data_external.address',
                        'data_external.occupation_id'
                    );
                    $messagesStudent = [
                        'data_external.address.required' => 'Se requiere número de cuenta',
                        'data_external.address.alpha_address' => 'Solo se aceptan caracteres alfanuméricos',
                        'data_external.address.max' => 'Solo se aceptan caracteres alfanuméricos',
                        'data_external.occupation_id.required' => 'Debes elegir una ocupación válida',
                        'data_external.occupation_id.integer' => 'Debes elegir una acupación válida',
                        'data_external.occupation_id.exist' => 'La ocupación no existe'
                    ];
                    $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                    break;
                default:
                    $validatorStudent = Validator::make($request->only('student_type'),['student_type' => 'required'], ['student_type.required' => 'Ingresa tipo de usuario']);
                    break;
            }
            if($validator->fails() || $validatorStudent->fails()) {
                $error = ($validator->fails()) ? $validator->messages() : $validatorStudent->messages();
                return response()->json(['success'=> $request->name, 'error'=> $error]);
            }
            $name = $request->name;
            $email = $request->email;
            
            $user->name = $name; 
            $user->email = $email;
            $user->lastname = $request->lastname;
            $user->surname = $request->surname;
            $user->number_id = $request->number_id;
            $user->phone = $request->phone;
            $user->celphone = $request->celphone;
            $user->gender = $request->gender;
            $user->save();

            switch($request->student_type){
                case 'student': case 'exstudent':
                    if(count($user->dataStudent) > 0){
                        $user->dataStudent[0]->year = $request->data_student['year'];
                        $user->dataStudent[0]->career_id = $request->data_student['career_id'];
                        $user->dataStudent[0]->campus_id = $request->data_student['campus_id'];
                        $user->dataStudent[0]->semester = $request->data_student['semester'];
                        $user->dataStudent[0]->system = $request->data_student['system'];
                        $user->dataStudent[0]->save();
                    }else{
                        DataStudent::Create([
                            'user_id' => $user->id,
                            'year' => $request->data_student['year'],
                            'career_id' => $request->data_student['career_id'],
                            'campus_id' => $request->data_student['campus_id'],
                            'semester' => $request->data_student['semester'],
                            'system' => $request->data_student['system']
                        ]);
                    }
                    break;
                case 'employee':
                    if(count($user->dataEmployee) > 0){
                        $user->dataEmployee[0]->workshift = $request->data_employee['workshift'];
                        $user->dataEmployee[0]->area = $request->data_employee['area'];
                        $user->dataEmployee[0]->campus_id = $request->data_employee['campus_id'];
                        $user->dataEmployee[0]->save();
                    }else{
                        DataEmployee::Create([
                            'user_id' => $user->id,
                            'workshift' => $request->data_employee['workshift'],
                            'area' => $request->data_employee['area'],
                            'campus_id' => $request->data_employee['campus_id']
                        ]);
                    }
                    break;
                case 'external':
                    if(count($user->dataExternal) > 0){
                        $user->dataExternal[0]->address = $request->data_external['address'];
                        $user->dataExternal[0]->occupation_id = $request->data_external['occupation_id'];
                        $user->dataExternal[0]->save();
                    }else{
                        DataExternal::Create([
                            'address' => $request->data_external['address'],
                            'occupation_id' => $request->data_external['occupation_id']
                        ]);
                    }
            }
            if(!$user->isRole($request->student_type)){
                $user->detachAllRoles();
                $studentRole = Role::findBySlug($request->student_type);
                $user->attachRole($studentRole);
            }
            return response()->json(['success'=> true, 'message'=> 'El usuario se editó correctamente']);
        } else {
            return response()->json(['success'=> false, 'message'=> 'Usuario no encontrado'],404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/users/{id}",
     *      summary="Delete a user",
     *      description="Delete a user object",
     *      operationId="deleteUser",
     *      tags={"User"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of User",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response()->json(['success' => true, 'message' => 'El usuario se eliminó correctamente']);
    }

    /**
     * @OA\Post(
     *      path="/api/users/{id}/restore",
     *      summary="Restore a user",
     *      description="Restore a user object",
     *      operationId="restoreUser",
     *      tags={"User"},
     *      security={ {"bearer": {} }},
    *       @OA\Parameter(
     *          description="ID of User",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $user = User::withTrashed()->find($id);
        if($user){
            $user->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Get(
     *      path="/api/student/types",
     *      operationId="getStudentTypes",
     *      tags={"User"},
     *      summary="Display a list of student types",
     *      @OA\Response(
     *          response=200,
     *          description="Show a User.",
     *          @OA\JsonContent(ref="#/components/schemas/Role") 
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get Types Student
     * 
     * @return \Illuminate\http\Response
     */
    public function studentTypes(){
        $students = Role::where('group', 'student')->get();
        return response()->json($students);
    }
    /**
     * @OA\Get(
     *      path="/api/student/by-id/{id}",
     *      operationId="getStudentType",
     *      tags={"User"},
     *      summary="Display a student types",
     *      @OA\Parameter(
     *          description="ID of Role",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a User.",
     *          @OA\JsonContent(ref="#/components/schemas/Role") 
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get Types Student
     * 
     * @return \Illuminate\http\Response
     */
    public function studentById($id){
        $student = Role::where('group', 'student')->where('id',$id)->limit(1)->first();
        return response()->json($student);
    }

    /**
     * @OA\Get(
     *      path="/api/users/search",
     *      operationId="searchUserList",
     *      tags={"User"},
     *      summary="Display a listing of the users",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Name to search",
     *          in="query",
     *          name="name",
     *          required=false,
     *          example="",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Email to search",
     *          in="query",
     *          name="email",
     *          required=false,
     *          example="",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Number cta to search",
     *          in="query",
     *          name="number_id",
     *          required=false,
     *          example="",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show User.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/User")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $rules = [
            'name' => 'nullable|max:255|alpha_space',
            'email' => 'nullable|email|max:255',
            'number_id' => 'nullable|alpha_num'
        ];
        $input = $request->only(
            'name',
            'email',
            'number_id'
        );
        $messages = [
            'name.alpha_space' => 'Nombre debe contener solo carácteres alfabéticos',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'email.email' => 'Introduce un email válido',
            'email.max' => 'El email debe tener máximo 150 caracteres',
            'number_id.alpha_num' => "El número debe contener solo carácteres alfanuméricos"
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $limit = $request->query('limit', 10);
        $page = $request->query('page', 1);
        $query =  DB::table('users')->select('id')->orderBy("name")->skip($limit * ($page - 1))->take($limit)->orderBy("lastname")->orderBy("surname")->whereNull('deleted_at');
        $bSearch = false;
        if($request->name) {
            $names = explode(" ", $request->name);
            foreach($names as $key => $nam){
                $query->where(function ($q) use ($nam) {
                    $q->where('name','like', '%' . $nam . '%')
                    ->orWhere('lastname','like', '%' . $nam . '%')
                    ->orWhere('surname','like', '%' . $nam . '%');
                });
            }
            $bSearch = true;
        }
        if($request->email) {
            $query->where('email', 'like', '%' . $request->email . '%');
            $bSearch = true;
        }
        if($request->number_id) {
            $query->where('number_id', 'like', '%' . $request->number_id . '%');
            $bSearch = true;
        }
        
        $usersAux = $query->get();
        $users = [];
        foreach($usersAux as $user) {
            $user = User::find($user->id);
            $user->roles;
            $users[] = $user;
        }
        $totalRegister = $query->skip(0)->count();
        return response()->json($users)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function active(Request $request)
    {   
        
        $user = auth()->user();
        if ($user){
            $user->roles;
            // dd($user);
            if(count($user->dataStudent) > 0){
                $user->dataStudent[0]->career;
                $user->dataStudent[0]->campus;
            }
            if(count($user->dataEmployee) > 0){
                $user->dataEmployee[0]->campus;
            }
            $user->dataExternal;
            return response()->json($user);
        } else {
            return response()->json($user, 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $user = auth()->user();
        if($user){
            $rules = [
                'name' => 'required|max:255|alpha_space',
                'email' => ['required','email','max:255',Rule::unique('users')->ignore($user->id)],
                'lastname' => 'required|alpha_space|max:255',
                'surname' => 'nullable|alpha_space|max:255',
                'phone' => 'nullable',
                'celphone' => 'nullable',
                'gender' => 'required|max:50|alpha_space',
            ];
            $numAux = 0;
            $numeroStr = "";
            $student_type = '';
            foreach($user->roles as $role){
                $student_type = $role->slug;
            }
            switch($student_type){
                case 'employee':
                    $numAux = 15;
                    $rules['number_id'] = ['required','alpha_num','max:15', Rule::unique('users')->ignore($user->id)];
                    $numeroStr = "trabajador";
                    break;
            }
            $input = $request->only(
                'name',
                'email',
                'lastname',
                'surname',
                'number_id',
                'phone',
                'celphone',
                'gender'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.alpha_space' => 'Nombre debe contener solo carácteres alfabéticos',
                'name.max' => 'Nombre debe contener máximo 255 carácteres',
                'email.required' => 'Se require email',
                'email.email' => 'Introduce un email válido',
                'email.max' => 'El email debe tener máximo 150 caracteres',
                'email.unique' => 'El email ya existe',
                'password.required' => 'Se require contraseña',
                'password.confirmed' => 'Las contraseñas no coinciden',
                'password.min' => 'La contraseña debe contener mínimo 6 caracteres',
                'lastname.required' => 'Se require primer apellido',
                'lastname.alpha_space' => 'Primer Apellido debe contener solo carácteres alfabéticos',
                'surname.alpha_space' => 'Segundo Apellido debe contener solo carácteres alfabéticos',
                'surname.max' => 'Segundo Apellido debe contener máximo 255 carácteres',
                'number_id.required' => "Se require el número de $numeroStr",
                'number_id.alpha_num' => "El número de $numeroStr debe contener solo carácteres alfanuméricos",
                'number_id.max' => "El número de $numeroStr Debe contener menos de $numAux carácteres",
                'number_id.unique' => "El número de $numeroStr ya existe",
                'gender.required' => 'Se require Género',
                'gender.alpha_space' => 'Género debe contener solo carácteres alfabéticos',
                'gender.max' => 'Género debe contener máximo 50 carácteres'
            ];
            $validator = Validator::make($input, $rules, $messages);

            switch($student_type){
                case 'employee':
                    $rulesStudent = [
                        'data_employee.workshift' => 'required|alpha|enum:Matutino,Vespertino,Ambos',
                        'data_employee.campus_id' => 'required|integer|exists:campus,id',
                        'data_employee.area' => 'required|alpha_num'
                    ];
                    $inputStudent = $request->only(
                        'data_employee.workshift',
                        'data_employee.campus_id',
                        'data_employee.area'
                    );
                    $messagesStudent = [
                        'data_employee.workshift.required' => 'Se require turno',
                        'data_employee.workshift.alpha' => 'Debes ingresar un turno válido',
                        'data_employee.workshift.enum' => 'Debes ingresar un turno válido',
                        'data_employee.campus_id.required' => 'Debes elegir un plantel',
                        'data_employee.campus_id.integer' => 'Debes elegir un plantel válido',
                        'data_employee.campus_id.exists' => 'El plantel no existe',
                        'data_employee.area.required' => 'Debes ingresar una área válida',
                        'data_employee.area.alpha_num' => 'Debes ingresar una área válida',
                    ];
                    $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                    break;
                case 'external':
                    $rulesStudent = [
                        'data_external.address' => 'required|alpha_address|max:255',
                        'data_external.occupation_id' => 'required|integer|exists:occupations,id'
                    ];
                    $inputStudent = $request->only(
                        'data_external.address',
                        'data_external.occupation_id'
                    );
                    $messagesStudent = [
                        'data_external.address.required' => 'Se requiere número de cuenta',
                        'data_external.address.alpha_address' => 'Solo se aceptan caracteres alfanuméricos',
                        'data_external.address.max' => 'Solo se aceptan caracteres alfanuméricos',
                        'data_external.occupation_id.required' => 'Debes elegir una ocupación válida',
                        'data_external.occupation_id.integer' => 'Debes elegir una acupación válida',
                        'data_external.occupation_id.exist' => 'La ocupación no existe'
                    ];
                    $validatorStudent = Validator::make($inputStudent, $rulesStudent, $messagesStudent);
                    break;
                default:
                    $validatorStudent = Validator::make([], [], []);
                    break;
            }
            if($validator->fails() || $validatorStudent->fails()) {
                $error = ($validator->fails()) ? $validator->messages() : $validatorStudent->messages();
                return response()->json(['success'=> $request->name, 'error'=> $error]);
            }
            $name = $request->name;
            $email = $request->email;
            
            $user->name = $name; 
            $user->email = $email;
            $user->lastname = $request->lastname;
            $user->surname = $request->surname;
            $user->number_id = $request->number_id;
            $user->phone = ($request->phone && $request->phone != 0) ? $request->phone : NULL;
            $user->celphone = $request->celphone;
            $user->gender = $request->gender;
            $user->save();

            switch($student_type){
                case 'employee':
                    if(count($user->dataEmployee) > 0){
                        $user->dataEmployee[0]->workshift = $request->data_employee['workshift'];
                        $user->dataEmployee[0]->area = $request->data_employee['area'];
                        $user->dataEmployee[0]->campus_id = $request->data_employee['campus_id'];
                        $user->dataEmployee[0]->save();
                    }
                    break;
                case 'external':
                    if(count($user->dataExternal) > 0){
                        $user->dataExternal[0]->address = $request->data_external['address'];
                        $user->dataExternal[0]->occupation_id = $request->data_external['occupation_id'];
                        $user->dataExternal[0]->save();
                    }
            }
            return response()->json(['success'=> true, 'message'=> 'El usuario se editó correctamente']);
        } else {
            return response()->json(['success'=> false, 'message'=> 'Usuario no encontrado'],404);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfilePass(Request $request)
    {
        $user = auth()->user();
        if($user){
            $rules = [
                'password' => 'required|confirmed|min:6',
                'password_current' => 'required|min:6',
            ];
            $input = $request->only(
                'password',
                'password_confirmation',
                'password_current'
            );
            $messages = [
                'password.required' => 'Se require contraseña',
                'password.confirmed' => 'Las contraseñas no coinciden',
                'password.min' => 'La contraseña debe contener mínimo 6 caracteres',
                'password_current.required' => 'Se require contraseña actual',
                'password_current.min' => 'La contraseña actual debe contener mínimo 6 caracteres'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            if(auth()->validate([
                'email' => $user->email,
                'password' => $request->password_current
            ])){
                $user->password = Hash::make($request->password);
                $user->save();
                return response()->json(['success'=> true, 'message'=> 'La contraseña se cambió correctamente']);
            } else {
                return response()->json(['success'=> false, 'message'=> 'La contraseña actual no es válida']);
            }
        } else {
            return response()->json(['success'=> false, 'message'=> 'Usuario no encontrado'],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProfilePhoto(Request $request) {
        $user = auth()->user();
        if($user){
            $rules = [
                'photo' => 'required',
            ];
            $input = $request->only(
                'photo'
            );
            $messages = [
                'photo.required' => 'Se require imagen'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            $base64_str = substr($request->photo, strpos($request->photo, ",")+1);
            $image = base64_decode($base64_str);
            $imgname = time();
            if($user->photo) {
                Storage::disk('users')->delete($user->photo_url);
            }
            Storage::disk('users')->put("$user->id/$imgname.jpg", $image);
            $user->photo = $imgname . '.jpg';
            $user->save();
            return response()->json(['success'=> true, 'message'=> 'La foto se actualizó correctamente' , 'path' => $user->photo_url]);
        } else {
            return response()->json(['success'=> false, 'message'=> 'Usuario no encontrado'],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateQrPhoto(Request $request) {
        $user = auth()->user();
        if($user){
            $this->generateQrUser($user);
            return response()->json(['success'=> true, 'message'=> 'El código QR se actualizó correctamente']);
        }else {
            return response()->json(['success'=> false, 'message'=> 'Usuario no encontrado'],404);
        }
    }

    /**
     * get Id Card the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function idCard(Request $request, $id = '') {
        $period = Period::current()->first();
        if ($period) {
            $user = auth()->user();
            $inscription = Inscription::where('id', '=', $id)->where('user_id', '=', $user->id)->first();
            if($inscription && $inscription->user->id == $user->id && $inscription->workshop_period->period->id == $period->id) {
                $this->generateQrUser($user);
                $data = ['user' => $user, 'period' => $period, 'inscription' => $inscription, 'token' => $request->query('token')];
                $pdf = PDF::loadView('pdf.idCard', $data)->setPaper('a4', 'landscape');
                return $pdf->download('id_card.pdf');
            }else {
                return response()->json(['success'=> false, 'message'=> 'No tienes inscripciones'],404);
            }
        } else {
            return response()->json(['success'=> false, 'message'=> 'Aún no hay un periodo activo'],404);
        }
        
    }
    /**
     * get Id Card the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function idCardAdmin(Request $request, $user_id, $id = '') {
        $period = Period::current()->first();
        if ($period) {
            $user = User::find($user_id);
            $inscription = Inscription::where('id', '=', $id)->where('user_id', '=', $user_id)->first();
            if($inscription && $inscription->user->id == $user_id && $inscription->workshop_period->period->id == $period->id) {
                $this->generateQrUser($user);
                $data = ['user' => $user, 'period' => $period, 'inscription' => $inscription, 'token' => $request->query('token')];
                $pdf = PDF::loadView('pdf.idCard', $data)->setPaper('a4', 'landscape');
                return $pdf->download('id_card.pdf');
            }else {
                return response()->json(['success'=> false, 'message'=> 'No tienes inscripciones'],404);
            }
        } else {
            return response()->json(['success'=> false, 'message'=> 'Aún no hay un periodo activo'],404);
        }
        
    }
    /**
     * change user's password from admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request, $id) {
        $user = User::find($id);
        if ($user) {
            $rules = [
                'password' => 'required|confirmed|min:6',
            ];
            $input = $request->only(
                'password',
                'password_confirmation'
            );
            $messages = [
                'password.required' => 'Se require contraseña',
                'password.confirmed' => 'Las contraseñas no coinciden',
                'password.min' => 'La contraseña debe contener mínimo 6 caracteres'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                return response()->json(['success'=> false, 'error'=> $validator->messages()]);
            }
            $user->password = Hash::make($request->password);
            $user->save();
            return response()->json(['success'=> true, 'message'=> 'La contraseña se cambió correctamente']);
        } else {
            return response()->json(['success'=> false, 'message'=> 'No se encuentra al usuario'],404);
        }
        
    }
    /**
     * verify user's from admin
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request, $id) {
        $user = User::find($id);
        if ($user) {
            $user->is_verified = 1;
            $user->is_verified_dept = 1;
            $user->save();
            return response()->json(['success'=> true, 'message'=> 'El usuario se validó correctamente']);
        } else {
            return response()->json(['success'=> false, 'message'=> 'No se encuentra al usuario'],404);
        }
        
    }

    /**
     * expor users from excel
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function exportExcel(Request $request) {
        $rules = [
            'name' => 'required',
            'limit' => 'required',
            'page' => 'required',
        ];
        $input = $request->only(
            'name',
            'limit',
            'page'
        );
        $messages = [
            'name.required' => 'Se require nombre de archivo',
            'limit.required' => 'Se require límite',
            'page.required' => 'Se require página'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        $jsonString = file_get_contents(storage_path('app/tmp/' . $request->name));
        $array = json_decode($jsonString, true);
        $rows = [];
        $newUsers = 0;
        $updateUsers = 0;
        
        if(count($array) > 0){
            for($i = ($request->page - 1)*$request->limit ; $i < $request->page*$request->limit; $i++){
                $row = $array[$i];
                $user = User::where('number_id', $row[3])->limit(1)->first();
                $career = Career::where('code', $row[1])->limit(1)->first();
                if($career) {
                    $userEmail = User::where('email', $row[7])->limit(1)->first();
                    if($user && $userEmail && $user->id != $userEmail->id) {
                        $userEmail->email .= '_reasignado';
                        $userEmail->save();
                    }
                    if(!$user) {
                        $user = $userEmail;
                    }
                    if($user){
                        if (count($user->dataStudent) > 0) {
                            $user->dataStudent[0]->system = $row[0];
                            $user->dataStudent[0]->career_id = $career->id;
                            $user->dataStudent[0]->year = $row[2];
                        } else {
                            DataStudent::Create([
                                'user_id' => $user->id,
                                'year' => $row[2],
                                'career_id' => $career->id,
                                'campus_id' => 1,
                                'semester' => 0
                            ]);
                        }
                        $user->email = $row[7] ? $row[7] : time() . "@cambiarporfavor.com.mx";
                        $user->number_id = $row[3];
                        $user->save();
                        $updateUsers++;
                    } else {
                        $user = User::create([
                            'name' => $row[6], 
                            'email' => $row[7] ? $row[7] : time() . "@cambiarporfavor.com.mx", 
                            'password' => Hash::make($row[3]),
                            'lastname' => $row[4],
                            'surname' => $row[5],
                            'number_id' => $row[3],
                            'gender' => "Especificar",
                            'is_verified' => 1,
                            'is_verified_dept' => 1
                        ]);
                        DataStudent::Create([
                            'user_id' => $user->id,
                            'year' => $row[2],
                            'career_id' => $career->id,
                            'campus_id' => 1,
                            'semester' => 0
                        ]);
                        $studentRole = Role::findBySlug('student');
                        $user->attachRole($studentRole);
                        $newUsers++;
                    }
                }
            }
        }
        return response()->json(['success'=> true, 'new'=> $newUsers , 'update' => $updateUsers]);
    }

    /**
     * valid a token
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validToken(Request $request) {
        $user = auth()->user();
        $user->roles;
        return response()->json(['success'=> true, 'user' => $user]);
    }

    private function generateQrUser($user) {
        if(!Storage::disk("users")->exists("$user->id")){
            Storage::disk("users")->makeDirectory("$user->id", 0775, true); //creates directory
        }
        if(!Storage::disk("users")->exists("$user->id/qr.svg")) {
            $encrypt_code = Crypt::encrypt(json_encode([
                'id' => $user->id,
                'number_id' => $user->number_id,
                'name' => $user->name,
                'lastname' => $user->lastname,
                'surname' => $user->surname,
                'user_type' => (isset($user->roles[0])) ? $user->roles[0]->slug : 'Indefinido'
            ]));
            $qr = QRCode::text($encrypt_code);
            $qr->setOutfile(storage_path() . "/app/users/$user->id/qr.svg")->svg();
        }
    }
}