<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Validator;
use Illuminate\Support\Facades\Storage;
class SliderController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/slider",
     *      operationId="getSliderList",
     *      tags={"Slider"},
     *      summary="Display a listing of the sliders",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Slider.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Slider")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:title,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be title,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'created_at');

        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Slider::withTrashed()->count();
            if($limit == -1)
                $sliders = Slider::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $sliders = Slider::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        } else {
            $totalRegister = Slider::count();
            if($limit == -1)
                $sliders = Slider::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $sliders = Slider::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }
        return response()->json($sliders)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/slider",
     *      summary="Store an slider",
     *      description="Store an slider object",
     *      operationId="storeSlider",
     *      tags={"Slider"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Slider")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'nullable|max:255|alpha_numeric_space',
            'img_desktop' => 'required|max:255',
            'img_mobile' => 'required|max:255',
            'order' => 'required|integer',
            'link' => 'nullable|max:255',
        ];
        $input = $request->only(
            'title',
            'img_desktop',
            'img_mobile',
            'link',
            'order'
        );
        $messages = [
            'title.alpha_numeric_space' => 'Título debe contener solo carácteres alfanuméricos',
            'title.max' => 'Título debe contener máximo 255 carácteres',
            'img_desktop.max' => 'Imágen debe contener máximo 255 carácteres',
            'img_desktop.required' => 'Imágen de escritorio requerida',
            'img_mobile.max' => 'Nombre debe contener máximo 255 carácteres',
            'img_mobile.required' => 'Imágen de movil requerida',
            'link.max' => 'Nombre debe contener máximo 255 carácteres',
            'order.integer' => 'Orden debe ser entero',
            'order.required' => 'Orden requerido',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        if($request->img_desktop){
            Storage::disk('public')->put("img/sliders/$request->img_desktop", Storage::disk('public')->get("tmp/$request->img_desktop"));
        }
        if($request->img_mobile){
            Storage::disk('public')->put("img/sliders/$request->img_mobile", Storage::disk('public')->get("tmp/$request->img_mobile"));
        }
        Slider::create([
            'title' => $request->title,
            'img_desktop' => $request->img_desktop,
            'img_mobile' => $request->img_mobile,
            'link' => $request->link,
            'order' => $request->order
        ]);
        return response()->json(['success'=> true, 'message'=> 'Slider registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/slider/{id}",
     *      operationId="getSlider",
     *      tags={"Slider"},
     *      summary="Display an slider",
     *      @OA\Parameter(
     *          description="ID of slider",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show an slider.",
     *          @OA\JsonContent(ref="#/components/schemas/Slider")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $slider = Slider::find($id);
        if ($slider) {
            return response()->json($slider);
        } else {
            return response()->json(['success' => false, 'message' => 'Slider no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/slider/{id}",
     *      summary="Update an slider",
     *      description="Update an slider object",
     *      operationId="updateSlider",
     *      tags={"Slider"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Slider",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Slider")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::withTrashed()->find($id);
        if ($slider) {
            $rules = [
                'title' => 'nullable|max:255|alpha_numeric_space',
                'img_desktop' => 'required|max:255',
                'img_mobile' => 'required|max:255',
                'order' => 'required|integer',
                'link' => 'nullable|max:255',
            ];
            $input = $request->only(
                'title',
                'img_desktop',
                'img_mobile',
                'link',
                'order'
            );
            $messages = [
                'title.alpha_numeric_space' => 'Título debe contener solo carácteres alfanuméricos',
                'title.max' => 'Título debe contener máximo 255 carácteres',
                'img_desktop.max' => 'Imágen debe contener máximo 255 carácteres',
                'img_desktop.required' => 'Imágen de escritorio requerida',
                'img_mobile.max' => 'Nombre debe contener máximo 255 carácteres',
                'img_mobile.required' => 'Imágen de movil requerida',
                'link.max' => 'Nombre debe contener máximo 255 carácteres',
                'order.integer' => 'Orden debe ser entero',
                'order.required' => 'Orden requerido',
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }

            if($request->img_desktop && $slider->img_desktop !== $request->img_desktop){
                Storage::disk('public')->delete('img/sliders/' . $slider->img_desktop);
                Storage::disk('public')->put("img/sliders/$request->img_desktop", Storage::disk('public')->get("tmp/$request->img_desktop"));
            }
            if($request->img_mobile && $slider->img_mobile !== $request->img_mobile){
                Storage::disk('public')->delete('img/sliders/' . $slider->img_mobile);
                Storage::disk('public')->put("img/sliders/$request->img_mobile", Storage::disk('public')->get("tmp/$request->img_mobile"));
            }
            
            $slider->title = $request->title;
            $slider->img_desktop = $request->img_desktop;
            $slider->img_mobile = $request->img_mobile;
            $slider->link = $request->link;
            $slider->order = $request->order;
            $slider->save();
            return response()->json(['success'=> true, 'message'=> 'Slider actualizado']);
        } else {
            return response()->json(['success' => false, 'message' => 'Slider no encontrado'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/slider/{id}",
     *      summary="Delete an slider",
     *      description="Delete an slider object",
     *      operationId="deleteSlider",
     *      tags={"Slider"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Slider",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::withTrashed()->find($id);
        if ($slider) {
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'Debe ser true o false, 1 ó 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $slider->delete();
            } else {
                $slider->forceDelete();
            }
            return response()->json(['message'=> 'Ocupación eliminada']);
        } else {
            return response()->json(['message'=> 'Slider Not Found'], 404);
        }
    }
    /**
     * @OA\Post(
     *      path="/api/slider/{id}/restore",
     *      summary="Restore an slider",
     *      description="Restore an slider object",
     *      operationId="restoreSlider",
     *      tags={"Slider"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of slider",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $slider = Slider::withTrashed()->find($id);
        if($slider){
            $slider->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Slider no encontrado'], 404);
        }
    }
}
