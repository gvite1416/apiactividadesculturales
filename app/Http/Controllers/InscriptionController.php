<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Inscription;
use Validator;
use Illuminate\Validation\Rule;
use App\Libraries\Dates;
use App\User;
use App\WorkshopPeriod;
use App\Period;
use App\Captureline;
use PDF;
use App\Libraries\FormatData;
use QRCode;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\Mail\WelcomeWorkshopEmail;
use Mail;
use App\Mail\CapturelineEmail;

class InscriptionController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/inscription",
     *      operationId="getInscriptionList",
     *      tags={"Inscriptions"},
     *      summary="Display a listing of the inscription",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Inscription.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Inscription")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:folio,ticket_date,quota,status,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be folio,ticket_date,quota,status,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', true);
        $orderBy = $request->query('orderBy', 'created_at');
        $totalRegister = Inscription::withTrashed()->count();
        if($limit == -1){
            $inscriptions = Inscription::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
        } else {
            $inscriptions = Inscription::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }
        return response()->json($inscriptions)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Get(
     *      path="/api/inscription/{id}",
     *      operationId="getInscription",
     *      tags={"Inscriptions"},
     *      summary="Display an inscription",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of inscription",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show an inscription.",
     *          @OA\JsonContent(ref="#/components/schemas/Inscription")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inscription = Inscription::with("user")
            ->with("user_validate")
            ->with("workshop_period.workshop")
            ->with("captureline")
            ->with("workshop_period.period")->find($id);
        if($inscription){
            return response()->json($inscription);
        }else{
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }
    /**
     * @OA\Put(
     *      path="/api/inscription/{id}/validate",
     *      summary="Validate an inscription",
     *      description="Validate an inscription",
     *      operationId="validateInscription",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Inscription")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * Validate inscription
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validateStatus(Request $request, $id)
    {
        $inscription = Inscription::find($id);
        if($inscription) {
            $rules = [
                'ticket_folio' => ['nullable','max:255','alpha_num', Rule::unique('inscriptions')->ignore($id)],
                'ticket_date' => 'nullable|date',
                'extra' => 'nullable|integer',
                'scholarship' => 'nullable|integer',
            ];
            $input = $request->only(
                'ticket_folio',
                'ticket_date',
                'extra',
                'scholarship'
            );
            $messages = [
                'ticket_folio.max' => 'Maximo 255 caracteres',
                'ticket_folio.alpha_num' => 'Sólo alfanuméricos',
                'ticket_folio.unique' => 'Ya existe el folio de ticket',
                'ticket_date.date' => 'Fecha no válida',
                'extra.integer' => 'Sólo números enteros',
                'scholarship.integer' => 'Sólo números enteros',
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                $errors = $validator->errors();
                if ($errors->has('ticket_folio')) {
                    $inscription = Inscription::where('ticket_folio', $request->ticket_folio)->first();
                    return response()->json(['success'=> false, 'error'=> $error, 'inscription_id' => $inscription->id]);
                } else {
                    return response()->json(['success'=> false, 'error'=> $error]);
                }
            }
            $inscription->ticket_folio = $request->ticket_folio;
            $inscription->ticket_date = ($request->ticket_date) ? Dates::changeFormat($request->ticket_date): $request->ticket_date;
            $inscription->extra = $request->extra;
            $inscription->scholarship = $request->scholarship;
            $user = auth()->user();
            $inscription->status = 1;
            $inscription->user_validate_id = $user->id;
            $inscription->save();
            try {
                Mail::to($inscription->user)->send(new WelcomeWorkshopEmail($inscription));
            } catch (\Exception $e) {
                //Return with error
                $error_message = $e->getMessage();
                return response()->json(['success' => false, 'error' => $error_message], 401);
            }
            return response()->json(['success'=> true, 'message'=> 'Inscripción validada']);
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }

    }
    /**
     * @OA\Put(
     *      path="/api/inscription/ask-again-files/{id}",
     *      summary="Change status 6",
     *      description="Change status inscription to 6",
     *      operationId="askAgainFiles",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * 
     * Validate inscription
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function askAgainFiles(Request $request, $id)
    {
        $inscription = Inscription::find($id);
        if($inscription) {
            $inscription->status = 6;
            $inscription->save();
            return response()->json(['success'=> true, 'message'=> 'Se cambió el estado']);
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }

    }
    /**
     * @OA\Delete(
     *      path="/api/inscription/{id}",
     *      summary="Delete an inscription",
     *      description="Delete an inscription object",
     *      operationId="deleteInscription",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscription",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Delete inscription
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $inscription = Inscription::find($id);
        if($inscription) {
            if($inscription->captureline_id){
                $capturelineToFree = Captureline::find($inscription->captureline_id);
                if($capturelineToFree){
                    $capturelineToFree->inscription_id = null;
                    $capturelineToFree->save();    
                }
            }
            $inscription->captureline_id = null;
            $inscription->expiration = null;
            $inscription->status = 3;
            $inscription->save();
            return response()->json(['success'=> true, 'message'=> 'Se cambio el estado']);
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }

    }
    /**
     * @OA\Post(
     *      path="/api/inscription/check-ticket/{id}",
     *      summary="Check ticket",
     *      description="Check if ticket folio exist",
     *      operationId="checkTicket",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="ticket_folio", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * check folio
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkTicket(Request $request, $id)
    {
        $rules = [
            'ticket_folio' => ['required','max:255','alpha_num', Rule::unique('inscriptions')->ignore($id)]
        ];
        $input = $request->only(
            'ticket_folio'
        );
        $messages = [
            'ticket_folio.required' => 'required',
            'ticket_folio.max' => 'max',
            'ticket_folio.alpha_num' => 'alpha_num',
            'ticket_folio.unique' => 'unique'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        return response()->json(['success'=> true]);
    }

    /**
     * @OA\Post(
     *      path="/api/inscription/inscribe/{id}",
     *      summary="Create inscription",
     *      description="Inscribe from Administrator",
     *      operationId="inscribe",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="user_id", type="integer"),
     *              @OA\Property(property="price", type="integer"),
     *              @OA\Property(property="college_degree_option", type="boolean"),
     *              @OA\Property(property="sendCaptureline", type="boolean")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Inscription
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inscribe(Request $request, $id)
    {
        $wp = WorkshopPeriod::find($id);
        $user = User::find($request->user_id);
        if ($wp && $user) {
            $folio = -1;
            $b = true;
            while ($b) {
                $folio = mt_rand(1, 2147483647);
                if (!Inscription::where('folio' , '=', $folio)->first()) {
                    $b = false;
                }
            }
            $student_price = $request->price ? $request->price : 0;
            foreach($wp->students as $student) {
                foreach($user->roles as $role) {
                    if($role->slug === $student->slug) {
                        $student_price = $student->pivot->price;
                    }
                }
            }
            $insc = Inscription::create([
                'workshop_period_id' => $wp->id,
                'user_id' => $user->id,
                'folio' => $folio,
                'quota' => $student_price,
                'college_degree_option' => $request->college_degree_option == 1 || $request->college_degree_option == true ? $request->college_degree_option : 0
            ]);
            if ($request->sendCaptureline){
                $this->sendCaptureline($insc->id);
            }
            return response()->json(['success'=> true]);
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }
    

    /**
     * @OA\Post(
     *      path="/api/inscription/change/{id}",
     *      summary="Change a workshop",
     *      description="Change a workshop from Administrator",
     *      operationId="change",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="workshop_period_id", type="integer")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * change inscription
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function change(Request $request, $id){
        $inscription = Inscription::find($id);
        if ($inscription) {
            $wp = WorkshopPeriod::find($request->workshop_period_id);
            if ($wp) {
                $inscription->workshop_period_id = $wp->id;
                $inscription->save();
                return response()->json(['success' => true, 'message' => 'Taller cambiado']);
            } else {
                return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
            }
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }
    /**
     * 
     * @OA\Get(
     *      path="/api/update-inscriptions-cron",
     *      summary="update inscriptions cron",
     *      description="Update status inscriptions",
     *      operationId="updateStatusCron",
     *      tags={"Inscriptions"},
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      )
     * )
     * update status inscriptions
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatusCron(Request $request){
        $inscriptions = Inscription::where('status', 5)->get();
        $inscriptions2 = Inscription::where('status', 2)->get();
        $now = Dates::getTimeUTC(false, true);
        foreach($inscriptions as $inscription){
            if ($now >= Dates::getTimeUTC($inscription->expiration)) {
                $inscription->status = 2;
                $inscription->save();
            }
        }
        foreach($inscriptions2 as $inscription){
            
            if ($now >= Dates::getTimeUTCAddOneDay($inscription->expiration)) {
                $inscription->status = 2;
                $inscription->save();
            }
        }
        return response()->json(['success' => true, 'message' => 'Proceso completado']);
    }

    /**
     * @OA\Get(
     *      path="/api/inscription/by-user/{id}",
     *      summary="Get inscription by user logged",
     *      description="Get user inscription",
     *      operationId="getByUser",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show user inscription.",
     *          @OA\JsonContent(ref="#/components/schemas/Inscription")              
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Get By User
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getByUser(Request $request, $id) {
        $user = auth()->user();
        $inscription = Inscription::where('id', '=', $id)->where('user_id', '=', $user->id)->first();
        if ($inscription) {
            // $inscription->user->roles;
            $inscription->user->user_requirements;
            $inscription->workshop_period->workshop;
            $inscription->workshop_period->schedules;
            $inscription->workshop_period->period->requirements;
            $inscription->captureline;
            return response()->json($inscription);
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }
    /**
     * @OA\Get(
     *      path="/api/inscription/by-user/{id}/{user_id}",
     *      summary="Get inscription by any user",
     *      description="Get user inscription from Administrator",
     *      operationId="getByUserAdmin",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="ID of user",
     *          in="path",
     *          name="user_id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show user inscription.",
     *          @OA\JsonContent(ref="#/components/schemas/Inscription")              
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Get By Period
     * 
     * @param int $period_id
     * @return \Illuminate\Http\Response
     */
    public function getByUserAdmin(Request $request, $id, $user_id) {
        $inscription = Inscription::where('id', '=', $id)->where('user_id', '=', $user_id)->first();
        if ($inscription) {
            $inscription->user->roles;
            $inscription->workshop_period->workshop;
            $inscription->workshop_period->schedules;
            $inscription->workshop_period->period->requirements;
            return response()->json($inscription);
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }

    function getInscribePdf(Request $request, $id) {
        $user = auth()->user();
        $inscription = Inscription::where('id', '=', $id)->where('user_id', '=', $user->id)->first();
        if($inscription && $inscription->user->id == $user->id) {
            $role = false;
            foreach($inscription->user->roles as $r) {
                $role = $r;
            }
            $inscription->workshop_period->workshop;
            $inscription->workshop_period->period->requirements;
            $this->generateQrUser($user);
            $data = ['inscription' => $inscription , 'role' => $role, 'token' => $request->query('token')];
            $pdf = PDF::loadView('pdf.inscription', $data);
            return $pdf->download('inscription.pdf');
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }
    function getInscribePdfUser(Request $request, $user_id, $id) {
        $inscription = Inscription::where('id', '=', $id)->where('user_id', '=', $user_id)->first();
        if($inscription && $inscription->user->id == $user_id) {
            $role = false;
            foreach($inscription->user->roles as $r) {
                $role = $r;
            }
            $inscription->workshop_period->workshop;
            $inscription->workshop_period->period->requirements;
            $this->generateQrUser($inscription->user);
            $data = ['inscription' => $inscription , 'role' => $role, 'token' => $request->query('token')];
            $pdf = PDF::loadView('pdf.inscription', $data);
            return $pdf->download('inscription.pdf');
        } else {
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }

    private function generateQrUser($user) {
        if(!Storage::disk("users")->exists("$user->id")){
            Storage::disk("users")->makeDirectory("$user->id", 0775, true); //creates directory
        }
        if(!Storage::disk("users")->exists("$user->id/qr.svg")) {
            $encrypt_code = Crypt::encrypt(json_encode([
                'id' => $user->id,
                'number_id' => $user->number_id,
                'name' => $user->name,
                'lastname' => $user->lastname,
                'surname' => $user->surname,
                'user_type' => (isset($user->roles[0])) ? $user->roles[0]->slug : 'Indefinido'
            ]));
            $qr = QRCode::text($encrypt_code);
            $qr->setOutfile(storage_path() . "/app/users/$user->id/qr.svg")->svg();
        }
    }

    /**
     * @OA\Put(
     *      path="/api/inscription/files-uploaded/{id}",
     *      summary="Change status 5",
     *      description="Change status inscription to 5",
     *      operationId="filesUploaded",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function filesUploaded($id)
    {
        $inscription = Inscription::find($id);
        if($inscription){
            $inscription->status = 5;
            $inscription->save();
            return response()->json(['success' => true]);
        }else{
            return response()->json(['success' => false, 'message' => 'Inscripción no encontrada'], 404);
        }
    }
    /**
     * 
     * @OA\Get(
     *      path="/api/inscription/files-actives",
     *      summary="Get inscriptions",
     *      description="Get inscriptions that review files",
     *      operationId="filesActives",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="Show user inscription.",
     *          @OA\JsonContent(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/Inscription")
     *          )   
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filesActives(Request $request)
    {
        $period = Period::current()->first();
        $inscriptions = [];
        $totalRegister = 0;
        $limit = $request->query('limit', 10);
        $page = $request->query('page', 1);
        if($period){    
            $totalRegister = Inscription::where("status", "=", 5)->count();
            $inscriptions = Inscription::where("status", "=", 5)->skip($limit * ($page - 1))->take($limit)->with("user")->with("workshop_period.workshop")->get();
        }
        return response()->json($inscriptions)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }
    /**
     * @OA\Put(
     *      path="/api/inscription/send-catureline/{id}",
     *      summary="Send captureline",
     *      description="Assign and send captureline ",
     *      operationId="sendCaptureline",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendCaptureline($id) {
        $inscription = Inscription::find($id);
        $captureline = Captureline::where("value", "=", $inscription->quota)->where("inscription_id", "=", null)->where("period_id", "=", $inscription->workshop_period->period_id)->first();
        if($captureline) {
            $captureline->inscription_id = $inscription->id;
            $captureline->save();
            $inscription->status = 4;
            $inscription->captureline_id = $captureline->id;
            $inscription->expiration = Dates::addNowTwoDays(true);
            $inscription->save();
            try{
                Mail::to($inscription->user)->queue(new CapturelineEmail($inscription, env('URL_WEB', '') . 'mis-talleres/' . $inscription->id));
            } catch(\Exception $e) {
                $error_message = $e->getMessage();
                $success = false;
            }
            return response()->json(['success' => true]);
        } else{
            return response()->json(['success' => false, 'message' => "Líneas de captura con valor de $inscription->quota y/o del periodo " . $inscription->workshop_period->period->name . " están agotadas"]);
        }
        
    }

    /**
     * @OA\Put(
     *      path="/api/inscription/resend-catureline/{id}",
     *      summary="Resend captureline",
     *      description="Reassign and resend captureline ",
     *      operationId="resendCaptureline",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function resendCaptureline($id) {        
        $inscription = Inscription::find($id);
        $captureline = Captureline::where("value", "=", $inscription->quota)->where("inscription_id", "=", null)->where("period_id", "=", $inscription->workshop_period->period_id)->first();
        if($inscription->captureline_id){
            $capturelineToFree = Captureline::find($inscription->captureline_id);
            if($capturelineToFree){
                $capturelineToFree->inscription_id = null;
                $capturelineToFree->save();
            }
        }
        if($captureline) {
            $captureline->inscription_id = $inscription->id;
            $captureline->save();
            $inscription->status = 4;
            $inscription->captureline_id = $captureline->id;
            $inscription->expiration = Dates::addNowTwoDays(true);
            $inscription->save();
            try{
                Mail::to($inscription->user)->queue(new CapturelineEmail($inscription, env('URL_WEB', '') . 'mis-talleres/' . $inscription->id));
            } catch(\Exception $e) {
                $error_message = $e->getMessage();
                $success = false;
            }
            return response()->json(['success' => true]);
        } else{
            return response()->json(['success' => false, 'message' => "Líneas de captura con valor de $inscription->quota y/o del periodo " . $inscription->workshop_period->period->name . " están agotadas"]);
        }
        
    }

    /**
     * @OA\Put(
     *      path="/api/inscription/remove-catureline/{id}",
     *      summary="Remove captureline",
     *      description="Remove captureline",
     *      operationId="removeCaptureline",
     *      tags={"Inscriptions"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Inscriptions",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove captureline.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeCaptureline($id) {        
        $inscription = Inscription::find($id);
        if($inscription->captureline_id){
            $capturelineToFree = Captureline::find($inscription->captureline_id);
            if($capturelineToFree){
                $capturelineToFree->inscription_id = null;
                $capturelineToFree->save();
                $inscription->captureline_id = null;
                $inscription->expiration = null;
                $inscription->save();    
            }
        }
        return response()->json(['success' => true]);
    }

}
