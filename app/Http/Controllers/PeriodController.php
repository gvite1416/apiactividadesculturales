<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Period;
use Validator;
use App\Libraries\Dates;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str as Str;
use App\PeriodRequirement;
use App\UserPeriodRequirement;
use App\Inscription;
use App\User;

class PeriodController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/period",
     *      operationId="getPeriodList",
     *      tags={"Period"},
     *      summary="Display a listing of the periods",
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="Pagination: Limit of records to request, if is -1 get all records",
     *          in="query",
     *          name="limit",
     *          required=false,
     *          example="10",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Page number",
     *          in="query",
     *          name="page",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: Order Asc = true, Desc = false",
     *          in="query",
     *          name="orderAsc",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Pagination: field to order",
     *          in="query",
     *          name="orderBy",
     *          required=false,
     *          example="name",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show Period.",
     *          @OA\JsonContent(
     *                  type="array",
     *                  @OA\Items(ref="#/components/schemas/Period")
     *          ),
     *          @OA\Header(
     *              header="X-Total-Registers",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          ),
     *          @OA\Header(
     *              header="X-Total-Pages",
     *              @OA\Schema(
     *                  type="integer"
     *              )
     *          )
     *      )
     * )
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'limit' => 'nullable|integer',
            'page' => 'nullable|integer',
            'orderAsc' => 'nullable|boolean_get',
            'orderBy' => 'nullable|in:name,created_at,updated_at',
        ];
        $input = $request->only(
            'limit',
            'page',
            'orderAsc',
            'orderBy'
        );
        $messages = [
            'limit.integer' => 'limit must be integer',
            'page.integer' => 'page must be integer',
            'orderAsc.boolean_get' => 'orderAsc must be true or false, 1 or 0',
            'orderBy.in' => 'orderBy must be name,created_at or updated_at',
            
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['message'=> $error], 400);
        }
        $limit = $request->query('limit', -1);
        $page = $request->query('page', 1);
        $orderAsc = $request->query('orderAsc', false);
        $orderBy = $request->query('orderBy', 'created_at');

        $user = auth()->user();
        if($user && $user->group() == 'depto'){
            $totalRegister = Period::withTrashed()->count();
            if($limit == -1)
                $periods = Period::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $periods = Period::withTrashed()->orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        } else {
            $totalRegister = Period::count();
            if($limit == -1)
                $periods = Period::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->get();
            else
                $periods = Period::orderBy($orderBy, $orderAsc == 'true' || $orderAsc == '1' ? 'asc' : 'desc')->skip($limit * ($page - 1))->take($limit)->get();
        }
        return response()->json($periods)->header('X-Total-Registers', $totalRegister)->header('X-Total-Pages', ceil($totalRegister/$limit) );
    }

    /**
     * @OA\Post(
     *      path="/api/period",
     *      summary="Store a period",
     *      description="Store a period object",
     *      operationId="storePeriod",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Period")
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255|alpha_numeric_dash_space',
            'start' => 'required|date',
            'finish' => 'required|date',
            'inscription_start' => 'required|date',
            'inscription_finish' => 'required|date'
        ];
        $input = $request->only(
            'name',
            'start',
            'finish',
            'inscription_start',
            'inscription_finish'
        );
        $messages = [
            'name.required' => 'Se requiere nombre',
            'name.alpha_numeric_dash_space' => 'Nombre debe contener solo carácteres alfanuméricos',
            'name.max' => 'Nombre debe contener máximo 255 carácteres',
            'start.required' => 'Se require fecha de inicio de periodo',
            'start.date_format' => 'La fecha de inicio de periodo no es válida',
            'finish.required' => 'Se require fecha de termino de periodo',
            'finish.date_format' => 'La fecha de termino de periodo no es válida',
            'inscription_start.required' => 'Se require fecha de inicio de inscripción',
            'inscription_start.date_format' => 'La fecha de inicio de inscripción no es válida',
            'inscription_finish.required' => 'Se require fecha de termino de inscripción',
            'inscription_finish.date_format' => 'La fecha de termino de inscripción no es válida'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails()) {
            $error = $validator->messages();
            return response()->json(['success'=> false, 'error'=> $error]);
        }
        Period::create([
            'name' => $request->name,
            'start' => Dates::changeFormat($request->start),
            'finish' => Dates::changeFormat($request->finish),
            'inscription_start' => Dates::changeFormat($request->inscription_start),
            'inscription_finish' => Dates::changeFormat($request->inscription_finish)
        ]);
        return response()->json(['success'=> true, 'message'=> 'Periodo registrado']);
    }

    /**
     * @OA\Get(
     *      path="/api/period/{id}",
     *      operationId="getPeriod",
     *      tags={"Period"},
     *      summary="Display a period",
     *      @OA\Parameter(
     *          description="ID of period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show a period.",
     *          @OA\JsonContent(ref="#/components/schemas/Period")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $period = Period::find($id);
        if($period){
            return response()->json($period);
        }else{
            return response()->json(['success' => false, 'message' => 'Periodo no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/period/{id}",
     *      summary="Update a period",
     *      description="Update a period object",
     *      operationId="updatePeriod",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/Period")
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $period = Period::withTrashed()->find($id);
        if($period){
            $rules = [
                'name' => 'required|max:255|alpha_numeric_dash_space',
                'start' => 'required|date',
                'finish' => 'required|date',
                'inscription_start' => 'required|date',
                'inscription_finish' => 'required|date'
            ];
            $input = $request->only(
                'name',
                'start',
                'finish',
                'inscription_start',
                'inscription_finish'
            );
            $messages = [
                'name.required' => 'Se requiere nombre',
                'name.alpha_numeric_dash_space' => 'Nombre debe contener solo carácteres alfanuméricos',
                'name.max' => 'Nombre debe contener máximo 255 carácteres',
                'start.required' => 'Se require fecha de inicio de periodo',
                'start.date_format' => 'La fecha de inicio de periodo no es válida',
                'finish.required' => 'Se require fecha de termino de periodo',
                'finish.date_format' => 'La fecha de termino de periodo no es válida',
                'inscription_start.required' => 'Se require fecha de inicio de inscripción',
                'inscription_start.date_format' => 'La fecha de inicio de inscripción no es válida',
                'inscription_finish.required' => 'Se require fecha de termino de inscripción',
                'inscription_finish.date_format' => 'La fecha de termino de inscripción no es válida'
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['success'=> false, 'error'=> $error]);
            }
            $period->name = $request->name;
            $period->start = Dates::changeFormat($request->start);
            $period->finish = Dates::changeFormat($request->finish);
            $period->inscription_start = Dates::changeFormat($request->inscription_start);
            $period->inscription_finish = Dates::changeFormat($request->inscription_finish);
            $period->save();
            return response()->json(['success'=> true, 'message'=> 'Periodo registrado']);
        }else{
            return response()->json(['success' => false, 'message' => 'Periodo no válido'], 404);
        }
    }

    /**
     * @OA\Delete(
     *      path="/api/period/{id}",
     *      summary="Delete a period",
     *      description="Delete a period object",
     *      operationId="deletePeriod",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Parameter(
     *          description="Force delete to database",
     *          in="query",
     *          name="force",
     *          required=false,
     *          example="true",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $period = Period::withTrashed()->find($id);
        if ($period) {
            $rules = [
                'force' => 'nullable|boolean_get',
            ];
            $input = $request->only(
                'force'
            );
            $messages = [
                'force.boolean_get' => 'Debe ser true o false, 1 ó 0'
                
            ];
            $validator = Validator::make($input, $rules, $messages);
            if($validator->fails()) {
                $error = $validator->messages();
                return response()->json(['message'=> $error], 400);
            }
            $force = $request->query('force', 'false');
            if($force == 'false') {
                $period->delete();
            } else {
                $period->forceDelete();
            }
            return response()->json(['message'=> 'Ocupación eliminada']);
        } else {
            return response()->json(['message'=> 'Period Not Found'], 404);
        }
    }

    /**
     * @OA\Post(
     *      path="/api/period/{id}/restore",
     *      summary="Restore a period",
     *      description="Restore a period object",
     *      operationId="restorePeriod",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of period",
     *          in="path",
     *          name="id",
     *          required=true,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $period = Period::withTrashed()->find($id);
        if($period){
            $period->restore();
            return response()->json(['success' => true, 'message'=> 'Se restauró correctamente']);
        }else{
            return response()->json(['success' => false, 'message' => 'Taller no encontrado'], 404);
        }
    }

    /**
     * @OA\Put(
     *      path="/api/period/check-name/{id?}",
     *      summary="checkNamePeriod",
     *      description="Check if the name exist",
     *      operationId="checkNamePeriod",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of Period",
     *          in="path",
     *          name="id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string")
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * API Check Name
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkName(Request $request, $id = false){
        if ($id === false) {
            $rules = [
                'name' => 'required|max:150|unique:periods',
            ];
        } else {
            $rules = [
                'name' => ['required', 'max:150', Rule::unique('periods')->ignore($id)],
            ];
        }
        $input = $request->only(
            'name'
        );
        $messages = [
            'name.required' => 'required',
            'name.unique' => 'unique'
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()], 400);
        }
        return response()->json(['success'=> true]);
    }
    /**
     * @OA\Post(
     *      path="/api/period/requirement",
     *      summary="Add or remove Requirement to period",
     *      description="Add or remove Requirement to period",
     *      operationId="requirement",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(
     *              @OA\Property(property="period_id", type="integer"),
     *              @OA\Property(property="requirement_id", type="integer"),
     *              @OA\Property(property="action", type="boolean", description="true=add and false=remove"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Success"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Some field is not valid",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="object"),
     *          )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Returns when user is not authenticated",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="Not authorized"),
     *          )
     *      )
     * )
     * API add or delete requirement
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function requirement(Request $request){
        $rules = [
            'period_id' => 'required|numeric|exists:periods,id',
            'requirement_id' => 'required|numeric|exists:requirements,id',
            'action' => 'required'
        ];
        $input = $request->only(
            'period_id',
            'requirement_id',
            'action'
        );
        $messages = [
            'period_id.required' => 'Se require periodo',
            'period_id.numeric' => 'Periodo id debe ser numérico',
            'period_id.exists' => 'No existe el periodo',
            'requirement_id.required' => 'Se require requerimiento',
            'requirement_id.numeric' => 'Requerimiento id debe ser numérico',
            'requirement_id.exists' => 'No existe el requerimiento',
            'action.required' => 'No existe el requerimiento',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if($validator->fails() ) {
            return response()->json(['success'=> false, 'error'=> $validator->messages() , 'id' => $id]);
        }
        $periodRequirement = PeriodRequirement::where('period_id' , $request->period_id)->where('requirement_id', $request->requirement_id)->first();
        if($request->action){
            if(!$periodRequirement) {
                PeriodRequirement::create([
                    'period_id' => $request->period_id,
                    'requirement_id' => $request->requirement_id
                ]);
            }
        } else {
            if($periodRequirement) {
                PeriodRequirement::destroy($periodRequirement->id);
            }
        }
        return response()->json(['success'=> true]);
    }

    /**
     * @OA\Get(
     *      path="/api/period/current",
     *      summary="Get current period",
     *      description="Get current period",
     *      operationId="current",
     *      tags={"Period"},
     *      @OA\Response(
     *          response=200,
     *          description="Show current period.",
     *          @OA\JsonContent(ref="#/components/schemas/Period")              
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function current()
    {
        $period = Period::current()->first();
        if($period){
            return response()->json($period);
        }else{
            return response()->json(['success' => false, 'message' => 'Periodo no encontrado'], 404);
        }
    }
    /**
     * @OA\Get(
     *      path="/api/period/user-files-requirements",
     *      summary="Get all requirements by current period",
     *      description="Get current period",
     *      operationId="requirementsFilesPeriodUser",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\Response(
     *          response=200,
     *          description="Show current period.",
     *          @OA\JsonContent(
     *              @OA\Property(property="period", type="object"),
     *              @OA\Property(property="requirementsPeriod", type="object"),
     *              @OA\Property(property="requirementsWorkshop", type="object"),
     *          )           
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get all requirements by period.
     *
     * @return \Illuminate\Http\Response
     */
    public function requirementsFilesPeriodUser()
    {
        $period = Period::current()->first();
        if($period){
            $user = auth()->user();
            $requirementsPeriod = [];
            $requirementsWorkshop = [];
            $requirementsWorkshopAux = [];
            foreach($user->inscriptions()->has('workshop_period')->get() as $inscription){
                if($inscription->workshop_period->period_id == $period->id && $inscription->captureline_id){
                    $requirementsWorkshopAux[$inscription->workshop_period->workshop->id] = [
                        "id" => $inscription->workshop_period->workshop->id,
                        "name" => $inscription->workshop_period->workshop->name,
                        "inscription_id" => $inscription->id,
                        "requirements" => []
                    ];      
                }
            }
            foreach($period->requirements as $requirement){
                if($requirement->role_id == $user->role->id && $requirement->has_file){
                    if($requirement->upload_type == "PERIOD"){
                        $userRequirement = UserPeriodRequirement::where("user_id", "=", $user->id)->where("period_requirement_id", "=", $requirement->pivot->id)->first();
                        $requirementsPeriod[] = [
                            "id" => $requirement->id,
                            "name" => $requirement->name,
                            "file" => ($userRequirement) ? $userRequirement->file : '',
                            "file_url" => ($userRequirement) ? $userRequirement->file_url : '',
                            "mime" => ($userRequirement) ? $userRequirement->mime : '',
                            "period_requirement_id" => $requirement->pivot->id,
                            "user_period_requirement_id" => ($userRequirement) ? $userRequirement->id : ''
                        ];
                    } else{
                        foreach($user->inscriptions()->has('workshop_period')->get() as $inscription){
                            if($inscription->workshop_period->period_id == $period->id && $inscription->captureline_id){
                                $userRequirement = UserPeriodRequirement::where("user_id", "=", $user->id)
                                    ->where("period_requirement_id", "=", $requirement->pivot->id)
                                    ->where("inscription_id", "=", $inscription->id)->first();
                                
                                $requirementsWorkshopAux[$inscription->workshop_period->workshop->id]["requirements"][] = [
                                    "id" => $requirement->id,
                                    "name" => $requirement->name,
                                    "file" => ($userRequirement) ? $userRequirement->file : '',
                                    "file_url" => ($userRequirement) ? $userRequirement->file_url : '',
                                    "mime" => ($userRequirement) ? $userRequirement->mime : '',
                                    "inscription_id" => $inscription->id,
                                    "period_requirement_id" => $requirement->pivot->id,
                                    "user_period_requirement_id" => ($userRequirement) ? $userRequirement->id : ''
                                ];
                            }
                        }
                    }
                }
            }
            foreach($requirementsWorkshopAux as $req){
                $requirementsWorkshop[] = $req;
            }
            return response()->json(["success" => true, "period" => $period,"requirementsPeriod" => $requirementsPeriod, "requirementsWorkshop" => $requirementsWorkshop]);
            //return response()->json($requirements);
        }else{
            return response()->json(['success' => false, 'message' => 'Periodo no encontrado'], 404);
        }
    }
    /**
     * @OA\Get(
     *      path="/api/period/user-files-requirements/{user_id}",
     *      summary="Get all requirements by current period",
     *      description="Get current period",
     *      operationId="requirementsFilesPeriodByUser",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of user",
     *          in="path",
     *          name="user_id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show current period.",
     *          @OA\JsonContent(
     *              @OA\Property(property="period", type="object"),
     *              @OA\Property(property="requirementsPeriod", type="object"),
     *              @OA\Property(property="requirementsWorkshop", type="object"),
     *          )           
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get all requirements by period.
     *
     * @return \Illuminate\Http\Response
     */
    public function requirementsFilesPeriodByUser($user_id)
    {
        $period = Period::current()->first();
        if($period){
            $user = User::find($user_id);
            $requirementsPeriod = [];
            $requirementsWorkshop = [];
            $requirementsWorkshopAux = [];
            foreach($user->inscriptions as $inscription){
                if($inscription->workshop_period->period_id == $period->id && $inscription->captureline_id){
                    $requirementsWorkshopAux[$inscription->workshop_period->workshop->id] = [
                        "id" => $inscription->workshop_period->workshop->id,
                        "name" => $inscription->workshop_period->workshop->name,
                        "inscription_id" => $inscription->id,
                        "requirements" => []
                    ];      
                }
            }
            foreach($period->requirements as $requirement){
                if($requirement->role_id == $user->role->id && $requirement->has_file){
                    if($requirement->upload_type == "PERIOD"){
                        $userRequirement = UserPeriodRequirement::where("user_id", "=", $user->id)->where("period_requirement_id", "=", $requirement->pivot->id)->first();
                        $requirementsPeriod[] = [
                            "id" => $requirement->id,
                            "name" => $requirement->name,
                            "file" => ($userRequirement) ? $userRequirement->file : '',
                            "file_url" => ($userRequirement) ? $userRequirement->file_url : '',
                            "mime" => ($userRequirement) ? $userRequirement->mime : '',
                            "period_requirement_id" => $requirement->pivot->id,
                            "user_period_requirement_id" => ($userRequirement) ? $userRequirement->id : ''
                        ];
                    } else{
                        foreach($user->inscriptions as $inscription){
                            if($inscription->workshop_period->period_id == $period->id && $inscription->captureline_id){
                                $userRequirement = UserPeriodRequirement::where("user_id", "=", $user->id)
                                    ->where("period_requirement_id", "=", $requirement->pivot->id)
                                    ->where("inscription_id", "=", $inscription->id)->first();
                                
                                $requirementsWorkshopAux[$inscription->workshop_period->workshop->id]["requirements"][] = [
                                    "id" => $requirement->id,
                                    "name" => $requirement->name,
                                    "file" => ($userRequirement) ? $userRequirement->file : '',
                                    "file_url" => ($userRequirement) ? $userRequirement->file_url : '',
                                    "mime" => ($userRequirement) ? $userRequirement->mime : '',
                                    "inscription_id" => $inscription->id,
                                    "period_requirement_id" => $requirement->pivot->id,
                                    "user_period_requirement_id" => ($userRequirement) ? $userRequirement->id : ''
                                ];
                            }
                        }
                    }
                }
            }
            foreach($requirementsWorkshopAux as $req){
                $requirementsWorkshop[] = $req;
            }
            return response()->json(["success" => true, "period" => $period,"requirementsPeriod" => $requirementsPeriod, "requirementsWorkshop" => $requirementsWorkshop]);
            //return response()->json($requirements);
        }else{
            return response()->json(['success' => false, 'message' => 'Periodo no encontrado'], 404);
        }
    }
    /**
     * @OA\Get(
     *      path="/api/period/user-files-requirements-inscription/{inscription_id}",
     *      summary="Get all requirements by inscription",
     *      description="Get current period",
     *      operationId="requirementsFilesPeriodByInscription",
     *      tags={"Period"},
     *      security={ {"bearer": {} }},
     *      @OA\Parameter(
     *          description="ID of inscription",
     *          in="path",
     *          name="inscription_id",
     *          required=false,
     *          example="1",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Show current period.",
     *          @OA\JsonContent(
     *              @OA\Property(property="period", type="object"),
     *              @OA\Property(property="requirementsPeriod", type="object"),
     *              @OA\Property(property="requirementsWorkshop", type="object"),
     *          )           
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found."
     *      )
     * )
     * Get all requirements by period.
     *
     * @return \Illuminate\Http\Response
     */
    public function requirementsFilesPeriodByInscription($inscription_id)
    {
        $inscription = Inscription::find($inscription_id);
        if($inscription){
            $period = $inscription->workshop_period->period;
            $user = $inscription->user;
            $requirementsPeriod = [];
            $requirementsWorkshop = [
                "id" => $inscription->workshop_period->workshop->id,
                "name" => $inscription->workshop_period->workshop->name,
                "inscription_id" => $inscription->id,
                "requirements" => []
            ];
            foreach($period->requirements as $requirement){
                if($requirement->role_id == $user->role->id && $requirement->has_file){
                    if($requirement->upload_type == "PERIOD"){
                        $userRequirement = UserPeriodRequirement::where("user_id", "=", $user->id)->where("period_requirement_id", "=", $requirement->pivot->id)->first();
                        $requirementsPeriod[] = [
                            "id" => $requirement->id,
                            "name" => $requirement->name,
                            "file" => ($userRequirement) ? $userRequirement->file : '',
                            "file_url" => ($userRequirement) ? $userRequirement->file_url : '',
                            "mime" => ($userRequirement) ? $userRequirement->mime : '',
                            "period_requirement_id" => $requirement->pivot->id,
                            "user_period_requirement_id" => ($userRequirement) ? $userRequirement->id : ''
                        ];
                    } else{
                        if($inscription->workshop_period->period_id == $period->id && $inscription->captureline_id){
                            $userRequirement = UserPeriodRequirement::where("user_id", "=", $user->id)
                                ->where("period_requirement_id", "=", $requirement->pivot->id)
                                ->where("inscription_id", "=", $inscription->id)->first();
                            
                            $requirementsWorkshop["requirements"][] = [
                                "id" => $requirement->id,
                                "name" => $requirement->name,
                                "file" => ($userRequirement) ? $userRequirement->file : '',
                                "file_url" => ($userRequirement) ? $userRequirement->file_url : '',
                                "mime" => ($userRequirement) ? $userRequirement->mime : '',
                                "inscription_id" => $inscription->id,
                                "period_requirement_id" => $requirement->pivot->id,
                                "user_period_requirement_id" => ($userRequirement) ? $userRequirement->id : ''
                            ];
                        }
                    }
                }
            }
            return response()->json(["success" => true, "period" => $period,"requirementsPeriod" => $requirementsPeriod, "requirementsWorkshop" => $requirementsWorkshop]);
        }else{
            return response()->json(["success" => false, 'message' => 'Inscripción no encontrada']);
        }
    }
}
