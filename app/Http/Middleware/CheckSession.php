<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class CheckSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = auth()->getToken();
            if( $user === false) {
                return $next($request);
            } else {
                auth()->checkOrFail();
                return $next($request);
            }
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json([
                "message" => "Token has expired",
                "token" => auth()->refresh()
            ], 401);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
            return response()->json([
                "message" => "The token has been blacklisted"
            ], 401);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return $next($request);
        }
        return $next($request);
    }
}
