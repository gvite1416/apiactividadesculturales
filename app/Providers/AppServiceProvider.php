<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('alpha_space', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^([a-zA-Z áéíóúüñÁÉÍÓÚÜÑ])+$/i', $value);
        });
        Validator::extend('alpha_numeric_dash_space', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^([a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ\.\-\_])+$/i', $value);
        });
        Validator::extend('alpha_numeric_space', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^([a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ])+$/i', $value);
        });
        Validator::extend('alpha_address', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^([a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ\/\.#,])+$/i', $value);
        });
        Validator::extend('alpha_text', function ($attribute, $value, $parameters, $validator) {
            return preg_match('/^([a-zA-Z0-9 áéíóúüñÁÉÍÓÚÜÑ\.,¿?!¡\*\%])+$/i', $value);
        });
        Validator::extend('enum', function ($attribute, $value, $parameters, $validator) {
            return in_array($value,$parameters, true);
        });
        Validator::extend('boolean_get', function ($attribute, $value, $parameters, $validator) {
            return in_array($value,['true', 'false', '1', '0'], true);
        });
    }
}
