<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserExcel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sis','carr','gen','cuenta','paterno','materno','nombres','correo'
    ];

}
