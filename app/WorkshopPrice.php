<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkshopPrice extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id','workshop_id','price'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workshop_prices';
}
