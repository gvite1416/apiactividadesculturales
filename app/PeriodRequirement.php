<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeriodRequirement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'requirement_id','period_id'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'period_requirements';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function requirement(){
        return $this->belongsTo('App\Requirement');
    }
}
