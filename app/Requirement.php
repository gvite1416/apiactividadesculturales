<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *      title="Requirement",
 *      description="Requirement model",
 *      @OA\Xml(
 *          name="Requirement"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="name", type="string", example="Name"),
 *      @OA\Property(property="role_id", type="integer", example="1"),
 *      @OA\Property(property="has_file", type="boolean", example="true"),
 *      @OA\Property(property="upload_type", type="string", example="PERIOD|WORKSHOP"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */

class Requirement extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','role_id','has_file','upload_type'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'requirements';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    function role (){
        return $this->belongsTo('App\Role');
    }
}
