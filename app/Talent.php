<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Talent extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','career_id','semester','number_id','email','phone','celphone','arts','art_activity','band','gender','members','duration','equipment','scenography','illumination','audio_video','assembly_duration','deassembly_duration','token'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'talent';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function career(){
        return $this->belongsTo('App\Career');
    }
}
