<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * @OA\Schema(
 *      title="Campus",
 *      description="Campus model",
 *      @OA\Xml(
 *          name="Campus"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="name", type="string", example="Fes Aragón"),
 *      @OA\Property(property="code", type="string", example="000"),
 *      @OA\Property(property="college_degree_option", type="integer", example="0"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */
class Campus extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'college_degree_option'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'campus';

    public function dataStudents(){
        return $this->hasMany('App\DataStudent');
    }
}
