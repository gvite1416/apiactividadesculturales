<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class WorkshopPeriodSchedule extends Model
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('day', function (Builder $builder) {
            $builder->orderBy('day');
        });
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workshop_period_id','day','start','finish'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workshop_period_schedules';
}
