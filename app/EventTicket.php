<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *      title="EventTicket",
 *      description="EventTicket model",
 *      @OA\Xml(
 *          name="EventTicket"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="file", type="string", example=""),
 *      @OA\Property(property="event_id", type="integer", example="1"),
 *      @OA\Property(property="event_inscription_id", type="integer", example="1"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */

class EventTicket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file','event_id','event_inscription_id'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_tickets';

    function event (){
        return $this->belongsTo('App\Event');
    }
    function event_inscription (){
        return $this->belongsTo('App\EventInscription');
    }
}
