<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @OA\Schema(
 *      title="WorkshopPeriod",
 *      description="WorkshopPeriod model",
 *      @OA\Xml(
 *          name="WorkshopPeriod"
 *      ),
 *      @OA\Property(property="id", type="integer", readOnly="true", example="1"),
 *      @OA\Property(property="period_id", type="integer", example="1"),
 *      @OA\Property(property="workshop_id", type="integer", example="1"),
 *      @OA\Property(property="teacher_id", type="integer", example="1"),
 *      @OA\Property(property="classroom_id", type="integer", example="1"),
 *      @OA\Property(property="quota", type="integer"),
 *      @OA\Property(property="group", type="string"),
 *      @OA\Property(property="notes", type="string"),
 *      @OA\Property(property="all_campus", type="boolean"),
 *      @OA\Property(property="created_at", type="string", readOnly="true",format="date-time"),
 *      @OA\Property(property="updated_at", type="string", readOnly="true",format="date-time")
 * )
 */

class WorkshopPeriod extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'period_id','workshop_id','teacher_id','classroom_id','quota','group','all_campus','notes'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['students_inscribed_count','students_inscribed_count_validated', 'students_preinscribed_count', 'students_inscribed_verify_count'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'workshop_periods';

    function period (){
        return $this->belongsTo('App\Period');
    }

    function workshop (){
        return $this->belongsTo('App\Workshop');
    }
    function teacher (){
        return $this->belongsTo('App\User');
    }
    function classroom (){
        return $this->belongsTo('App\Classroom');
    }
    function schedules (){
        return $this->hasMany('App\WorkshopPeriodSchedule');
    }
    function workshop_period_students (){
        return $this->hasMany('App\WorkshopPeriodStudent');
    }
    function students () {
        return $this->belongsToMany('App\Role', 'workshop_period_students', 'workshop_period_id', 'role_id')->withPivot('id', 'price','limit');
    }
    function students_inscribed (){
        return $this->belongsToMany('App\User', 'inscriptions', 'workshop_period_id', 'user_id')->withTimestamps()->withPivot('id','folio','ticket_folio','captureline_id','ticket_date', 'created_at','status','quota','extra','scholarship');
    }
    function campus() {
        return $this->belongsToMany('App\Campus', 'workshop_period_campus', 'workshop_period_id', 'campus_id')->withTimestamps();
    }

    /**
     * Get students inscribed.
     *
     * @param  string  $value
     * @return string
     */
    public function getStudentsInscribedCountAttribute()
    {
        $count = 0;
        $students_inscribed = $this->students_inscribed()->get();
        if(count($students_inscribed) > 0) {
            foreach ($students_inscribed as $student) {
                if ($student->pivot->status == 0 || $student->pivot->status == 1 || $student->pivot->status == 4 || $student->pivot->status == 5) {
                    $count++;
                }
            }
        }
        return $count;
    }

    /**
     * Get studentn preinscribed.
     *
     * @param  string  $value
     * @return string
     */
    public function getStudentsPreinscribedCountAttribute()
    {
        $count = 0;
        $students_inscribed = $this->students_inscribed()->get();
        if(count($students_inscribed) > 0) {
            foreach ($students_inscribed as $student) {
                if ($student->pivot->status == 0) {
                    $count++;
                }
            }
        }
        return $count;
    }

    /**
     * Get studentn inscribed verify.
     *
     * @param  string  $value
     * @return string
     */
    public function getStudentsInscribedVerifyCountAttribute()
    {
        $count = 0;
        $students_inscribed = $this->students_inscribed()->get();
        if(count($students_inscribed) > 0) {
            foreach ($students_inscribed as $student) {
                if ($student->pivot->status == 1) {
                    $count++;
                }
            }
        }
        return $count;
    }

    /**
     * Get the Photo.
     *
     * @param  string  $value
     * @return string
     */
    public function getStudentsInscribedCountValidatedAttribute()
    {
        $count = 0;
        $students_inscribed = $this->students_inscribed()->get();
        if(isset($students_inscribed) && count($students_inscribed) > 0) {
            foreach ($students_inscribed as $student) {
                if ($student->pivot->status == 1) {
                    $count++;
                }
            }
        }
        return $count;
    }

}
