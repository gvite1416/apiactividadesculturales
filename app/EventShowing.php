<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventShowing extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id','start','finish'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_showings';

    function event (){
        return $this->belongsTo('App\Event');
    }

    function students_inscribed() {
        return $this->belongsToMany('App\User', 'event_inscriptions', 'event_showing_id', 'user_id')->withTimestamps()->withPivot('id','folio','ticket_folio','ticket_date','status','get_into_date','quota');
    }
}
