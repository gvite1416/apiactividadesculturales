<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventInscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_inscriptions' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('event_showing_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('folio')->unsigned();
            $table->string('ticket_folio')->nullable();
            $table->date('ticket_date')->nullable();
            $table->date('get_into_date')->nullable();
            $table->integer('quota')->unsigned();
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('event_showing_id')->references('id')->on('event_showings')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_inscriptions', function (Blueprint $table) {
            $table->dropForeign('event_inscriptions_event_showing_id_foreign'); 
            $table->dropForeign('event_inscriptions_user_id_foreign'); 
        });
        Schema::dropIfExists("event_inscriptions");
    }
}
