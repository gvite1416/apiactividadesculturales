<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataExternalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_externals' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('user_id')->unsigned();
            $table->string('address');
            $table->integer('occupation_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('occupation_id')->references('id')->on('occupations')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_externals', function (Blueprint $table) {
            $table->dropForeign('data_externals_user_id_foreign');
            $table->dropForeign('data_externals_occupation_id_foreign'); 
        });
        Schema::dropIfExists("data_externals");
    }
}
