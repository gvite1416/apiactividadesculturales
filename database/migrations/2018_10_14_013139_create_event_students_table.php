<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_students' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('event_id')->unsigned()->index();
            $table->integer('role_id')->unsigned()->index();
            $table->float('price')->unsigned()->default(0);
            $table->integer('limit')->default(-1);
            $table->timestamps();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_students', function (Blueprint $table) {
            $table->dropForeign('event_students_event_id_foreign'); 
            $table->dropForeign('event_students_role_id_foreign'); 
        });
        Schema::dropIfExists("event_students");
    }
}
