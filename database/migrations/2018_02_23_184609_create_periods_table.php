<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('periods' , function(Blueprint $table){
            $table->increments("id");
            $table->string('name');
            $table->date('start');
            $table->date('finish');
            $table->datetime('inscription_start');
            $table->datetime('inscription_finish');
            $table->boolean('is_current')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("periods");
    }
}
