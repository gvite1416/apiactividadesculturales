<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkshopPeriodStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workshop_period_students' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('workshop_period_id')->unsigned()->index();
            $table->integer('role_id')->unsigned()->index();
            $table->float('price')->unsigned()->default(0);
            $table->integer('limit')->default(-1);
            $table->timestamps();
            $table->foreign('workshop_period_id')->references('id')->on('workshop_periods')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workshop_period_students', function (Blueprint $table) {
            $table->dropForeign('workshop_period_students_workshop_period_id_foreign'); 
            $table->dropForeign('workshop_period_students_role_id_foreign'); 
        });
        Schema::dropIfExists("workshop_period_students");
    }
}
