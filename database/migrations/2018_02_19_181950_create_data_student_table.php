<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_students' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('user_id')->unsigned();
            $table->integer('year')->unsigned();
            $table->integer('career_id')->unsigned();
            $table->integer('campus_id')->unsigned();
            $table->enum('system',['ESC','SUA']);
            $table->unsignedTinyInteger('semester')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('career_id')->references('id')->on('careers')->onDelete('cascade');
            $table->foreign('campus_id')->references('id')->on('campus')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_students', function (Blueprint $table) {
            $table->dropForeign('data_students_user_id_foreign');
            $table->dropForeign('data_students_career_id_foreign'); 
            $table->dropForeign('data_students_campus_id_foreign'); 
        });
        Schema::dropIfExists("data_students");
    }
}
