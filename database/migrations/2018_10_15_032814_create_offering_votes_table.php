<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfferingVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offering_votes' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('offering_id')->unsigned()->index();
            $table->integer('role_id')->unsigned()->index();
            $table->timestamps();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('offering_id')->references('id')->on('offerings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offering_votes', function (Blueprint $table) {
            $table->dropForeign('offering_votes_offering_id_foreign'); 
            $table->dropForeign('offering_votes_role_id_foreign'); 
        });
        Schema::dropIfExists("offering_votes");
    }
}
