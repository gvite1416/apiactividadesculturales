<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMimeUserperiodrequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_period_requirements', function (Blueprint $table) {
            $table->enum('mime', ['jpg','jpeg', 'png', 'pdf']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_period_requirements', function (Blueprint $table) {
            $table->dropColumn('mime');
        });
    }
}
