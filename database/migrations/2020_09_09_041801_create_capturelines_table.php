<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapturelinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capturelines', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inscription_id')->nullable()->unique()->unsigned()->index();
            $table->string('pathname', 150)->unique();
            $table->string('agreement')->nullable();
            $table->string('reference', 150)->unique()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('inscription_id')->references('id')->on('inscriptions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('capturelines', function (Blueprint $table) { 
            $table->dropForeign('capturelines_inscription_id_foreign'); 
        });
        Schema::dropIfExists('capturelines');
    }
}
