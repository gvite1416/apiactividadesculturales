<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('period_requirements' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('requirement_id')->unsigned()->index();
            $table->integer('period_id')->unsigned()->index();
            $table->foreign('requirement_id')->references('id')->on('requirements')->onDelete('cascade');
            $table->foreign('period_id')->references('id')->on('periods')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('period_requirements', function (Blueprint $table) {
            $table->dropForeign('requirements_requirement_id_foreign'); 
            $table->dropForeign('requirements_period_id_foreign'); 
        });
        Schema::dropIfExists('period_requirements');
    }
}
