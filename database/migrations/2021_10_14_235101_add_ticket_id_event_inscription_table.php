<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketIdEventInscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_inscriptions', function (Blueprint $table) {
            $table->integer('event_ticket_id')->nullable()->unique()->unsigned()->index();
            $table->foreign('event_ticket_id')->references('id')->on('event_tickets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_tickets', function (Blueprint $table) {
            $table->dropForeign('event_inscriptions_event_ticket_id_foreign');
            $table->dropColumn('event_ticket_id');
        });
    }
}
