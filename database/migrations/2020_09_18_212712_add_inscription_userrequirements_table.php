<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInscriptionUserrequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_requirements', function (Blueprint $table) {
            $table->integer('inscription_id')->nullable()->unique()->unsigned()->index();
            $table->foreign('inscription_id')->references('id')->on('inscriptions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_requirements', function (Blueprint $table) {
            $table->dropForeign('user_requirements_inscription_id_foreign');
            $table->dropColumn('inscription_id');
        });
    }
}
