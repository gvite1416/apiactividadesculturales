<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkshopPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workshop_periods' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('period_id')->unsigned()->index();
            $table->integer('workshop_id')->unsigned()->index();
            $table->integer('teacher_id')->unsigned()->index();
            $table->integer('classroom_id')->unsigned()->index();
            $table->integer('quota');
            $table->string('group');
            $table->text('notes')->nullable();
            $table->boolean('all_campus')->default(0);
            $table->foreign('period_id')->references('id')->on('periods')->onDelete('cascade');
            $table->foreign('workshop_id')->references('id')->on('workshops')->onDelete('cascade');
            $table->foreign('teacher_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workshop_periods', function (Blueprint $table) {
            $table->dropForeign('workshop_periods_period_id_foreign'); 
            $table->dropForeign('workshop_periods_workshop_id_foreign'); 
            $table->dropForeign('workshop_periods_teacher_id_foreign'); 
            $table->dropForeign('workshop_periods_classroom_id_foreign'); 
        });
        Schema::dropIfExists("workshop_periods");
    }
}
