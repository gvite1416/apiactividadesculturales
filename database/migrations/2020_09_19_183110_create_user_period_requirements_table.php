<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPeriodRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_period_requirements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('period_requirement_id')->unsigned()->index();
            $table->integer('inscription_id')->nullable()->unsigned()->index();
            $table->string('file');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('period_requirement_id')->references('id')->on('period_requirements')->onDelete('cascade');
            $table->foreign('inscription_id')->references('id')->on('inscriptions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_period_requirements', function (Blueprint $table) {
            $table->dropForeign('user_period_requirements_period_requirement_id_foreign');
            $table->dropForeign('user_period_requirements_user_id_foreign');
            $table->dropForeign('user_period_requirements_inscription_id_foreign');
        });
        Schema::dropIfExists('user_period_requirements');
    }
}
