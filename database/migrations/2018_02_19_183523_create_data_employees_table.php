<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_employees' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('user_id')->unsigned();
            $table->integer('campus_id')->unsigned();
            $table->enum('workshift',['Matutino','Vespertino','Ambos']);
            $table->string('area');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('campus_id')->references('id')->on('campus')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_employees', function (Blueprint $table) {
            $table->dropForeign('data_employees_user_id_foreign');
            $table->dropForeign('data_employees_campus_id_foreign'); 
        });
        Schema::dropIfExists("data_employees");
    }
}
