<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventShowingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_showings' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('event_id')->unsigned()->index();
            $table->datetime('start');
            $table->datetime('finish');
            $table->timestamps();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_showings', function (Blueprint $table) {
            $table->dropForeign('event_showings_event_id_foreign');
        });
        Schema::dropIfExists("event_showings");
    }
}
