<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent' , function(Blueprint $table){
            $table->increments("id");
            $table->string('name');
            $table->integer('career_id')->unsigned()->index();
            $table->unsignedTinyInteger('semester')->unsigned();
            $table->string('number_id',15)->unique();
            $table->string('email',150);
            $table->string('phone',20)->nullable();
            $table->string('celphone',20);
            $table->enum('arts',['Artes escénicas','Artes musicales']);
            $table->string('art_activity')->nullable();
            $table->string('band')->nullable();
            $table->string('gender')->nullable();
            $table->text('members')->nullable();
            $table->integer('duration')->nullable();
            $table->text('equipment')->nullable();
            $table->text('scenography')->nullable();
            $table->string('illumination')->nullable();
            $table->string('audio_video')->nullable();
            $table->integer('assembly_duration')->nullable();
            $table->integer('deassembly_duration')->nullable();
            $table->string('token');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('career_id')->references('id')->on('careers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('talent', function (Blueprint $table) {
            $table->dropForeign('talent_event_career_id_foreign');
        });
        Schema::dropIfExists("talent");
    }
}
