<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeriodidToCaptureline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('capturelines', function (Blueprint $table) {
            $table->integer('period_id')->unsigned()->index()->default(8);
            $table->foreign('period_id')->references('id')->on('periods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('capturelines', function (Blueprint $table) {
            $table->dropForeign('capturelines_period_id_foreign');
            $table->dropColumn('period_id');
        });
    }
}
