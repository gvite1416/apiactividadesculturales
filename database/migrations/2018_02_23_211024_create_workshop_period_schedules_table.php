<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkshopPeriodSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workshop_period_schedules' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('workshop_period_id')->unsigned()->index();
            $table->tinyInteger('day')->unsigned();
            $table->time('start');
            $table->time('finish');
            $table->timestamps();
            $table->foreign('workshop_period_id')->references('id')->on('workshop_periods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workshop_period_schedules', function (Blueprint $table) {
            $table->dropForeign('workshop_period_schedules_workshop_period_id_foreign'); 
        });
        Schema::dropIfExists("workshop_period_schedules");
    }
}
