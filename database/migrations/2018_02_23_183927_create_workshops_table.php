<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkshopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workshops' , function(Blueprint $table){
            $table->increments("id");
            $table->string('name', 150);
            $table->string('slug', 150)->unique();
            $table->string('image', 255)->nullable();
            $table->text('objective')->nullable();
            $table->text('requirement')->nullable();
            $table->text('information')->nullable();
            $table->boolean('one_more_time')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("workshops");
    }
}
