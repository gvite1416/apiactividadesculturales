<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventCampusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_campus' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('event_id')->unsigned()->index();
            $table->integer('campus_id')->unsigned()->index();
            $table->timestamps();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('cascade');
            $table->foreign('campus_id')->references('id')->on('campus')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_campus', function (Blueprint $table) {
            $table->dropForeign('event_campus_event_id_foreign');
            $table->dropForeign('event_campus_campus_id_foreign');
        });
        Schema::dropIfExists("event_campus");
    }
}
