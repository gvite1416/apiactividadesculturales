<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeDegreeHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('college_degree_hours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('inscription_id')->unsigned()->index();
            $table->datetime('start');
            $table->datetime('finish');
            $table->integer("total_hours");
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('inscription_id')->references('id')->on('inscriptions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('college_degree_hours', function (Blueprint $table) {
            $table->dropForeign('college_degree_hours_inscription_id_foreign');
        });
        Schema::dropIfExists('college_degree_hours');
    }
}
