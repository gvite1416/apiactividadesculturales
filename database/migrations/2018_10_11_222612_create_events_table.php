<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events' , function(Blueprint $table){
            $table->increments("id");
            $table->string('title', 150);
            $table->string('slug', 150)->unique();
            $table->string('image');
            $table->string('thumbnail');
            $table->text('description');
            $table->datetime('inscription_start')->nullable();
            $table->datetime('inscription_finish')->nullable();
            $table->integer('quota')->default(-1);
            $table->integer('event_category_id')->unsigned()->index();
            $table->integer('classroom_id')->unsigned()->index();
            $table->boolean('all_campus')->default(0);
            $table->boolean('one_more_time')->default(0);
            $table->integer('init_folio')->unsigned()->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('event_category_id')->references('id')->on('event_categories')->onDelete('cascade');
            $table->foreign('classroom_id')->references('id')->on('classrooms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->dropForeign('events_category_id_foreign'); 
            $table->dropForeign('events_classroom_id_foreign'); 
        });
        Schema::dropIfExists('events');
    }
}
