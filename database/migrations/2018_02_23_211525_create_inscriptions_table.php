<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscriptions' , function(Blueprint $table){
            $table->increments("id");
            $table->integer('workshop_period_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->integer('folio')->unsigned();
            $table->string('ticket_folio')->nullable();
            $table->date('ticket_date')->nullable();
            $table->integer('quota')->unsigned();
            $table->integer('extra')->unsigned()->nullable();
            $table->tinyInteger('status')->unsigned()->default(0);
            $table->integer('scholarship')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('workshop_period_id')->references('id')->on('workshop_periods')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inscriptions', function (Blueprint $table) {
            $table->dropForeign('inscriptions_workshop_period_id_foreign'); 
            $table->dropForeign('inscriptions_user_id_foreign'); 
        });
        Schema::dropIfExists("inscriptions");
    }
}
