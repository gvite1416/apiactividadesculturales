<?php

use Illuminate\Database\Seeder;
use App\Career;
class CareerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Career::Create([
            'name' => 'ARQUITECTURA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'COMUNICACION Y PERIODISMO',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'DERECHO',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'DISEÑO INDUSTRIAL',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'ECONOMIA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'ING. CIVIL',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'ING. ELECTRICA - ELECTRONICA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'ING. EN COMPUTACION',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'ING. INDUSTRIAL',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'ING. MECANICA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'ING. MECANICA ELECTRICA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'PEDAGOGIA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'PLANIF. PARA EL DES. AGROPECUARIO',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'RELACIONES INTERNACIONALES',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'SOCIOLOGIA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'SUA - DERECHO',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'SUA - ECONOMIA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'SUA - RELACIONES INTERNACIONALES',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'CIRUJANO DENTISTA',
            'code' => '000'
        ]);
        Career::Create([
            'name' => 'PREPARATORIA',
            'code' => '000'
        ]);
    }
}
