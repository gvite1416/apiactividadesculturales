<?php

use Illuminate\Database\Seeder;
use App\Occupation;
class OccupationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Occupation::Create([
            'name' => 'Estudiante'
        ]);
        Occupation::Create([
            'name' => 'Ama de casa'
        ]);
        Occupation::Create([
            'name' => 'Trabajador'
        ]);
        Occupation::Create([
            'name' => 'Otro'
        ]);
    }
}
