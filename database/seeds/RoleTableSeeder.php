<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Administrador',
            'slug' => 'admin',
            'description' => 'Usuario para administrar el sistema',
            'group' => 'depto'
        ]);
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Alumno UNAM',
            'slug' => 'student',
            'description' => 'Alumno de la UNAM que se inscribe a talleres escolares',
            'group' => 'student'
        ]);
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Ex Alumno UNAM',
            'slug' => 'exstudent',
            'description' => 'Ex Alumno de la UNAM que se inscribe a talleres escolares',
            'group' => 'student'
        ]);
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Trabajador UNAM',
            'slug' => 'employee',
            'description' => 'Trabajador de la UNAM que se inscribe a talleres escolares',
            'group' => 'student'
        ]);
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Externo',
            'slug' => 'external',
            'description' => 'Usuario que se inscribe a talleres escolares',
            'group' => 'student'
        ]);
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Servicio Social',
            'slug' => 'socialservice',
            'description' => 'Usuario que ayuda a válidar inscripciones',
            'group' => 'depto'
        ]);
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Profesores',
            'slug' => 'teacher',
            'description' => 'Profesores de los talleres',
            'group' => 'teacher'
        ]);
        \HttpOz\Roles\Models\Role::create([
            'name' => 'Guardia',
            'slug' => 'guard',
            'description' => 'Guardia de seguridad',
            'group' => 'guard'
        ]);
    }
}
