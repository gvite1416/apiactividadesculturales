<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = \HttpOz\Roles\Models\Role::findBySlug('admin');
        $felipe = \App\User::create([
            'name' => 'Gerardo',
            'lastname' => 'Flores',
            'email' => 'gerardo.flores.slaughter@gmail.com',
            'password' => bcrypt('7169Gera'),
            'gender' => 'Masculino',
            'is_verified' => 1,
            'is_verified_dept' => 1
        ]);
        $felipe->attachRole($adminRole);
    }
}
