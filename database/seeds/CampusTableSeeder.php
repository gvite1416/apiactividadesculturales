<?php

use Illuminate\Database\Seeder;
use App\Campus;
class CampusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Campus::Create([
            'name' => 'FES Aragón',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'FES Iztacala',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'FES Acatlán',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'FES Cuautitlán',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'FES Zaragoza',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Arquitectura',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Artes y Diseño',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Ciencias',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Ciencias Políticas y Sociales',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Contaduría y Administración',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Derecho',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Economía',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Filosofía y Letras',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Ingeniería',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Medicina',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Medicina Veterinaria y Zootecnia',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Música',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Odontología',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Psicología',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'Facultad de Química',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'CCH Azcapotzalco',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'CCH Naucalpan',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'CCH Oriente',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'CCH Sur',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'CCH Vallejo',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 1',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 2',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 3',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 4',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 5',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 6',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 7',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 8',
            'code' => '000'
        ]);
        Campus::Create([
            'name' => 'ENEP Plantel 9',
            'code' => '000'
        ]);
    }
}
