<?php

use Illuminate\Database\Seeder;
use App\EventCategory;
class EventCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventCategory::Create([
            'name' => 'Concierto',
            'slug' => 'concierto',
        ]);
        EventCategory::Create([
            'name' => 'Ofrenda',
            'slug' => 'ofrenda',
        ]);
    }
}
